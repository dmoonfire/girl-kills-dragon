#region Namespaces

using System.Collections.Generic;

#endregion

namespace GirlKillsDragon.IO
{
	public class DetailSetData
	{
		public List<DetailSetDetailData> Details { get; set; }

		public string Tags { get; set; }
	}
}
