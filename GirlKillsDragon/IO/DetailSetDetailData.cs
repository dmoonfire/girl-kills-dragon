#region Namespaces

using GirlKillsDragon.Details;

#endregion

namespace GirlKillsDragon.IO
{
	public class DetailSetDetailData
	{
		public DetailType Type { get; set; }

		public string Value { get; set; }
	}
}
