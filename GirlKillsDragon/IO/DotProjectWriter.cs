#region Namespaces

using System.IO;
using System.Linq;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.IO
{
	public class DotProjectWriter : DagProjectWriter
	{
		public DotProjectWriter(NodeManager nodes, LocationManager locations)
			: base(nodes, locations)
		{
		}

		protected override void Write(StreamWriter writer, DagGraph graph)
		{
			// Start the graph.
			writer.WriteLine("digraph G {");

			// Write the elements that are not in a chapter.
			foreach (var node in graph.Nodes.Where(n => n.Category == null && n.Group == null))
			{
				Write(writer, node);
			}

			// Write out the chapters.
			var chapterNames = graph.Nodes
				.Where(n => n.Category != null)
				.Select(n => n.Category)
				.Distinct()
				.ToList();

			for (var i = 0; i < chapterNames.Count; i++)
			{
				// Write out the subgraph. The "cluster" name is special.
				var chapterName = chapterNames[i];

				writer.WriteLine($"subgraph cluster{i} {{");
				writer.WriteLine($"label = \"{chapterName}\"");

				// Write out the nodes in the chapter.
				foreach (var node in graph.Nodes.Where(n => n.Category == chapterName))
				{
					Write(writer, node);
				}

				// Finish up the chapter.
				writer.WriteLine("}");
			}

			// Write out the links for all the nodes.
			foreach (var link in graph.Links.Where(l => l.Category == null))
			{
				Write(writer, link);
			}

			// Finish up the graph.
			writer.WriteLine("}");
		}

		private void Write(StreamWriter writer, DagNode node)
		{
			var label = node.Label.Replace("\n", "\\l").Replace("\"", "\\\"");

			writer.WriteLine($"\t{node.Id} [");
			writer.WriteLine($"\t\tshape=box");
			//writer.WriteLine($"\t\tstyle=filled");
			//writer.WriteLine($"\t\tcolor=\"{node.Foreground}\"");
			//writer.WriteLine($"\t\tfontcolor=\"{node.Foreground}\"");
			//writer.WriteLine($"\t\tbgcolor=\"{node.Background}\"");
			writer.WriteLine($"\t\tlabel=\"{label}\\l\"");
			writer.WriteLine("\t]");
		}

		private void Write(StreamWriter writer, DagLink link)
		{
			var label = link.Label.Replace("\n", "\\l").Replace("\"", "\\\"");

			writer.WriteLine($"{link.Source} -> {link.Target} [");
			writer.WriteLine($"\t\tlabel=\"{label}\\l\"");

			if (!string.IsNullOrWhiteSpace(link.StrokeThickness))
			{
				writer.WriteLine($"\t\tpenwidth={link.StrokeThickness}");
			}

			if (!string.IsNullOrWhiteSpace(link.StrokeDashArray))
			{
				writer.WriteLine("\t\tstyle=dotted");
			}

			writer.WriteLine("]");
		}
	}
}
