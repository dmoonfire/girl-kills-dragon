#region Namespaces

using System.Collections.Generic;
using System.Xml.Serialization;
using GirlKillsDragon.Extensions;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Writing;

#endregion

namespace GirlKillsDragon.IO
{
	public struct DagNode
	{
		[XmlAttribute] public string Id;

		[XmlAttribute] public string Label;

		[XmlAttribute] public string Status;

		[XmlAttribute] public string StartTime;

		[XmlAttribute] public string EndTime;

		[XmlAttribute] public string Elapsed;

		[XmlAttribute] public string Foreground;

		[XmlAttribute] public string Background;

		[XmlAttribute] public string Stroke;

		[XmlAttribute] public string Actors;

		[XmlAttribute] public string Location;

		[XmlAttribute] public string DateTime;

		[XmlAttribute] public string Category;

		[XmlAttribute] public string Group;

		public DagNode(Node node)
			: this()
		{
			// Figure out which actors are in here.
			var actors = node.InActorNames;

			if (actors.Count == 0 && node is ActorNodeBase actorNode)
			{
				actors = new List<string> {actorNode.Actor.ToString()};
			}

			// Set some other labels.
			Id = $"N{node.NodeId}";
			Location = node.Location.ToString();
			Actors = actors.OxfordAndJoin();
			DateTime = node.DateTime.Specific?.ToString("yyyy-MM-dd HH:mm:ss");

			// If we have chapter, add that.
			var chapterLabel = "";

			if (node.Chapter != null)
			{
				Category = $"Chapter {node.Chapter.Number}";
				chapterLabel = $"\nChapter {node.Chapter.Number}, Scene {node.Chapter.IndexOf(node) + 1}";
			}

			// Build up the message.
			Label = string.Join(
				"\n",
				$"N{node.NodeId} ({node.GetType().Name}, {node.NodeType})",
				DateTime + chapterLabel,
				$"Locations: {Location}",
				$"Actors: {Actors}");

			// Color the node based on node type.
			// Scheme from http://paletton.com/#uid=73n0u0kllllaFw0g0qFqFg0w0aF
			switch (node.NodeType)
			{
				case NodeType.Past:
					Background = "#FFE5AA";
					Foreground = "#553B00";
					break;
				case NodeType.Narrative:
					Background = "#6B949E";
					Foreground = "#012B35";
					break;
				case NodeType.Epilogue:
					Background = "#7A85AD";
					Foreground = "#07123A";
					break;
				case NodeType.Future:
					Background = "#FFD4AA";
					Foreground = "#552A00";
					break;
			}
		}

		public DagNode(Location location)
			: this()
		{
			// Set some other labels.
			Id = $"L{location.LocationId}";

			// Build up the message.
			Label = string.Join(
				"\n",
				location.ToString(),
				location.Country?.ToString(),
				$"{location.Distance / 1000:N2} km");
		}

		public DagNode(string id, string label)
			: this()
		{
			Id = id;
			Label = label;
		}

		public DagNode(Chapter chapter)
			: this()
		{
			Id = $"Chapter {chapter.Number}";
			Actors = chapter.Actor.Name;
			Group = "Expanded";
		}
	}
}
