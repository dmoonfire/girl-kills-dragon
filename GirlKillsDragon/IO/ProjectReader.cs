#region Namespaces

using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

#endregion

namespace GirlKillsDragon.IO
{
	public class ProjectReader
	{
		public ProjectData Read(FileInfo file)
		{
			using (var reader = file.OpenText())
			{
				var deserializer = new DeserializerBuilder()
					.WithNamingConvention(new CamelCaseNamingConvention())
					.Build();
				var projectData = deserializer.Deserialize<ProjectData>(reader);

				return projectData;
			}
		}
	}
}
