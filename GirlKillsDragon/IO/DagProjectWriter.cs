#region Namespaces

using System.IO;
using System.Linq;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.IO
{
	/// <summary>
	/// Base class for writers which generate a directed graph.
	/// </summary>
	public abstract class DagProjectWriter
	{
		private readonly LocationManager _locations;

		private readonly NodeManager _nodes;

		protected DagProjectWriter(NodeManager nodes, LocationManager locations)
		{
			_nodes = nodes;
			_locations = locations;
		}

		public void Write(StreamWriter writer)
		{
			// Create the DGML graph.
			var chapters = _nodes
				.Where(n => n.Chapter != null)
				.Select(n => n.Chapter)
				.Distinct()
				.ToList();

			var nodeNodes = _nodes.Select(p => new DagNode(p));
			var locationNodes = _locations.Select(l => new DagNode(l));
			var chapterNodes = chapters.Select(c => new DagNode(c));

			var nodeEdges = _nodes.SelectMany(p => p.OutEdges).Select(l => new DagLink(l));
			var beforeEdges = _nodes.SelectMany(p => p.BeforePlots.Select(pb => new DagLink(p, pb)));
			var locationEdges = _locations.Routes.Select(r => new DagLink(r));
			var chapterEdges = chapters.Skip(1).Select(c => new DagLink(c));
			var chapterNodeEdges = chapters.SelectMany(c => c.Select(n => new DagLink(c, n)));

			var chapterCategories = chapters
				.Select(c => new DagCategory(c))
				.ToList();

			var graph = new DagGraph
			{
				Nodes = nodeNodes
					.Union(locationNodes)
					.Union(chapterNodes)
					.ToArray(),
				Links = nodeEdges
					.Union(beforeEdges)
					.Union(locationEdges)
					.Union(chapterEdges)
					.Union(chapterNodeEdges)
					.ToArray(),
				Categories = chapterCategories.ToArray()
			};

			// Write out the graph.
			Write(writer, graph);
		}

		protected abstract void Write(StreamWriter writer, DagGraph graph);
	}
}
