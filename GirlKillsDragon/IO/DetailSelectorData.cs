#region Namespaces

using System.Collections.Generic;

#endregion

namespace GirlKillsDragon.IO
{
	public class DetailSelectorData
	{
		public Dictionary<string, string> Details { get; set; }

		public string Selector { get; set; }

		public string Tags { get; set; }
	}
}
