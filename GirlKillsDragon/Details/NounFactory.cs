#region Namespaces

using System.Linq;
using Serilog;

#endregion

namespace GirlKillsDragon.Details
{
	/// <summary>
	/// Factory class that generates nouns based on their keywords.
	/// </summary>
	public class NounFactory
	{
		private readonly DetailSetManager _detailSets;

		private readonly ILogger _log;

		private readonly NounSelectorManager _selectors;

		private readonly NounTemplateManager _templates;

		public NounFactory(
			ILogger log,
			NounTemplateManager templates,
			NounSelectorManager selectors,
			DetailSetManager detailSets)
		{
			_log = log;
			_templates = templates;
			_selectors = selectors;
			_detailSets = detailSets;
		}

		public Noun Create(string selectorTags)
		{
			var tags = new TagSet(selectorTags);

			return Create(tags);
		}

		public Noun Create(TagSet selectorTags)
		{
			// Start by getting the noun selector for these tags. This defines what
			// we'll be creating including the various templates/slots it will have.
			var selector = _selectors.Choose(selectorTags);

			// Create the top-level noun.
			var noun = new Noun();

			if (selector == null)
			{
				_log.Warning(
					"Cannot find a selector for {0} from {1} entries",
					selectorTags,
					_selectors.Count());
			}
			else
			{
				Set(selector, noun, selectorTags, selector.SelectorTags);
			}

			return noun;
		}

		public void Set(
			NounSelector selector,
			Noun parentNoun,
			TagSet selectorTags,
			TagSet additionalTags)
		{
			// Get the template for the base item, that is what we'll be using to
			// generate the actual details.
			var template = _templates.Choose(selectorTags) ?? new NounTemplate(selectorTags.ToString());

			// To handle some recursion, we add "of:" and "in:" to include child
			// tags. This will make selection easier while doing details.
			var ofTags = new TagSet(template.Tags.Select(t => $"{t}:of")).Union(additionalTags);
			var inTags = new TagSet(template.Tags.Select(t => $"{t}:in")).Union(ofTags);
			var detailTags = ofTags.Union(template.Tags);

			// Create a noun based on this template.
			Set(selector, template, detailTags, parentNoun);

			// Go through the list and create all the sub templates.
			foreach (var subTag in template.SubTags)
			{
				Set(selector, parentNoun, subTag, inTags);
			}
		}

		private void Set(NounSelector selector, NounTemplate template, TagSet detailTags, Noun noun)
		{
			// Figure out if the selector is going to update the tags we'll be using for the
			// details. This lets the selector clarify or limit the tagging even further. We
			// wrapped this in an array to handle closure.
			TagSet[] tagArray = {template.Tags};

			foreach (var selectorDetail in selector.Where(sd => tagArray[0].IsMatch(sd.TemplateTags)))
			{
				tagArray[0] = selectorDetail.DetailTags;
			}

			// Choose a single random detail based on the tags.
			var templateTags = tagArray[0];
			var detailSet = _detailSets.Choose(templateTags);

			if (detailSet == null)
			{
				return;
			}

			// We add some additional selectors based on the detail's sets.
			if (detailSet.ContainsHasDetails)
			{
				detailTags = detailTags.Union(":has");
			}

			if (detailSet.ContainsIsDetails)
			{
				detailTags = detailTags.Union(":is");
			}

			// Add this detail set to the appropriate noun.
			var nounDetail = new NounDetail(detailTags, detailSet);

			noun.Details.Add(nounDetail);
		}
	}
}
