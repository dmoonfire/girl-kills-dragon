namespace GirlKillsDragon.Details
{
	public enum DetailType
	{
		/// <summary>
		/// Indicates that the detail can be expressed as "X is Y".
		/// </summary>
		Is,

		/// <summary>
		/// Indicates the detail can be expressed as "X has Y".
		/// </summary>
		Has
	}
}
