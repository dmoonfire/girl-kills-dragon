namespace GirlKillsDragon.Details
{
	public interface ITaggedItem
	{
		TagSet Tags { get; }
	}
}
