#region Namespaces

using System.Linq;

#endregion

namespace GirlKillsDragon.Details
{
	public class WriterContext
	{
		public WriterContext()
		{
			Nouns = new NounCollection();
		}

		public NounCollection Nouns { get; set; }

		public Noun Subject { get; set; }

		public Detail GetDetail(DetailType detailType)
		{
			return GetDetail(Subject, detailType);
		}

		private Detail GetDetail(Noun noun, DetailType detailType)
		{
			return noun.Details
				.Select(nd => nd.Details)
				.SelectMany(ds => ds)
				.First(d => d.DetailType == detailType);
		}
	}
}
