#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Details
{
	/// <summary>
	/// Represents an abstract object in the system.
	/// </summary>
	public class Noun
	{
		public Noun()
		{
			Details = new List<NounDetail>();
		}

		public Noun(string name)
			: this()
		{
			Name = name;
		}

		public List<NounDetail> Details { get; set; }

		public string Name { get; set; }

		public Noun AddDetails(string tags, params string[] details)
		{
			var tagSet = new TagSet(tags);

			return AddDetails(tagSet, details);
		}

		public Noun AddDetails(string tags, params Detail[] details)
		{
			var tagSet = new TagSet(tags);

			return AddDetails(tagSet, details);
		}

		public Noun AddDetails(TagSet tagSet, params string[] details)
		{
			return AddDetails(
				tagSet,
				details
					.Select(d => new Detail(DetailType.Is, d))
					.ToArray());
		}

		public Noun AddDetails(TagSet tags, params Detail[] details)
		{
			foreach (var detail in details)
			{
				var detailSet = new DetailSet(tags) {detail};

				AddDetails(tags, detailSet);
			}

			return this;
		}

		public Noun AddDetails(string tags, DetailSet details)
		{
			var tagSet = new TagSet(tags);

			return AddDetails(tagSet, details);
		}

		public Noun AddDetails(TagSet tags, DetailSet details)
		{
			var nounDetail = new NounDetail(tags, details);

			Details.Add(nounDetail);

			return this;
		}

		public List<NounDetail> GetDetails(string tags)
		{
			var tagSet = new TagSet(tags);

			return GetDetails(tagSet);
		}

		private List<NounDetail> GetDetails(TagSet tags)
		{
			return Details
				.Where(nd => tags.IsMatch(nd.Tags))
				.ToList();
		}

		public string GetValue(string tags)
		{
			return GetDetails(tags).FirstOrDefault()?.Value;
		}

		public override string ToString()
		{
			return $"{Name} (Details {Details.Count})";
		}
	}
}
