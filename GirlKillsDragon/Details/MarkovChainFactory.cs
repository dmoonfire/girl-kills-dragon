#region Namespaces

using System;
using System.IO;
using System.Text.RegularExpressions;

#endregion

namespace GirlKillsDragon.Details
{
	/// <summary>
	/// Creates a Markov chain from a US Census data file with weights.
	/// </summary>
	public class MarkovChainFactory
	{
		private readonly Chaos _chaos;

		private MarkovChain _chain;

		public MarkovChainFactory(Chaos chaos)
		{
			_chaos = chaos;
		}

		public bool ForceUnique { get; set; }

		public bool IgnoreWeights { get; private set; }

		public int Order { get; set; } = 4;

		public bool SingleToken { get; set; }

		public double Weight { get; set; } = 1.0;

		public MarkovChainFactory Append(FileInfo file)
		{
			// Make sure we have a valid chain.
			EnsureChainExists();

			// Append the file into the chain.
			using (var stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
			using (var reader = new StreamReader(stream))
			{
				string line;

				while ((line = reader.ReadLine()) != null)
				{
					// Ignore comments and blanks.
					if (string.IsNullOrWhiteSpace(line) || line.StartsWith("#"))
					{
						continue;
					}

					// Add in regular expression replacements.
					if (line.StartsWith("@"))
					{
						var replaces = line.Split(new[] {'@'}, StringSplitOptions.RemoveEmptyEntries);

						_chain.AddReplacement(
							v => Regex.Replace(v, replaces[0], replaces[1], RegexOptions.IgnoreCase));
					}

					// For everything else, we split it based on the fields and add them.
					var parts = !SingleToken
						? line.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
						: new[] {line.Trim()};

					var name = parts[0].ToLower();
					var weight = !IgnoreWeights && parts.Length > 1
						? double.Parse(parts[1])
						: Weight;

					_chain.Add(name, weight);

					// If we are forcing unique, add the excluded results.
					if (ForceUnique)
					{
						_chain.Exclude(name);
					}
				}
			}

			return this;
		}

		public MarkovChain Create()
		{
			var chain = _chain;
			_chain = null;
			return chain;
		}

		public MarkovChain Create(FileInfo file)
		{
			Append(file);
			return Create();
		}

		private void EnsureChainExists()
		{
			if (_chain == null)
			{
				Reset();
			}
		}

		public void Reset()
		{
			_chain = new MarkovChain(_chaos, Order);
		}

		public MarkovChainFactory SetForceUnique(bool value = true)
		{
			ForceUnique = value;
			return this;
		}

		public MarkovChainFactory SetIgnoreWeights(bool value = true)
		{
			IgnoreWeights = value;
			return this;
		}

		public MarkovChainFactory SetSingleToken(bool value = true)
		{
			SingleToken = value;
			return this;
		}

		public MarkovChainFactory SetWeight(double weight)
		{
			Weight = weight;
			return this;
		}
	}
}
