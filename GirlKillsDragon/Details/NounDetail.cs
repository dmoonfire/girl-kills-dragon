#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Details
{
	public class NounDetail : ITaggedItem
	{
		public NounDetail(string tags, DetailSet details)
			: this(new TagSet(tags), details)
		{
		}

		public NounDetail(TagSet tags, DetailSet details)
		{
			Tags = tags;
			Details = details;
		}

		public DetailSet Details { get; }

		public List<Detail> HasDetails => Details.Where(d => d.DetailType == DetailType.Has).ToList();

		public List<Detail> IsDetails => Details.Where(d => d.DetailType == DetailType.Is).ToList();

		public Detail this[int index] => Details[index];

		public string Value => Details[0].Value;

		public TagSet Tags { get; }

		public override string ToString()
		{
			return $"({Tags} (Details {Details}))";
		}
	}
}
