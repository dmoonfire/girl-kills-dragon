#region Namespaces

using System.Collections;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Details
{
	public class DetailSet : IEnumerable<Detail>
	{
		private readonly List<Detail> _details = new List<Detail>();

		public DetailSet()
		{
		}

		public DetailSet(string tags)
			: this(new TagSet(tags))
		{
		}

		public DetailSet(TagSet tags)
		{
			Tags = tags;
		}

		public bool ContainsHasDetails => _details.Any(d => d.DetailType == DetailType.Has);

		public bool ContainsIsDetails => _details.Any(d => d.DetailType == DetailType.Has);

		public Detail this[int index] => _details[index];

		/// <summary>
		/// Gets or sets the tags that are used to say what this detail provides to avoid
		/// overlapping detail sets.
		/// </summary>
		public TagSet Provides { get; set; } = new TagSet();

		/// <summary>
		/// Gets or sets the tags used to look up a given detail set.
		/// </summary>
		public TagSet Tags { get; set; } = new TagSet();

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<Detail> GetEnumerator()
		{
			return _details.GetEnumerator();
		}

		public void Add(Detail detail)
		{
			_details.Add(detail);
		}

		public override string ToString()
		{
			return $"({Tags} (Provides {Provides}))";
		}
	}
}
