#region Namespaces

using System;
using System.Collections.Generic;
using System.IO;
using GirlKillsDragon.Actors;
using GirlKillsDragon.IO;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Writing;
using Semver;
using Serilog;

#endregion

namespace GirlKillsDragon
{
	/// <summary>
	///     The primary contextual class that handles all the processing for a given story including the DAG of plot
	///     items, actors and locations involved, and other elements needed to write out the results.
	/// </summary>
	public class Project
	{
		private readonly Author _author;

		private readonly ActorManager _actors;

		private readonly LocationManager _locations;

		private readonly CountryManager _countries;

		private readonly Chaos _chaos;

		private readonly ILogger _log;

		public Project(
			ILogger log,
			Chaos chaos,
			Author author,
			NodeManager nodes,
			ActorManager actors,
			LocationManager locations,
			CountryManager countries)
		{
			_log = log;
			_chaos = chaos;
			_author = author;
			_actors = actors;
			_locations = locations;
			_countries = countries;

			nodes.Project = this;
		}

		/// <summary>
		///     The desired narrative count for the resulting output.
		/// </summary>
		public int NarrativeCount => ProjectData.Narrative;

		public ProjectData ProjectData { get; private set; }

		public DateTime Start =>
			ProjectData.DateTime ?? new DateTime(1987, 5, 6, 8, 19, 0);

		public void Initialize(ProjectData projectData)
		{
			ProjectData = projectData;

			_chaos.SetSeed(ProjectData.Seed);
		}

		public void Write(
			StreamWriter writer,
			List<Chapter> chapters)
		{
			// Write out the top-matter and YAML header.
			var version = new SemVersion(typeof(CoreModule).Assembly.GetName().Version);

			writer.WriteLine("---");
			writer.WriteLine($"generator: GirlKillDragon v{version}");
			writer.WriteLine("ad: Read D. Moonfire's non-procedural novels at https://fedran.com/");
			writer.WriteLine("---");

			// Wrap the writer in a word-counting one.
			using (var wordCountingWriter = new WordCountingWriter(writer))
			{
				// Go through the chapters and write each one.
				foreach (var chapter in chapters)
				{
					_author.WriteChapter(wordCountingWriter, chapter);
				}

				// Report how many words we've written.
				_log.Information("The result was {0:N0} words long", wordCountingWriter.WordCount);
				_log.Information(
					"There are {0:N0} actors, {1:N0} locations, {2:N0} countries",
					_actors.Count,
					_locations.Count,
					_countries.Count);
			}
		}
	}
}
