namespace GirlKillsDragon.Plots.Resolvers
{
	/// <summary>
	/// Indicates a resolver that is run after every plot injection.
	/// </summary>
	public interface IInjectionResolver : IResolver
	{
	}
}
