#region Namespaces

using System.Collections.Generic;
using GirlKillsDragon.Nodes;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	/// <summary>
	/// Removes nodes that aren't connected to the main node.
	/// </summary>
	public class RemoveOrphanNodesResolver : IInjectionResolver
	{
		private readonly NodeGraph _graph;

		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		public RemoveOrphanNodesResolver(
			ILogger log,
			NodeManager nodes,
			NodeGraph graph)
		{
			_log = log;
			_nodes = nodes;
			_graph = graph;
		}

		public int ResolverOrder => 0;

		public void Resolve()
		{
			// Keep track of all the plots already there so we can remove the
			// onces as we see them.
			var plots = _nodes;
			var unseen = new List<Node>(plots);

			_log.Verbose("Checking connectivity of {0:N0} nodes", plots.Count);

			// Make sure the initial plot is in the list.
			if (!plots.Contains(_nodes.InitialNode))
			{
				_log.Error("Cannot find initial node in the node collections.");
				return;
			}

			// Use a visitor to visit every connected node.
			var visitor = new GraphVisitor
			{
				VisitNode = delegate(Node node) { unseen.Remove(node); }
			};

			visitor.Visit(_graph, _nodes.InitialNode);

			// If we get through the loop (we should), then if unseen has
			// anything left, we have something that can't reach. In this
			// case, we want to remove all of them from the tree.
			if (unseen.Count <= 0)
			{
				return;
			}

			_nodes.Remove(unseen);
			_log.Information("Removing {0} unreachable nodes leaving {1}", unseen.Count, _nodes.ToString());
		}
	}
}
