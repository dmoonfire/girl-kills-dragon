#region Namespaces

using System.Collections.Generic;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Details;
using GirlKillsDragon.Locations;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	public class CountryActorNamingResolver : IPlotResolver
	{
		private readonly ActorManager _actors;

		private readonly CountryManager _countries;

		private readonly CountryResolver _countryResolver;

		private readonly ILogger _log;

		public CountryActorNamingResolver(
			ILogger log,
			ActorManager actors,
			CountryManager countries,
			CountryResolver countryResolver)
		{
			_log = log;
			_actors = actors;
			_countries = countries;
			_countryResolver = countryResolver;
		}

		public bool Enabled { get; set; } = true;

		public int ResolverOrder => _countryResolver.ResolverOrder + 1;

		public void Resolve()
		{
			// If we aren't enabled (for tests), we don't resolve. This gives
			// us a consistent testing without the random naming breaking it
			// as the inputs change.
			if (!Enabled)
			{
				return;
			}

			// Go through the actors and name each one based on the country of
			// their origin. We make sure there are no duplicates in the names
			// and strongly discourage names with repeated first characters.
			var seenLast = new HashSet<string>();
			var seenFirst = new HashSet<string>();
			var seenLetter = new HashSet<string>();

			foreach (var actor in _actors)
			{
				// See if we have a country, things don't work without it.
				if (actor.Country == null)
				{
					_log.Warning("{0} doesn't have a country", actor);
					actor.Noun
						.AddDetails("name.first", actor.ToString())
						.AddDetails("name.last", actor.ToString())
						.AddDetails("name", actor.ToString());
					continue;
				}

				// Figure out the pronoun which determines which value.
				var pronoun = actor.Pronoun;

				// Figure out the names, trying to avoid repeats.
				var first = GetGivenName(
					pronoun == "he"
						? actor.Country.MaleMarkovChain
						: actor.Country.FemaleMarkovChain,
					seenFirst,
					seenLetter);
				var last = GetGivenName(
					actor.Country.LastMarkovChain,
					seenLast,
					seenLetter);

				// Finish up the name.
				var full = $"{first} {last}";

				actor.Noun
					.AddDetails("name.first", new Detail(DetailType.Is, first))
					.AddDetails("name.last", new Detail(DetailType.Is, last))
					.AddDetails("name", new Detail(DetailType.Is, full));
			}
		}

		private string GetGivenName(
			MarkovChain chain,
			HashSet<string> seen,
			HashSet<string> seenLetter)
		{
			// Loop forever until we find a valid combination.
			var attempt = 0;

			while (true)
			{
				// Generate a random name.
				var name = chain.Create();

				// If we've seen the name already, always ignore it.
				if (seen.Contains(name))
				{
					continue;
				}

				// See if we've seen the first letter, we'll try a number of
				// times before giving up.
				var first = name[0].ToString();

				if (seenLetter.Contains(first))
				{
					if (++attempt > 10)
					{
						attempt = 0;
						seenLetter.Clear();
					}

					continue;
				}

				// Mark that we've accept this so mark it for repeat.
				seen.Add(name);
				seenLetter.Add(first);

				return name;
			}
		}
	}
}
