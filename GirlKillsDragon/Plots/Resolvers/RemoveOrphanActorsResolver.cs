#region Namespaces

using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Nodes;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	/// <summary>
	/// Removes actors that are not longer associated with any valid plot.
	/// </summary>
	public class RemoveOrphanActorsResolver : IInjectionResolver
	{
		private readonly ActorManager _actors;

		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		private readonly RemoveOrphanNodesResolver _removeOrphanNodesResolver;

		public RemoveOrphanActorsResolver(
			ILogger log,
			ActorManager actors,
			NodeManager nodes,
			RemoveOrphanNodesResolver removeOrphanNodesResolver)
		{
			_log = log;
			_actors = actors;
			_nodes = nodes;
			_removeOrphanNodesResolver = removeOrphanNodesResolver;
		}

		public int ResolverOrder => _removeOrphanNodesResolver.ResolverOrder + 1;

		public void Resolve()
		{
			// Get all actors that aren't used by any current node.
			var allActors = _actors.ToList();
			var used = _nodes.SelectMany(n => n.InActors).ToList();
			var unused = allActors
				.Where(a => !used.Contains(a))
				.ToList();

			if (unused.Count == 0)
			{
				return;
			}

			// Report the actors we're removing and do so.
			_actors.Remove(unused);
			_log.Information("Removing {0} unused actors leaving {1}", unused.Count, _actors.Count);
		}
	}
}
