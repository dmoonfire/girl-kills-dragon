#region Namespaces

using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	/// <summary>
	/// Determines the distances between the various abstract locations so travel times can
	/// be calculated.
	/// </summary>
	public class LocationDistanceResolver : IInjectionResolver
	{
		private readonly ActorManager _actors;

		private readonly NodeGraph _graph;

		private readonly LocationManager _locations;

		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		public LocationDistanceResolver(
			ILogger log,
			ActorManager actors,
			LocationManager locations,
			NodeManager nodes,
			NodeGraph graph)
		{
			_log = log;
			_actors = actors;
			_locations = locations;
			_nodes = nodes;
			_graph = graph;
		}

		public int ResolverOrder => 0;

		public void Resolve()
		{
			// Reset all of the locations.
			Reset();

			// The bulk of the system is based on the distance from the starting point. We use
			// that to create the idea of "less friendly" or more exotic the further away the
			// story goes from the starting point. To do that, we want to calculate the path
			// between the initial node while following the first character.
			RouteHeroLocations();

			// Once we get through this, we have to backfill the other locations to get their distance
			// from an established route. We do that by trying to find a location that has been routed
			// (which means it has a distance) and figure it out from there. If we can't, we try again
			// until we can resolve all of them.
			while (true)
			{
				// Get all the nodes that we have not processed their locations.
				var nodes = _nodes
					.Where(p => !p.Location.Distance.HasValue)
					.ToList();

				if (nodes.Count == 0)
				{
					break;
				}

				// Go through the nodes and attempt to calculate them.
				var processedAtLeastOne = false;

				foreach (var node in nodes)
				{
					// Get all the locations adjacent to this node. We also only look for ones that have
					// a distance calculated. If we don't have any, then we can't calculate this node.
					var adjacentLocations = node.InNodes
						.Select(n => n.Location)
						.Union(node.OutNodes.Select(n => n.Location))
						.Where(l => l != node.Location)
						.Where(l => l.Distance.HasValue)
						.Distinct()
						.ToList();

					if (adjacentLocations.Count == 0)
					{
						continue;
					}

					// Establish a direct connection with all the adjacent ones.
					foreach (var adjacent in adjacentLocations)
					{
						_locations.EstablishDirectRoute(adjacent, node.Location);
					}

					processedAtLeastOne = true;
				}

				// If we didn't process one, we have a problem which means we have locations
				// that need to be culled.
				if (processedAtLeastOne)
				{
					continue;
				}

				_log.Debug("Cannot calculate distance between remaining locations");
				break;
			}

			// Remove all disconnected locations.
			_locations.RemoveUnconnected();
		}

		private void Reset()
		{
			var locations = _locations.Where(l => l != _nodes.InitialNode.Location).ToList();

			foreach (var location in locations)
			{
				location.Distance = null;
			}
		}

		private void RouteHeroLocations()
		{
			// First mark the initial location as having a distance of zero.
			var hero = _actors.Primary;
			var initialNode = hero.Born;
			var location = initialNode.Location;

			location.Distance = 0;

			// Follow through on the hero's path.
			var visitor = new GraphVisitor
			{
				BackwardEdgeFilter = EdgeFilter.ExcludeAll,
				ForwardEdgeFilter = EdgeFilter.Follow(hero).SetIncludeAll(),
				VisitNode = delegate(Node node)
				{
					if (!node.Location.Distance.HasValue)
					{
						_locations.EstablishDirectRoute(location, node.Location);
					}

					location = node.Location;
				}
			};

			visitor.Visit(_graph, initialNode);
		}
	}
}
