#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Nodes;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	public class PlotDateTimeResolver : IInjectionResolver
	{
		private readonly BornDateTimeResolver _bornDateTimeResolver;

		private readonly Chaos _chaos;

		private readonly LocationDistanceResolver _locationDistanceResolver;

		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		public PlotDateTimeResolver(
			ILogger log,
			Chaos chaos,
			NodeManager nodes,
			BornDateTimeResolver bornDateTimeResolver,
			LocationDistanceResolver locationDistanceResolver)
		{
			_log = log;
			_chaos = chaos;
			_nodes = nodes;
			_bornDateTimeResolver = bornDateTimeResolver;
			_locationDistanceResolver = locationDistanceResolver;
		}

		public int ResolverOrder =>
			Math.Max(_bornDateTimeResolver.ResolverOrder, _locationDistanceResolver.ResolverOrder) + 1;

		/// <summary>
		/// Goes through and resolves the relative dates in each plot
		/// into concrete DateTime objects.
		/// </summary>
		public void Resolve()
		{
			// Reset the date time to a known state.
			Reset();

			// Get a list of all the plots that don't have a specific time.
			List<Node> remaining;

			while ((remaining = GetUnspecifiedPlots()).Count > 0)
			{
				// First go through and attempt to resolve all plots while requiring every other link on a given side (in or out)
				// being resolved. This handles most of the problems we have where we have forked nodes not resolving.
				var resolvedAtLeastOne = false;

				foreach (var plot in remaining)
				{
					var resolved = Resolve(plot);

					resolvedAtLeastOne |= resolved;
				}

				if (resolvedAtLeastOne)
				{
					// We prefer this style of resolving, so we restart and see if it ends up resolving more plots.
					continue;
				}

				// If we still got this far, then the previous step didn't resolve anything and we need to fill in one of the
				// nodes to try again. To do this, we want to pick a node that has one (but not all) of its incoming nodes
				// specified and then backfill one of those incoming nodes with an arbitrary date.
				try
				{
					var backfillNode = remaining
						.Where(
							delegate(Node node)
							{
								var links = GetBeforePlotLinksInfo(node);
								return links.HasOneDirectSpecified && !links.HasAllDateTimes;
							})
						.OrderBy(
							delegate(Node node)
							{
								var links = GetBeforePlotLinksInfo(node);
								return links.GraphDepth;
							})
						.FirstOrDefault();

					if (backfillNode == null)
					{
						throw new InvalidOperationException("Cannot find a matching node.");
					}

					// Figure out the links for this node.
					var backfillLinks = GetBeforePlotLinksInfo(backfillNode);
					var backfillLink = backfillLinks.Links.Values
						.OrderBy(link => link.Node.NodeId)
						.First(edge => edge.Node.DateTime.Resolved == DateTime.MinValue);

					// Backfill by a random amount before the starting point of the current time.
					var backfillDateTime = backfillLinks.DateTime.AddMinutes((1 + _chaos.NextDouble()) * -1);

					backfillLink.Node.DateTime.Resolved = backfillDateTime;
				}
				catch
				{
					_log.Error("Could not resolve times for {0:N0} plots.", remaining.Count);

					foreach (var remain in remaining.Take(10))
					{
						_log.Error("Could not resolve {0}", remain);
					}

					throw;
				}
			}

			// Go through and mark everything before the initial plot as happening in the past.
			var initialDateTime = _nodes.InitialNode.DateTime.Resolved;
			var pastNodes = _nodes.Where(p => p.DateTime.Resolved < initialDateTime);

			foreach (var plot in pastNodes)
			{
				plot.NodeType = NodeType.Past;
			}

			// Go through and mark all plots after the epilogue as in the future.
			if (_nodes.EpilogueNode != null)
			{
				var epilogueDateTime = _nodes.EpilogueNode.DateTime.Resolved;
				var futureNodes = _nodes.Where(p => p.DateTime.Resolved > epilogueDateTime);

				foreach (var plot in futureNodes)
				{
					plot.NodeType = NodeType.Future;
				}
			}

			// Finish resolving plots.
			_log.Verbose("Resolved dates and times: {0}", _nodes.ToString());
		}

		private PlotLinksInfo GetAfterPlotLinksInfo(Node node)
		{
			return GetPlotLinksInfo(
				node,
				node.OutEdges,
				p => p.TailNode,
				node.BeforePlots.Select(p => new Edge(node, p)),
				(t, d) => d - t.Get(),
				Math.Min);
		}

		private PlotLinksInfo GetBeforePlotLinksInfo(Node node)
		{
			return GetPlotLinksInfo(
				node,
				node.InEdges,
				p => p.HeadNode,
				node.AfterPlots.Select(p => new Edge(p, node)),
				(t, d) => d + t.Get(),
				Math.Max);
		}

		private PlotLinksInfo GetPlotLinksInfo(
			Node node,
			EndpointEdgeCollection directLinks,
			Func<Edge, Node> getNode,
			IEnumerable<Edge> relativeLinks,
			Func<PlotTimeSpan, DateTime, DateTime> applyLinkTimeSpan,
			Func<long, long, long> getTimestamp)
		{
			// We fake a link between the relative plots so the rest of the
			// code is logical.
			var edges = directLinks.Union(relativeLinks).ToList();

			// Get the collection we are working with. If we don't have anything in the list, then we
			// return an empty collection.
			var count = edges.Count;

			if (count == 0)
			{
				return new PlotLinksInfo();
			}

			// If we have items, we want to see if every one has a specific date time or not.
			var info = new PlotLinksInfo {Count = count};

			foreach (var edge in edges)
			{
				// Add the link for debugging purposes.
				var otherNode = getNode(edge);
				var isDirect = directLinks.Contains(edge);

				info.Links[otherNode.ToString()] = new LinkInfo(
					isDirect,
					otherNode.DateTime.Resolved,
					otherNode);

				// If we don't have a specific one, we can't resolve this one at all.
				if (otherNode.DateTime.Resolved == DateTime.MinValue)
				{
					continue;
				}

				// Figure out the date time for the link based on the timespan between them.
				var otherDateTime = applyLinkTimeSpan(edge.TimeSpan, otherNode.DateTime.Resolved);

				// Otherwise, we grab the appropriate date.
				info.DateTime = info.HasOneDirectSpecified && info.DateTime != DateTime.MinValue
					? new DateTime(getTimestamp(info.DateTime.Ticks, otherDateTime.Ticks))
					: otherDateTime;
			}

			// Return the resulting links.
			return info;
		}

		/// <summary>
		/// Gets all the plots that don't have a specific time.
		/// </summary>
		private List<Node> GetUnspecifiedPlots()
		{
			var remaining = _nodes
				.Where(p => p.DateTime.Resolved == DateTime.MinValue)
				.ToList();

			return remaining;
		}

		private void Reset()
		{
			// Go through all the nodes and reset their plot types and date times.
			foreach (var node in _nodes)
			{
				// Reset the node type.
				if (node.NodeType != NodeType.Epilogue)
				{
					node.NodeType = NodeType.Narrative;
				}

				// Reset all the resolved date times to min unless we have a specified one.
				node.DateTime.Reset();
			}
		}

		/// <summary>
		/// Attempts to resolve the time for a given plot item.
		/// </summary>
		private bool Resolve(Node node)
		{
			// Get time about incoming and outgoing plots. If we don't have any, then we don't have to
			// worry about that side (it's a leaf plot and relatively speaking, rare).
			_log.Verbose("Resolving timestamp for {0}", node);

			var incoming = GetBeforePlotLinksInfo(node);
			var outgoing = GetAfterPlotLinksInfo(node);

			if (!incoming.HasAllDateTimes && !outgoing.HasAllDateTimes)
			{
				// We couldn't fully resolve the time.
				return false;
			}

			// Pick one or the other.
			if (incoming.HasAllDateTimes && outgoing.HasAllDateTimes)
			{
				node.DateTime.Resolved = outgoing.DateTime;
			}
			else if (incoming.HasAllDateTimes)
			{
				node.DateTime.Resolved = incoming.DateTime;
			}
			else
			{
				node.DateTime.Resolved = outgoing.DateTime;
			}

			// Because we have a date time, we are resolved.
			return true;
		}

		private class LinkInfo
		{
			public LinkInfo(bool isDirect, DateTime dateTime, Node node)
			{
				IsDirect = isDirect;
				DateTime = dateTime;
				Node = node;
			}

			public DateTime DateTime { get; }

			public int GraphDepth => Node.GraphDepth;

			public bool IsDirect { get; }

			public Node Node { get; }

			public override string ToString()
			{
				return $"{(IsDirect ? "Direct" : "Indirect")} {DateTime:yyyy-MM-dd HH:mm:ss}";
			}
		}

		private class PlotLinksInfo
		{
			public int Count { get; set; }

			public DateTime DateTime { get; set; }

			public int GraphDepth => Links.Values.Max(l => l.GraphDepth);

			/// <summary>
			/// Gets a flag whether every link on the side have been specified.
			/// </summary>
			public bool HasAllDateTimes => Links.Count > 0 && Links.Values.All(l => l.DateTime != DateTime.MinValue);

			/// <summary>
			/// Gets a flag whether at least one link on the side has been specified.
			/// </summary>
			public bool HasOneDirectSpecified
				=> Links.Count > 0 && Links.Values.Any(l => l.IsDirect && l.DateTime != DateTime.MinValue);

			public Dictionary<string, LinkInfo> Links { get; } = new Dictionary<string, LinkInfo>();
		}
	}
}
