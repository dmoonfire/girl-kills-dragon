#region Namespaces

using GirlKillsDragon.Locations;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Resolvers
{
	/// <summary>
	/// Creates countries based on the distance between locations.
	/// </summary>
	public class CountryResolver : IPlotResolver
	{
		private readonly CountryManager _countries;

		private readonly LocationDistanceResolver _locationDistanceResolver;

		private readonly LocationManager _locations;

		private readonly ILogger _log;

		public CountryResolver(
			ILogger log,
			CountryManager countries,
			LocationManager locations,
			LocationDistanceResolver locationDistanceResolver)
		{
			_log = log;
			_countries = countries;
			_locations = locations;
			_locationDistanceResolver = locationDistanceResolver;
		}

		public int ResolverOrder => _locationDistanceResolver.ResolverOrder + 1;

		public void Resolve()
		{
			foreach (var location in _locations)
			{
				if (!location.Distance.HasValue)
				{
					_log.Error("Location {0} does not have a distance", location);
					continue;
				}

				location.Country = _countries.Get(location.Distance.Value);
			}
		}
	}
}
