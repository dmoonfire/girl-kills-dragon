#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Extensions;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Plots.Injectors;
using GirlKillsDragon.Plots.Resolvers;
using GirlKillsDragon.Plots.Seeds;
using GirlKillsDragon.Plots.Validators;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots
{
	public class PlotInjection
	{
		private readonly Chaos _chaos;

		private readonly NodeGraph _graph;

		private readonly List<PlotInjectorBase> _injectors;

		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		private readonly Project _project;

		private readonly ResolverManager _resolvers;

		private readonly List<PlotSeedBase> _seeds;

		private readonly ValidatorManager _validators;

		public PlotInjection(
			ILogger log,
			Project project,
			NodeManager nodes,
			NodeGraph graph,
			ResolverManager resolvers,
			Chaos chaos,
			IEnumerable<PlotSeedBase> seeds,
			IEnumerable<PlotInjectorBase> injectors,
			ValidatorManager validators)
		{
			_log = log;
			_project = project;
			_nodes = nodes;
			_graph = graph;
			_resolvers = resolvers;
			_chaos = chaos;
			_validators = validators;
			_seeds = seeds.ToList();
			_injectors = injectors.ToList();

			if (_seeds.Count == 0)
			{
				throw new InvalidOperationException("Cannot find any plot seeds.");
			}

			if (_injectors.Count == 0)
			{
				throw new InvalidOperationException("Cannot find any plot injectors.");
			}
		}

		public bool Inject()
		{
			// Report how much complexity we are aiming for.
			_log.Information(
				"Injecting nodes to bring {0:N0} to {1:N0}",
				_nodes.NarrativeCount,
				_project.NarrativeCount);

			// We want to avoid reusing plot items, so we don't repeat an
			// injector too often. Our threshold is 50% of the total injections.
			var avoidRepeatingInjectorCount = _injectors.Count / 2;
			var previous = new List<PlotInjectorBase>();

			// Loop until we've injected enough plot items. We use `plotId` mainly so we can create
			// breakpoints on a specific step.
			var nextReport = DateTime.UtcNow.AddSeconds(15);
			var plotId = 0;

			while (_nodes.NarrativeCount < _project.NarrativeCount)
			{
				// See if we should make some noise.
				if (DateTime.UtcNow > nextReport)
				{
					_log.Information("Still injecting: {0}", _nodes.ToString());
					nextReport = DateTime.UtcNow.AddSeconds(15);
				}

				// We start by picking a random injector. If we've seen it recently, then we
				// skip it because we don't want to inject 13 fights in a row.
				// Pick a random injector from the list.
				var injector = _injectors.Choose(_chaos);

				if (previous.Contains(injector))
				{
					continue;
				}

				// Because some of the injectors are rather specific in where they can go, we
				// make a number of attempts to find a place this specific injector can be used.
				const int findValidLinkAttempts = 1024;

				for (var attempt = 0; attempt < findValidLinkAttempts; attempt++)
				{
					// Filter to find a node that has at least one outgoing (no link) and then pick a
					// random link in that node.
					var links = _nodes
						.Where(p => p.OutEdges.Count > 0)
						.ToList()
						.Choose(_chaos)
						.OutEdges
						.Filter(_graph, injector.EdgeFilter)
						.ToList();

					if (links.Count == 0)
					{
						continue;
					}

					// We have at least one good link, so pick it.
					var link = links.Choose(_chaos);

					// Attempt to inject the plots into the list. If the injector
					// can't, then it won't do anything.
					plotId++;

					var successful = injector.Inject(plotId, link);

					if (!successful)
					{
						continue;
					}

					// Since we've used the injector, add it to the list so we don't
					// reuse it too frequently. Then, trim down the list if we have
					// too many entries.
					previous.Add(injector);

					while (previous.Count > avoidRepeatingInjectorCount)
					{
						previous.RemoveAt(0);
					}

					// Resolve the injection-level processes.
					_resolvers.ResolveInjection();

					// Verify we have a valid state.
					if (!_validators.Validate())
					{
						_log.Error(
							"Could not validate the state after injecting {1} as plot {0}",
							plotId,
							injector.GetType().Name);
						return false;
					}

					// We successfully used it once, so break out of the retry loop.
					break;
				}
			}

			// We were successful.
			_log.Information("Finished injecting: {0}", _nodes.ToString());
			return true;
		}

		/// <summary>
		/// Creates the initial plots for the story.
		/// </summary>
		public void Seed()
		{
			var seed = _chaos.Choose(_seeds);

			seed.Seed();
		}
	}
}
