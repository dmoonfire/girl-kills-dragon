#region Namespaces

using System.Linq;
using GirlKillsDragon.Nodes;
using Serilog;

#endregion

namespace GirlKillsDragon.Plots.Validators
{
	/// <summary>
	/// A validator that ensures that all the incoming edges for a node
	/// happen before the current node. It also checks before nodes.
	/// </summary>
	public class NodeDateTimeOrderingValidator : IValidator
	{
		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		public NodeDateTimeOrderingValidator(
			ILogger log,
			NodeManager nodes)
		{
			_log = log;
			_nodes = nodes;
		}

		public bool Validate()
		{
			// Go through each of the nodes in the plot tree. We don't have to
			// use a visitor for this because we have another validator that
			// ensures connectivity.
			var errors = false;

			foreach (var node in _nodes)
			{
				// Go through the edges. We only look at the heads of these
				// edges because we'll get the tails later.
				var invalids = node
					.InNodes
					.Where(i => i.DateTime.Specific >= node.DateTime.Specific)
					.ToList();

				foreach (var invalid in invalids)
				{
					_log.Error("{0} happens before direct in {1}", node, invalid);
					errors = true;
				}

				// We also go through the after plots with the same rules.
				invalids = node
					.AfterPlots
					.Where(i => i.DateTime.Specific >= node.DateTime.Specific)
					.ToList();

				foreach (var invalid in invalids)
				{
					_log.Error("{0} happens before order in {1}", node, invalid);
					errors = true;
				}
			}

			// Return our error state.
			return !errors;
		}
	}
}
