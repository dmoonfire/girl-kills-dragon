namespace GirlKillsDragon.Plots.Validators
{
	/// <summary>
	/// Common signature for something that validates the project.
	/// </summary>
	public interface IValidator
	{
		bool Validate();
	}
}
