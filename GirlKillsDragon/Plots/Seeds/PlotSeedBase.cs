namespace GirlKillsDragon.Plots.Seeds
{
	/// <summary>
	/// Defines the common interface of something that generates a random starting point for the plot.
	/// </summary>
	public abstract class PlotSeedBase : PlotFactoryBase
	{
		/// <summary>
		/// Seeds a given project with the initial plots.
		/// </summary>
		public abstract void Seed();
	}
}
