#region Namespaces

using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	/// <summary>
	/// Injects a random fight into the story.
	/// </summary>
	public class RandomFightPlotInjector : PlotInjectorBase
	{
		public override bool Inject(int plotId, Edge edge)
		{
			// Introduce an opponent to pick a fight.
			var opponent = Actors.Create();

			Log.Debug("{1}: Random Fight: {0}", opponent, edge);

			// We use the arrive plot to have the opponent show up from nowhere.
			// This avoids wasting the effort to build up their backstory (these
			// are mooks). Once they are in the location, they pick a fight.
			var bornLocation = Locations.Create();
			var location = edge.TailNode.Location;
			var born = new BornNode(bornLocation, opponent)
			{
				NodeType = NodeType.Past
			};
			var leave = new LeaveNode(bornLocation)
			{
				NodeType = NodeType.Past
			};
			var introduce = new ArriveNode(location);
			var beginFight = new BeginFightNode(location);
			var death = new DeathNode(location, opponent);
			var endFight = new EndFightNode(location)
				.AddAfterPlot(death)
				.AddBeforePlot(edge.TailNode);

			Nodes.Add(born, leave, introduce, beginFight, endFight, death);

			// Remove the existing link because we're going to rebuild it.
			Graph.Disconnect(edge);

			// Inject the next link into the mess. The 
			Graph.Connect(born, leave, opponent);
			Graph.Connect(leave, introduce, opponent);
			Graph.Connect(introduce, beginFight, opponent);
			Graph.Connect(edge.HeadNode, beginFight, edge.Actors);
			Graph.Connect(beginFight, death, opponent);
			Graph.Connect(beginFight, endFight, edge.Actors);
			Graph.Connect(endFight, edge.TailNode, edge.Actors);

			// We are successful.
			return true;
		}
	}
}
