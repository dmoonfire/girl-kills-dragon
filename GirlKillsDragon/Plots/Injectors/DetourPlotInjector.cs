#region Namespaces

using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	/// <summary>
	/// Injects a detour between a leave/arrival pair of plots.
	/// </summary>
	public class DetourPlotInjector : PlotInjectorBase
	{
		public override EdgeFilter EdgeFilter => new EdgeFilter
		{
			HeadFilter = new NodeFilter
			{
				RequireLeave = true
			},
			TailFilter = new NodeFilter
			{
				RequireArrive = true
			}
		};

		public override bool Inject(int plotId, Edge edge)
		{
			// We can insert a detour into the story. To do so, we need a new
			// location to visit.
			var leave = (LeaveNode) edge.HeadNode;
			var arrive = (ArriveNode) edge.TailNode;
			var location = Locations.Create();

			// Create a new pair of arrive and leave pairs.
			var newArrive = new ArriveNode(location);
			var newLeave = new LeaveNode(location);

			Nodes.Add(newArrive, newLeave);

			// Figure out the travel times between these locations.
			var leaveToNewArriveTime = Locations.GetTravelTime(leave.Location, location);
			var newLeaveToArriveTime = Locations.GetTravelTime(location, arrive.Location);

			// Make some noise.
			Log.Debug(
				"{3}: Detour: {0} -> {1} -> {2}",
				leave.Location,
				location,
				arrive.Location,
				edge);

			// Remove the existing link because we're going to rebuild it.
			Graph.Disconnect(edge);

			// Inject the next link into the mess.
			Graph.Connect(leave, newArrive, leaveToNewArriveTime, edge.Actors);
			Graph.Connect(newArrive, newLeave, edge.Actors);
			Graph.Connect(newLeave, arrive, newLeaveToArriveTime, edge.Actors);

			// We are successful.
			return true;
		}
	}
}
