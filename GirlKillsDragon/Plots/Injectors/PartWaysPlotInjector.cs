#region Namespaces

using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Extensions;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	/// <summary>
	/// Creates a plot where a secondary character goes their separate ways.
	/// </summary>
	public class PartWaysPlotInjector : PlotInjectorBase
	{
		private ActorFilter ActorFilter => new ActorFilter();

		public override EdgeFilter EdgeFilter => new EdgeFilter
			{
				HeadFilter = NodeFilter.IncludeAll,
				TailFilter = NodeFilter.IncludeAll
					.SetIsFiltered(node => !(node is LeaveNode))
			}
			.SetIsFiltered(edge => GetActors(edge).Count == 0);

		private ActorCollection GetActors(Edge edge)
		{
			var actors = edge.HeadNode.InActors
				.Where(a => !edge.TailNode.InActors.Contains(a))
				.Filter(ActorFilter)
				.ToList();

			return new ActorCollection(actors);
		}

		public override bool Inject(int plotId, Edge edge)
		{
			// Figure out which actor is leave.
			var leavingActor = edge.TailNode.InActors;
			var actors = GetActors(edge);
			var partingActor = Chaos.Choose(actors);

			// This person is going to head to a new location.
			var location = Locations.Create();
			var leave = new LeaveNode(edge.HeadNode.Location)
				.AddBeforePlot(edge.TailNode);
			var arrive = new ArriveNode(location);
			var exit = new ExitNode(location, partingActor);

			Nodes.Add(leave, arrive, exit);

			// Make some noise.
			Log.Debug(
				"{0}: Part Ways: When {1} leave, so does {2}",
				edge,
				leavingActor.Names.OxfordAndJoin(),
				partingActor);

			// Insert the new parting node with new location.
			Graph.Remove(partingActor, edge.HeadNode);
			Graph.Disconnect(edge);
			Graph.Connect(edge.HeadNode, leave, partingActor);
			Graph.Connect(leave, arrive, partingActor);
			Graph.Connect(arrive, exit, partingActor);
			Graph.Connect(edge.HeadNode, edge.TailNode, leavingActor);

			// Let's keep on going.
			return true;
		}
	}
}
