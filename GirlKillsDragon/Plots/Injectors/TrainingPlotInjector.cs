#region Namespaces

using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	public class TrainingPlotInjector
		: InsertNodePlotInjectorBase<BeginTrainingNode, EndTrainingNode>
	{
		public override string PluralMessage => "train together";

		public override string SingularMessage => "trains alone";
	}
}
