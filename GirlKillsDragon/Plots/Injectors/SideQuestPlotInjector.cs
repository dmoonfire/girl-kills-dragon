#region Namespaces

using GirlKillsDragon.Extensions;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	/// <summary>
	/// Injects a detour where the actors go to a new location and then come back.
	/// </summary>
	public class SideQuestPlotInjector : PlotInjectorBase
	{
		public override bool Inject(int plotId, Edge edge)
		{
			// We need a new location to insert a side quest. We don't have to worry about "rejoining" later because the
			// later injections will add that comlexity.
			var newLocation = Locations.Create();
			var oldLocation = edge.HeadNode.Location;

			Log.Debug(
				"{3}: Side Quest: {2} leave on a brief side quest: {0} -> {1} -> {0}",
				oldLocation,
				newLocation,
				edge.ActorNames.OxfordAndJoin(),
				edge);

			// We have to leave the current location, arrive and then leave the new one, and then arrive back at the original.
			var oldLeave = new LeaveNode(oldLocation);
			var newArrive = new ArriveNode(newLocation);
			var newLeave = new LeaveNode(newLocation);
			var oldArrive = new ArriveNode(oldLocation);

			Nodes.Add(oldLeave, newArrive, newLeave, oldArrive);

			// Figure out the travel times between these locations.
			var travelTime = Locations.GetTravelTime(oldLocation, newLocation);

			// Remove the existing link because we're going to rebuild it.
			Graph.Disconnect(edge);

			// Inject the next link into the mess.
			Graph.Connect(edge.HeadNode, oldLeave, edge.Actors);
			Graph.Connect(oldLeave, newArrive, travelTime, edge.Actors);
			Graph.Connect(newArrive, newLeave, travelTime, edge.Actors);
			Graph.Connect(newLeave, oldArrive, edge.Actors);
			Graph.Connect(oldArrive, edge.TailNode, edge.Actors);

			// We are successful.
			return true;
		}
	}
}
