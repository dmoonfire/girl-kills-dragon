#region Namespaces

using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	public class RelationshipPlotInjector
		: InsertNodePlotInjectorBase<BeginRelationshipNode, EndRelationshipNode>
	{
		public override string PluralMessage => "spends time together";

		public override string SingularMessage => "spends time alone";
	}
}
