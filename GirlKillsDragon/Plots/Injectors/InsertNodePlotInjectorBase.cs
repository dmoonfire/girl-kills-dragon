#region Namespaces

using System;
using System.Linq;
using GirlKillsDragon.Extensions;
using GirlKillsDragon.Nodes;

#endregion

namespace GirlKillsDragon.Plots.Injectors
{
	public abstract class InsertNodePlotInjectorBase<TBegin, TEnd> : PlotInjectorBase
		where TBegin : Node, new()
		where TEnd : Node, new()
	{
		public abstract string PluralMessage { get; }

		public abstract string SingularMessage { get; }

		public override bool Inject(int plotId, Edge edge)
		{
			// We are not going to inject if the head of the link is the same as the tail
			// of our link and the reverse. This avoids multiple trials after each other.
			if (edge.HeadNode.GetType() == typeof(TEnd) || edge.TailNode.GetType() == typeof(TBegin))
			{
				return false;
			}

			// If we are in a fight, we can't merge these.
			if (Graph.IsInFight(edge.HeadNode))
			{
				return false;
			}

			// We try not to have too many events in the same location. Look for the same type
			// of beginning node in the current location. If we find it, then skip it.
			var likeNodesAtLocation = Nodes.OfType<TBegin>().Count(n => n.Location == edge.HeadNode.Location);

			if (likeNodesAtLocation > 0)
			{
				return false;
			}

			// Develop a relationship.
			var actors = edge.Actors;

			Log.Debug(
				"{2}: Insert pair: {0} {1}",
				actors.Names.OxfordAndJoin(),
				actors.Count == 1 ? SingularMessage : PluralMessage,
				edge);

			// We just insert a relationship node between these.
			var incoming = edge.HeadNode;
			var begin = new TBegin
			{
				Location = incoming.Location,
				CanInjectAfter = false
			};
			var end = new TEnd
			{
				Location = incoming.Location
			};

			Nodes.Add(begin, end);

			// Remove the existing link because we're going to rebuild it.
			Graph.Disconnect(edge);

			// Figure out the time to perform these actions.
			var afterTime = TimeSpan.FromHours(1 + Chaos.NextDouble() * 12);
			var afterPlotTime = new PlotTimeSpan(afterTime);

			// Inject the next link into the graph. We only include the original party until we use
			// the Graph to merge the group following the main character.
			Graph.Connect(incoming, begin, actors);
			Graph.Connect(begin, end, afterPlotTime, actors);
			Graph.Connect(end, edge.TailNode, actors);

			// We are successful.
			return true;
		}
	}
}
