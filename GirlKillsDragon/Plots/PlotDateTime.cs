#region Namespaces

using System;

#endregion

namespace GirlKillsDragon.Plots
{
	/// <summary>
	/// Represents the time for a specific plot item which may be concrete (or
	/// specific) or relative to other elements.
	/// </summary>
	public class PlotDateTime
	{
		public PlotDateTime()
		{
		}

		public PlotDateTime(DateTime specific)
		{
			Specific = specific;
			Resolved = Specific ?? DateTime.MinValue;
		}

		/// <summary>
		/// Contains the resolved date time, if there is one.
		/// </summary>
		public DateTime Resolved { get; set; }

		/// <summary>
		/// Gets the specific time, if any, associated with this time.
		/// </summary>
		public DateTime? Specific { get; set; }

		public string TimeOfDay
		{
			get
			{
				if (Resolved == DateTime.MinValue)
				{
					return null;
				}

				switch (Resolved.Hour)
				{
					case 0: return "midnight";
					case 1:
					case 2:
					case 3:
						return "very early morning";
					case 4:
					case 5:
					case 6:
						return "early morning";
					case 7:
					case 8:
					case 9:
						return "morning";
					case 10:
					case 11:
						return "late morning";
					case 12:
						return "noon";
					case 13:
					case 14:
						return "early afternoon";
					case 15:
					case 16:
						return "afternoon";
					case 17:
					case 18:
						return "early evening";
					case 19:
					case 20:
						return "evening";
					case 21:
					case 22:
						return "late evening";
					case 23:
						return "near midnight";
					default:
						return string.Format("hour {0:D2}", Specific.Value.Hour);
				}
			}
		}

		public void Reset()
		{
			Resolved = Specific ?? DateTime.MinValue;
		}

		public override string ToString()
		{
			return $"{Specific:yyyy-MM-dd HH:mm:ss}";
		}
	}
}
