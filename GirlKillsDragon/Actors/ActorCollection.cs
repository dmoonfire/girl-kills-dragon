#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Actors
{
	/// <summary>
	/// A collection of actors that are combined together.
	/// </summary>
	public class ActorCollection : List<Actor>
	{
		public ActorCollection()
		{
		}

		public ActorCollection(IEnumerable<Actor> actors)
			: base(actors)
		{
		}

		public Actor MostSignificantActor => this.OrderBy(a => a).FirstOrDefault();

		public List<string> Names => this.OrderBy(a => a).Select(a => a.ToString()).ToList();

		/// <summary>
		/// Filters out the actors that don't match the given filter.
		/// </summary>
		public ActorCollection Filter(ActorFilter filter)
		{
			return new ActorCollection(this.Where(filter.Include));
		}
	}
}
