#region Namespaces

using System;

#endregion

namespace GirlKillsDragon.Actors
{
	/// <summary>
	/// Represents a faction or group of individuals with like-minded goals. A faction changes as the plot continues so it
	/// has to be calculated using the `NodeGraph` for any given time.
	/// </summary>
	public class Faction
		: IEquatable<Faction>
	{
		public Faction(Actor leader)
		{
			Leader = leader;
		}

		public Actor Leader { get; set; }

		public bool Equals(Faction other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}
			if (ReferenceEquals(this, other))
			{
				return true;
			}

			return Equals(Leader, other.Leader);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}
			if (ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != GetType())
			{
				return false;
			}

			return Equals((Faction) obj);
		}

		public override int GetHashCode()
		{
			return Leader != null ? Leader.GetHashCode() : 0;
		}

		public static bool operator ==(Faction left, Faction right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(Faction left, Faction right)
		{
			return !Equals(left, right);
		}

		public override string ToString()
		{
			return $"Faction({Leader})";
		}
	}
}
