#region Namespaces

using System.Collections.Generic;
using System.IO;
using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Extensions;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using Humanizer;
using Serilog;

#endregion

namespace GirlKillsDragon.Writing
{
	public class Author
	{
		private readonly NodeGraph _graph;

		private readonly ILogger _log;

		private readonly HashSet<Actor> _namedActors = new HashSet<Actor>();

		private readonly HashSet<Actor> _seenActors = new HashSet<Actor>();

		private readonly HashSet<Location> _seenLocations = new HashSet<Location>();

		private Actor _lastNamed;

		public Author()
		{
		}

		public Author(
			ILogger log,
			NodeGraph graph)
		{
			_log = log;
			_graph = graph;
		}

		private string GetName(Actor actor)
		{
			string name;

			//if (actor == _lastNamed) {
			//	name = actor.Pronoun;
			//} else
			if (_namedActors.Contains(actor))
			{
				name = actor.FirstName;
			}
			else
			{
				_namedActors.Add(actor);
				name = actor.Name;
			}

			_lastNamed = actor;

			return name;
		}

		private string GetName(Country country)
		{
			return country.Name;
		}

		private string GetName(Location location)
		{
			return location.Name;
		}

		private string GetNames(IEnumerable<Actor> actors)
		{
			return GetNames(actors.ToArray());
		}

		private string GetNames(params Actor[] actors)
		{
			return actors.Select(GetName).OxfordAndJoin();
		}

		/// <summary>
		/// Figure out the detail we need to set the actor.
		/// </summary>
		public void WriteActor(ParagraphBuilder builder, Actor actor)
		{
			// Figure out how much detail is needed for the location.
			DetailLevel detailLevel;

			if (!_seenActors.Contains(actor))
			{
				detailLevel = DetailLevel.Detailed;
				builder.Add(
					"A {0} description of {1}. They are {2} years old. They were born at {3} in {4}.",
					detailLevel.ToString().ToLower(),
					GetName(actor),
					actor.Age.Days / 365,
					GetName(actor.Born.Location),
					GetName(actor.Country));
			}
			else
			{
				detailLevel = DetailLevel.Reminder;
				builder.Add(
					"A {0} description of {1}.",
					detailLevel.ToString().ToLower(),
					GetName(actor));
			}

			// We've seen them, so we want to keep track so we don't give heavy details again.
			_seenActors.Add(actor);
		}

		public void WriteAlreadyPresentActors(ParagraphBuilder builder, Chapter chapter)
		{
			// Gather up all the actors that are present in any of the nodes in this chapter.
			var actors = new ActorCollection();

			foreach (var node in chapter)
			{
				var nodeActors = _graph.GetPresentActors(node, chapter);

				actors.AddRange(nodeActors);
			}

			actors = new ActorCollection(actors.Distinct());

			// If we don't have any actors, skip them.
			if (actors.Count <= 0)
			{
				return;
			}

			// Write out who is already here.
			var names = GetNames(actors);

			builder.Add("{0} {1} already present.", names, actors.Count == 1 ? "is" : "are");

			// Describe these characters for the viewer.
			foreach (var actor in actors)
			{
				WriteActor(builder, actor);
			}
		}

		public void WriteArrival(TextWriter writer, ArriveNode node)
		{
			// Figure out where we are coming from.
			var link = node.InEdges[0];

			// Write out the output based on the departure point.
			var builder = new ParagraphBuilder();

			builder.Add(
				"{0} arrive at {1} from {2}, it took {3}.",
				GetNames(link.Actors),
				GetName(node.Location),
				GetName(link.HeadNode.Location),
				link.TimeSpan.Get().Humanize(2));

			// Describe these characters for the viewer.
			foreach (var actor in link.Actors)
			{
				WriteActor(builder, actor);
			}
		}

		public void WriteBeginFight(TextWriter writer, BeginFightNode node)
		{
			// We need to know everyone who is here and who is fighting for who.
			var actors = node.InActors;
			var actorFactions = actors.ToDictionary(a => a, a => _graph.GetFactions(node, a).First());
			var factions = actorFactions.Values.Distinct().OrderBy(f => f.Leader.ActorId).ToList();
			var factionActors = factions.ToDictionary(
				f => f,
				f => actorFactions.Where(p => p.Value == f).Select(p => p.Key).OrderBy(a => a.ActorId).ToList());

			// Build up the message of what we're doing.
			var builder = new ParagraphBuilder();

			builder.Add("{0} begin to fight.", GetNames(node.InActors));

			foreach (var faction in factions)
			{
				// Figure who is fighting with whom.
				var factionFighters = factionActors[faction];
				var first = factionFighters.First();
				var other = factionFighters.Skip(1).ToList();

				if (other.Count == 0)
				{
					builder.Add(" {0} fights alone.", GetName(first));
				}
				else
				{
					builder.Add(
						" {0} fights along with {1}.",
						GetName(first),
						GetNames(other));
				}
			}

			// Write out the paragraph.
			builder.Write(writer);
		}

		public void WriteBeginRelationship(TextWriter writer, BeginRelationshipNode node)
		{
			var link = node.InEdges[0];

			ParagraphBuilder.Write(
				writer,
				"{0} begin some time {1}.",
				GetNames(link.Actors),
				link.Actors.Count == 1 ? "alone" : "together");
		}

		public void WriteBeginTraining(TextWriter writer, BeginTrainingNode node)
		{
			var link = node.InEdges[0];

			ParagraphBuilder.Write(
				writer,
				"{0} {1}.",
				GetNames(link.Actors),
				link.Actors.Count == 1 ? "starts to train alone" : "start to train together");
		}

		public void WriteBeginTrial(TextWriter writer, BeginTrialNode node)
		{
			var link = node.InEdges[0];

			ParagraphBuilder.Write(
				writer,
				"{0} {1} a trial.",
				GetNames(link.Actors),
				link.Actors.Count == 1 ? "begins" : "begin");
		}

		/// <summary>
		/// Writes out the beginning of the chapter.
		/// </summary>
		public void WriteChapter(
			TextWriter writer,
			Chapter chapter)
		{
			// We simply number the chapters starting at one (but chapterIndex
			// is zero based).
			var chapterTitle = chapter.Number.ToWords().Titleize();

			// Write out the chapter in Markdown using underline-style headers
			writer.WriteLine();
			writer.WriteLine($"# {chapterTitle}");

			// We need to introduce the scene.
			var builder = new ParagraphBuilder();

			WritePointOfView(builder, chapter);
			WriteLocation(builder, chapter);
			WriteAlreadyPresentActors(builder, chapter);

			builder.Write(writer);

			// Write out each of the scenes in the chapter.
			foreach (var plot in chapter)
			{
				switch (plot)
				{
					case ArriveNode x:
						WriteArrival(writer, x);
						break;

					case LeaveNode x:
						WriteLeave(writer, x);
						break;

					case DeathNode x:
						WriteDeath(writer, x);
						break;

					case BeginFightNode x:
						WriteBeginFight(writer, x);
						break;

					case EndFightNode x:
						WriteEndFight(writer, x);
						break;

					case BeginTrialNode x:
						WriteBeginTrial(writer, x);
						break;

					case EndTrialNode x:
						WriteEndTrial(writer, x);
						break;

					case BeginTrainingNode x:
						WriteBeginTraining(writer, x);
						break;

					case EndTrainingNode x:
						WriteEndTraining(writer, x);
						break;

					case BeginRelationshipNode x:
						WriteBeginRelationship(writer, x);
						break;

					case EndRelationshipNode x:
						WriteEndRelationship(writer, x);
						break;

					case MergeGroupNode x:
						WriteMergeGroup(writer, x);
						break;

					case IncitingNode x:
						WriteInciting(writer, x);
						break;

					case EpilogueNode x:
						WriteEpilogue(writer, x);
						break;

					case ExitNode x:
						WriteExit(writer, x);
						break;

					case var unknown:
						WriteUnknownScene(writer, unknown);
						break;
				}
			}
		}

		public void WriteDeath(TextWriter writer, DeathNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"{0} died.",
				GetName(node.Actor));
		}

		public void WriteEndFight(TextWriter writer, EndFightNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"{0} finish fighting.",
				GetNames(node.InActors));
		}

		public void WriteEndRelationship(TextWriter writer, EndRelationshipNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"{0} finish their time together.",
				GetNames(node.InActors));
		}

		public void WriteEndTraining(TextWriter writer, EndTrainingNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"{0} {1} {2} training.",
				GetNames(node.InActors),
				node.InActors.Count == 1 ? "finishes" : "finish",
				node.InActors.Count == 1 ? node.InActors[0].Noun.GetValue("possessive") : "their");
		}

		public void WriteEndTrial(TextWriter writer, EndTrialNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"{0} {1} {2} trial.",
				GetNames(node.InActors),
				node.InActors.Count == 1 ? "finishes" : "finish",
				node.InActors.Count == 1 ? node.InActors[0].Noun.GetValue("possessive") : "their");
		}

		public void WriteEpilogue(TextWriter writer, EpilogueNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"THE END");
		}

		public void WriteExit(TextWriter writer, ExitNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"{0} leaves.",
				GetNames(node.InActors));
		}

		public void WriteInciting(TextWriter writer, IncitingNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"Something terrible happened.");
		}

		public void WriteLeave(TextWriter writer, LeaveNode node)
		{
			// Figure out where we are arriving from.
			var link = node.OutEdges[0];

			// Write out the output based on the departure point.
			ParagraphBuilder.Write(
				writer,
				"{0} {3} {1} on the route to {2}.",
				GetNames(link.Actors),
				GetName(node.Location),
				GetName(link.TailNode.Location),
				link.Actors.Count == 1 ? "leaves" : "leave");
		}

		/// <summary>
		/// Figure out the detail we need to set the location.
		/// </summary>
		public void WriteLocation(ParagraphBuilder builder, Chapter chapter)
		{
			// Figure out how much detail is needed for the location.
			DetailLevel detailLevel;

			if (!_seenLocations.Contains(chapter.Location))
			{
				detailLevel = DetailLevel.Detailed;
				builder.Add(
					"{0} is {1:N2} km away from the start of the novel. It is located in {2} and it is a {3}.",
					GetName(chapter.Location),
					chapter.Location.Distance / 1000,
					GetName(chapter.Location.Country),
					chapter.Location.Terrain);
			}
			else if (chapter.LocationChanged)
			{
				detailLevel = DetailLevel.Medium;
			}
			else
			{
				detailLevel = DetailLevel.Reminder;
			}

			_seenLocations.Add(chapter.Location);

			// Write out the line.
			builder.Add(
				"A {0} description of {1} during {2}.",
				detailLevel.ToString().ToLower(),
				GetName(chapter.Location),
				chapter.DateTime.TimeOfDay);
		}

		public void WriteMergeGroup(TextWriter writer, MergeGroupNode node)
		{
			ParagraphBuilder.Write(
				writer,
				"{0} joins {1}.",
				GetNames(node.Joining),
				GetNames(node.Group));
		}

		public void WritePointOfView(ParagraphBuilder builder, Chapter chapter)
		{
			builder.Add(
				"This chapter is from {0}'s point of view.",
				GetName(chapter.Actor));
		}

		public void WriteUnknownScene(TextWriter writer, Node node)
		{
			_log.Error("Don't know how to write node: {0}", node);

			ParagraphBuilder.Write(
				writer,
				"Writing " + node + " at " + node.DateTime.Resolved.ToString("yyyy-MM-dd HH:mm:ss"));
		}
	}
}
