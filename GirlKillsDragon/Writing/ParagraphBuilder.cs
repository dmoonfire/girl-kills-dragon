#region Namespaces

using System.Collections.Generic;
using System.IO;
using System.Text;

#endregion

namespace GirlKillsDragon.Writing
{
	public class ParagraphBuilder
	{
		private readonly List<string> _sentences = new List<string>();

		public ParagraphBuilder()
		{
		}

		public ParagraphBuilder(string format, params object[] args)
			: this()
		{
			Add(format, args);
		}

		public void Add(string format, params object[] args)
		{
			_sentences.Add(string.Format(format, args));
		}

		public override string ToString()
		{
			return ToString(73);
		}

		public string ToString(int columns)
		{
			// Build up all the strings into a single buffer so we can do
			// word wrapping.
			var sentences = string.Join(" ", _sentences).Trim();
			var buffer = new StringBuilder(sentences);
			var results = new StringBuilder();

			results.Append("\n");

			while (buffer.Length > columns)
			{
				// Find the break character.
				for (var i = columns; i > 0; i--)
				{
					if (char.IsWhiteSpace(buffer[i]))
					{
						var line = buffer.ToString(0, i).Trim();

						buffer = new StringBuilder(
							buffer.ToString().Substring(i).Trim());

						results.Append(line);
						results.Append("\n");
						break;
					}
				}
			}

			// If we still have some left, then add it. Then return the results.
			results.Append(buffer);

			return results.ToString();
		}

		public static void Write(
			TextWriter writer,
			string format,
			params object[] args)
		{
			var builder = new ParagraphBuilder(format, args);

			builder.Write(writer);
		}

		public void Write(TextWriter writer)
		{
			writer.Write(ToString());
			writer.Write("\n");
		}
	}
}
