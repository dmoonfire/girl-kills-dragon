#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Extensions
{
	public static class EnumerableExtensions
	{
		public static string OxfordAndJoin(this IEnumerable<string> items)
		{
			// If we have zero items, return a null.
			var list = items?.ToList();

			if (list == null || list.Count == 0)
			{
				return null;
			}

			// If we have just the one item, then return it directly.
			if (list.Count == 1)
			{
				return list[0];
			}

			// If we have two items, join them together.
			if (list.Count == 2)
			{
				return $"{list[0]} and {list[1]}";
			}

			// For everything else, make it an Oxford list.
			list[list.Count - 1] = "and " + list[list.Count - 1];

			return string.Join(", ", list);
		}
	}
}
