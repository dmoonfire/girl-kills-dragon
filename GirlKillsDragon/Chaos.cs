#region Namespaces

using System.Collections.Generic;
using GirlKillsDragon.Extensions;
using MathNet.Numerics.Random;

#endregion

namespace GirlKillsDragon
{
	/// <summary>
	/// Provides a consistent randomizer that is driven by the project file.
	/// </summary>
	public class Chaos
	{
		private MersenneTwister _random = new MersenneTwister(13);

		public T Choose<T>(IList<T> items)
		{
			return items.Choose(_random);
		}

		public int Next(int minimum, int maximum)
		{
			return _random.Next(minimum, maximum);
		}

		public double NextDouble()
		{
			return _random.NextDouble();
		}

		public void SetSeed(int seed)
		{
			_random = new MersenneTwister(seed);
		}
	}
}
