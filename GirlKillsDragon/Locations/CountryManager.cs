#region Namespaces

using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Details;
using GirlKillsDragon.Extensions;
using Serilog;

#endregion

namespace GirlKillsDragon.Locations
{
	public class CountryManager : IRegisteredManager
	{
		private readonly Chaos _chaos;

		private readonly List<Country> _countries = new List<Country>();

		private readonly MarkovChain _countriesChain;

		private readonly FileSystem _fileSystem;

		private readonly ILogger _log;

		public CountryManager(
			ILogger log,
			Chaos chaos,
			FileSystem fileSystem)
		{
			// Save the variables.
			_log = log;
			_chaos = chaos;
			_fileSystem = fileSystem;

			// Create a collection of country names.
			var countryFile = fileSystem.GetDataFile("countries.txt");
			var usLastFile = fileSystem.GetDataFile("en.last.txt");
			var jpLastFile = fileSystem.GetDataFile("jp.last.txt");

			_countriesChain = new MarkovChainFactory(_chaos)
				.SetIgnoreWeights()
				.SetWeight(5)
				.Append(usLastFile)
				.SetWeight(1)
				.Append(jpLastFile)
				.SetSingleToken()
				.SetForceUnique()
				.SetWeight(10)
				.Append(countryFile)
				.Create();

			// Create the first country.
			Create(0);
		}

		public int Count => _countries.Count;

		private Country LastCountry => _countries.Last();

		public Country Create()
		{
			var startMeters = LastCountry.StopMeters + 1;

			return Create(startMeters);
		}

		private Country Create(double startMeters)
		{
			// Create the country and add it to the list.
			var country = new Country(_countries.Count)
			{
				StartMeters = startMeters,
				LengthMeters = 1000 * (_chaos.NextDouble() * 300 + 100)
			};

			_countries.Add(country);

			// Name the country.
			var name = _countriesChain.Create();

			country.Noun.AddDetails("name", new Detail(DetailType.Is, name));

			// Figure out the language that this country is using.
			var languages = new List<string> {"en", "jp"};
			var language = "en";

			if (country.CountryId > 1)
			{
				language = languages.Choose(_chaos);
			}

			if (country.CountryId == 1 && language == "en")
			{
				language = "jp";
			}

			_log.Information(
				"Assigning {0} language to {1}: {2}",
				language,
				country,
				name);
			country.LoadMarkovChains(_chaos, _fileSystem, language);

			// Return the resulting country.
			return country;
		}

		public Country Get(double distance)
		{
			// See if we have a country that matches, then use that.
			var country = _countries.FirstOrDefault(c => c.StartMeters <= distance && c.StopMeters >= distance);

			if (country != null)
			{
				return country;
			}

			// Create a new country and try again.
			Create();

			return Get(distance);
		}
	}
}
