#region Namespaces

using GirlKillsDragon.Details;

#endregion

namespace GirlKillsDragon.Locations
{
	/// <summary>
	/// Defines an abstract country based on locations. Countries are based on distance from the
	/// starting point in a straight line (no countries from opposite directions).
	/// </summary>
	public class Country
	{
		public Country(int countryId)
		{
			CountryId = countryId;
			Noun = new Noun();
		}

		public int CountryId { get; }

		public MarkovChain FemaleMarkovChain { get; set; }

		public MarkovChain LastMarkovChain { get; set; }

		public double LengthMeters { get; set; }

		public MarkovChain MaleMarkovChain { get; set; }

		public string Name => Noun.GetValue("name");

		public Noun Noun { get; }

		public double StartKilometers => StartMeters / 1000;

		public double StartMeters { get; set; }

		public double StopKilometers => StopMeters / 1000;

		public double StopMeters => StartMeters + LengthMeters;

		public void LoadMarkovChains(
			Chaos chaos,
			FileSystem fileSystem,
			string language)
		{
			// Load the language chains.
			var chainFactory = new MarkovChainFactory(chaos);
			var lastFile = fileSystem.GetDataFile(language + ".last.txt");
			var femaleFile = fileSystem.GetDataFile(language + ".female.txt");
			var maleFile = fileSystem.GetDataFile(language + ".male.txt");

			LastMarkovChain = chainFactory.Create(lastFile);
			FemaleMarkovChain = chainFactory.Create(femaleFile);
			MaleMarkovChain = chainFactory.Create(maleFile);
		}

		public override string ToString()
		{
			return $"C{CountryId}({StartKilometers:N2} km to {StopKilometers:N2} km)";
		}
	}
}
