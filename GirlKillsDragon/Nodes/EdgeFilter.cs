#region Namespaces

#endregion

#region Namespaces

using System;
using GirlKillsDragon.Actors;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// Defines a filtering class that determines if a specific edge should be filtered it.
	/// </summary>
	public class EdgeFilter
	{
		public EdgeFilter(EdgeFilter filter)
		{
			HeadFilter = filter.HeadFilter;
			TailFilter = filter.TailFilter;
			IsFiltered = filter.IsFiltered;
		}

		public EdgeFilter()
		{
			HeadFilter = new NodeFilter();
			TailFilter = new NodeFilter();
		}

		public static EdgeFilter ExcludeAll => new EdgeFilter
		{
			IsFiltered = edge => true
		};

		public NodeFilter HeadFilter { get; set; }

		public static EdgeFilter IncludeAll => new EdgeFilter
		{
			HeadFilter = NodeFilter.IncludeAll,
			TailFilter = NodeFilter.IncludeAll
		};

		public static EdgeFilter InnerSceneFilter => new EdgeFilter
		{
			HeadFilter = new NodeFilter
			{
				DenyLeave = true,
				RequireCanInjectAfter = true
			},
			TailFilter = new NodeFilter()
		};

		/// <summary>
		/// Create a custom test function for the edge. If the return value is true, then
		/// the item is filtered out.
		/// </summary>
		public Func<Edge, bool> IsFiltered { get; set; } = edge => false;

		public NodeFilter TailFilter { get; set; }

		public bool Exclude(Edge edge)
		{
			return !Include(edge);
		}

		/// <summary>
		/// Creates an edge filter that follows a specific user. This defaults to the default settings.
		/// </summary>
		public static EdgeFilter Follow(Actor actor)
		{
			return new EdgeFilter
			{
				IsFiltered = edge => !edge.Actors.Contains(actor)
			};
		}

		public bool Include(Edge edge)
		{
			return !IsFiltered(edge)
				&& HeadFilter.Include(edge.HeadNode)
				&& TailFilter.Include(edge.TailNode);
		}

		public EdgeFilter SetIncludeAll()
		{
			HeadFilter = TailFilter = NodeFilter.IncludeAll;
			return this;
		}

		public EdgeFilter SetIsFiltered(Func<Edge, bool> isFiltered)
		{
			IsFiltered = isFiltered;
			return this;
		}
	}
}
