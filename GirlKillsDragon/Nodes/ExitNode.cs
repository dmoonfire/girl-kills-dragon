#region Namespaces

using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	public class ExitNode : ActorNodeBase
	{
		public ExitNode(Location location, Actor actor)
			: base(location, actor)
		{
		}

		public override bool IsExit => true;

		public override bool IsLeave => true;
	}
}
