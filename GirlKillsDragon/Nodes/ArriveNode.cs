#region Namespaces

using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// A plot where one or more characters arrive at a new location.
	/// </summary>
	public class ArriveNode : Node
	{
		public ArriveNode(Location location)
			: base(location)
		{
		}

		public override bool IsArrive => true;
	}
}
