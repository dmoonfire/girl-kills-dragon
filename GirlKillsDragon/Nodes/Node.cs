#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Plots;
using GirlKillsDragon.Writing;
using Newtonsoft.Json;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// Defines a single point in the story where something happens. Actors come in as one or more groups,
	/// have changes made to them, and then exit in one or more groups.
	/// </summary>
	public class Node
	{
		private static int _nextNodeId;

		public Node()
			: this(new Location(-1))
		{
		}

		public Node(Location location)
		{
			Location = location ?? throw new ArgumentNullException(nameof(location));
			NodeId = _nextNodeId++;
		}

		public List<int> AfterPlotIds => AfterPlots.Select(n => n.NodeId).ToList();

		[JsonIgnore]
		public List<Node> AfterPlots { get; set; } = new List<Node>();

		public List<int> BeforePlotIds => BeforePlots.Select(n => n.NodeId).ToList();

		[JsonIgnore]
		public List<Node> BeforePlots { get; set; } = new List<Node>();

		/// <summary>
		/// Gets or sets a flag whether plots can be injected after this one.
		/// </summary>
		public bool CanInjectAfter { get; set; } = true;

		/// <summary>
		/// Contains the chapter that this node is associated with.
		/// </summary>
		[JsonIgnore]
		public Chapter Chapter { get; set; }

		public int? ChapterNumber => Chapter?.Number;

		/// <summary>
		/// Gets or sets the relative or specific time for this plot.
		/// </summary>
		public PlotDateTime DateTime { get; set; } = new PlotDateTime();

		/// <summary>
		/// Gets the depth of the node in the graph.
		/// </summary>
		public int GraphDepth => InEdges.Count == 0 ? 0 : InEdges.Max(e => e.HeadNode.GraphDepth) + 1;

		public List<string> InActorNames =>
			InActors.Select(a => a.ToString()).ToList();

		[JsonIgnore]
		public ActorCollection InActors =>
			new ActorCollection(
				InEdges.SelectMany(p => p.Actors).Distinct().OrderBy(p => p));

		public EndpointEdgeCollection InEdges { get; } = new EndpointEdgeCollection(true);

		[JsonIgnore]
		public List<Node> InNodes => InEdges.Select(l => l.HeadNode).ToList();

		/// <summary>
		/// Gets a flag whether this node involves arriving in a location.
		/// </summary>
		public virtual bool IsArrive => false;

		public virtual bool IsEntry => false;

		public virtual bool IsExit => false;

		/// <summary>
		/// Gets or sets a flag whether this is the initial node of the plot.
		/// </summary>
		public bool IsInitial { get; set; }

		/// <summary>
		/// Gets a flag whether this node involves leaving a location.
		/// </summary>
		public virtual bool IsLeave => false;

		[JsonIgnore]
		public Location Location { get; set; }

		public int? LocationId => Location?.LocationId;

		public string NodeClass => GetType().Name;

		public int NodeId { get; }

		/// <summary>
		/// Describes the type of plot. This is used to determine if this plot
		/// is in the narrative or a prologue.
		/// </summary>
		public NodeType NodeType { get; set; }

		public EndpointEdgeCollection OutEdges { get; } = new EndpointEdgeCollection(false);

		[JsonIgnore]
		public List<Node> OutNodes => OutEdges.Select(l => l.TailNode).ToList();

		public Node AddAfterPlot(Node node)
		{
			node.AddBeforePlot(this);
			return this;
		}

		public Node AddBeforePlot(Node node)
		{
			BeforePlots.Add(node);
			BeforePlots = BeforePlots.Distinct().ToList();
			node.AfterPlots.Add(this);
			node.AfterPlots = node.AfterPlots.Distinct().ToList();
			return this;
		}

		public Edge GetOutLink(Actor actor)
		{
			return OutEdges.FirstOrDefault(l => l.Actors.Contains(actor));
		}

		public override string ToString()
		{
			return $"N{NodeId}({GetType().Name}@{Location})";
		}
	}
}
