#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Plots;
using Serilog;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// Contains the business logic for manipulating the plot graph in a
	/// consistent manner.
	/// </summary>
	public class NodeGraph
	{
		private readonly ILogger _log;

		private readonly NodeManager _nodes;

		public NodeGraph(
			ILogger log,
			NodeManager nodeManager)
		{
			_log = log;
			_nodes = nodeManager;
		}

		/// <summary>
		/// Adds the following character into all the plot links that are followed by "follow".
		/// </summary>
		/// <param name="node">The node to update.</param>
		/// <param name="follow">Which actor to follow.</param>
		/// <param name="following">Which actor is following.</param>
		public void ActorFollowsActor(Node node, Actor follow, Actor following)
		{
			var visitor = new GraphVisitor
			{
				BackwardEdgeFilter = EdgeFilter.ExcludeAll,
				ForwardEdgeFilter = EdgeFilter.Follow(follow).SetIncludeAll(),
				VisitForwardEdge = delegate(Edge edge) { edge.Actors.Add(following); }
			};

			visitor.Visit(this, node);
		}

		public Edge Connect(
			Node incomingNode,
			Node outgoingNode,
			IEnumerable<Actor> actors)
		{
			return Connect(
				incomingNode,
				outgoingNode,
				new PlotTimeSpan(),
				actors?.ToArray());
		}

		public Edge Connect(
			Node incomingNode,
			Node outgoingNode,
			params Actor[] actors)
		{
			return Connect(
				incomingNode,
				outgoingNode,
				new PlotTimeSpan(),
				actors);
		}

		public Edge Connect(
			Node incomingNode,
			Node outgoingNode,
			TimeSpan timeSpan,
			IEnumerable<Actor> actors)
		{
			return Connect(
				incomingNode,
				outgoingNode,
				timeSpan,
				actors?.ToArray());
		}

		public Edge Connect(
			Node incomingNode,
			Node outgoingNode,
			TimeSpan timeSpan,
			params Actor[] actors)
		{
			return Connect(
				incomingNode,
				outgoingNode,
				new PlotTimeSpan(timeSpan),
				actors);
		}

		public Edge Connect(
			Node incomingNode,
			Node outgoingNode,
			PlotTimeSpan timeSpan,
			IEnumerable<Actor> actors)
		{
			return Connect(incomingNode, outgoingNode, timeSpan, actors.ToArray());
		}

		public Edge Connect(
			Node incomingNode,
			Node outgoingNode,
			PlotTimeSpan timeSpan,
			params Actor[] actors)
		{
			// Add the nodes if we haven't already added them.
			_nodes.Add(incomingNode, outgoingNode);

			// Create a link between the nodes.
			var link = new Edge
			{
				HeadNode = incomingNode,
				TailNode = outgoingNode,
				TimeSpan = timeSpan,
				Actors = new ActorCollection(actors ?? new Actor[0])
			};

			incomingNode.OutEdges.Add(link);
			outgoingNode.InEdges.Add(link);

			_log.Verbose("Connect nodes: {0}", link);

			return link;
		}

		public IEnumerable<Edge> Connect(
			Node node1,
			Node node2,
			Node node3,
			params Actor[] actors)
		{
			return new[]
			{
				Connect(node1, node2, actors),
				Connect(node2, node3, actors)
			};
		}

		public IEnumerable<Edge> Connect(
			Node node1,
			Node node2,
			Node node3,
			Node node4,
			params Actor[] actors)
		{
			return new[]
			{
				Connect(node1, node2, actors),
				Connect(node2, node3, actors),
				Connect(node3, node4, actors)
			};
		}

		public IEnumerable<Edge> Connect(
			Node node1,
			Node node2,
			Node node3,
			Node node4,
			Node node5,
			params Actor[] actors)
		{
			return new[]
			{
				Connect(node1, node2, actors),
				Connect(node2, node3, actors),
				Connect(node3, node4, actors),
				Connect(node4, node5, actors)
			};
		}

		public void Disconnect(Edge edge)
		{
			_log.Verbose("Disconnect nodes: {0}", edge);

			edge.HeadNode.OutEdges.Remove(edge);
			edge.HeadNode.InEdges.Remove(edge);
			edge.TailNode.InEdges.Remove(edge);
			edge.TailNode.OutEdges.Remove(edge);
		}

		public List<Faction> GetFactions(Node initial, Actor actor)
		{
			// We start with a blank list of factions. Then we use a visitor to follow a character back to the beginning. We add
			// these to the end and then reverse the order.
			var factions = new List<Faction>();
			var visitor = new GraphVisitor
			{
				BackwardEdgeFilter = EdgeFilter.Follow(actor).SetIncludeAll(),
				ForwardEdgeFilter = EdgeFilter.ExcludeAll,
				VisitNode = delegate(Node node)
				{
					// If we have a born node, we always add the actors as their own faction.
					if (node is BornNode born)
					{
						factions.Add(new Faction(born.Actor));
					}

					// Merge nodes always favor the lowest ID.
					if (node is MergeGroupNode mergeGroup)
					{
						var newLeader = mergeGroup.InActors.OrderBy(a => a.ActorId).First();

						if (actor != newLeader)
						{
							factions.Add(new Faction(newLeader));
						}
					}
				}
			};

			visitor.Visit(this, initial);

			// Return the resulting factions.
			return factions;
		}

		public ActorCollection GetPresentActors(
			Node node,
			IEnumerable<Node> excludeNodes)
		{
			return GetPresentActors(node, excludeNodes.ToArray());
		}

		/// <summary>
		/// Retrieves all the actors that are present at a given point in the plot.
		/// </summary>
		public ActorCollection GetPresentActors(
			Node node,
			params Node[] excludeNodes)
		{
			// Get all the nodes before this point.
			var exclude = excludeNodes.ToList();
			var earlierNodes = _nodes
				.Where(p => p.Location == node.Location)
				.Where(p => p.DateTime.Resolved < node.DateTime.Resolved)
				.Where(p => !exclude.Contains(p))
				.OrderBy(p => p.DateTime.Resolved)
				.ToList();

			// Process through the list to figure out who is there and not there.
			var present = new List<Actor>();

			foreach (var earlier in earlierNodes)
			{
				present = SetPresentActors(earlier, present);
			}

			// Rebuild the list so it is in a consistent order and then return it.
			var actors = new ActorCollection(present.OrderBy(a => a.ActorId).Distinct());

			return actors;
		}

		/// <summary>
		/// Determines if the current node is inside a fight.
		/// </summary>
		public bool IsInFight(Node node)
		{
			// Set up a visitor to go back and look for a fight.
			var inFight = false;

			var visitor = new GraphVisitor
			{
				BackwardEdgeFilter = EdgeFilter.IncludeAll,
				ForwardEdgeFilter = EdgeFilter.ExcludeAll,
				VisitNode = delegate(Node n)
				{
					if (n is BeginFightNode)
					{
						inFight = true;
					}
				},
				ContinueNode = n => !(n is BeginFightNode || n is EndFightNode)
			};

			visitor.Visit(this, node);

			return inFight;
		}

		/// <summary>
		/// Removes an actor from the graph entirely.
		/// </summary>
		public void Remove(Actor actor)
		{
			Remove(actor, actor.Born);
		}

		/// <summary>
		/// Removes an actor from the graph starting at the given node.
		/// </summary>
		public void Remove(Actor actor, Node node)
		{
			// Go through and remove the actor from the list.
			var emptyEdges = new List<Edge>();

			var visitor = new GraphVisitor
			{
				BackwardEdgeFilter = EdgeFilter.ExcludeAll,
				ForwardEdgeFilter = EdgeFilter.Follow(actor).SetIncludeAll(),
				VisitForwardEdge = delegate(Edge edge)
				{
					// Remove the actor from the edge.
					edge.Actors.Remove(actor);

					// If we have no edges in here, we need to remove that edge.
					if (edge.Actors.Count == 0)
					{
						emptyEdges.Add(edge);
					}
				}
			};

			visitor.Visit(this, node);

			// Go through and delete any empty edges.
			foreach (var edge in emptyEdges)
			{
				Remove(edge);
			}
		}

		/// <summary>
		/// Removes an edge from the graph along with any nodes that are no longer in use.
		/// </summary>
		public void Remove(Edge edge, bool removeNodes = true)
		{
			// Disconnect that edge from the system. That will remove it from both the head
			// and tail.
			Disconnect(edge);

			// If the tail node has no more incoming edges, we have to remove it.
			if (removeNodes && edge.TailNode.InEdges.Count == 0)
			{
				// We need to remove this node.
				Remove(edge.TailNode);
			}
		}

		/// <summary>
		/// Removes the node from the graph along with any edges that it may have in use.
		/// </summary>
		public void Remove(Node node)
		{
			// Remove this node from the plot.
			_nodes.Remove(node);

			// Go through and remove all the related edges. We pass in false because we don't
			// want a stack overflow. We also need a copy of the edges because we're removing them.
			foreach (var edge in node.InEdges.ToList())
			{
				Remove(edge, false);
			}

			foreach (var edge in node.OutEdges.ToList())
			{
				Remove(edge);
			}
		}

		/// <summary>
		/// Goes through the current node and determines who is arriving or departing from
		/// this element.
		/// </summary>
		/// <param name="node"></param>
		/// <param name="present"></param>
		private List<Actor> SetPresentActors(
			Node node,
			List<Actor> present)
		{
			// Handle the arrive and leaves.
			if (node is DeathNode death)
			{
				present.Remove(death.Actor);
			}
			else if (node.IsExit || node.IsLeave)
			{
				present.RemoveAll(a => node.InActors.Contains(a));
			}

			if (node is BornNode born)
			{
				present.Add(born.Actor);
			}
			else if (node.IsEntry || node.IsArrive)
			{
				present.AddRange(node.InActors);
			}

			// Deduplicate the list.
			return present.Distinct().ToList();
		}
	}
}
