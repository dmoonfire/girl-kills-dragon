#region Namespaces

using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	public class BeginFightNode : Node
	{
		public BeginFightNode(Location location)
			: base(location)
		{
		}
	}
}
