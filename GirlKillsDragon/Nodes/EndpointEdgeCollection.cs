#region Namespaces

using System.Collections.Generic;
using System.Linq;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// A custom collection for managing plot links.
	/// </summary>
	public class EndpointEdgeCollection : EdgeCollection
	{
		private readonly bool _incoming;

		public EndpointEdgeCollection()
		{
		}

		public EndpointEdgeCollection(bool incoming, IEnumerable<Edge> source)
			: base(source)
		{
			_incoming = incoming;
		}

		public EndpointEdgeCollection(bool incoming)
		{
			_incoming = incoming;
		}

		public List<Node> Plots => this
			.Select(i => _incoming ? i.HeadNode : i.TailNode)
			.Distinct()
			.ToList();

		public new EndpointEdgeCollection Filter(NodeGraph graph, EdgeFilter filter)
		{
			return new EndpointEdgeCollection(
				_incoming,
				this.Where(e => filter.Include(e)));
		}
	}
}
