#region Namespaces

using GirlKillsDragon.Locations;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// The point where the hero joins into the story.
	/// </summary>
	public class IncitingNode : Node
	{
		public IncitingNode(Location location)
			: base(location)
		{
		}
	}
}
