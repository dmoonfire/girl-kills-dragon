#region Namespaces

using System;
using System.Collections.Generic;

#endregion

namespace GirlKillsDragon.Nodes
{
	/// <summary>
	/// A visitor class that starts at a given node and goes through every related
	/// edge and node based on the filter conditions.
	/// </summary>
	public class GraphVisitor
	{
		/// <summary>
		/// Gets or sets a filter when going from tail to head (going into the past).
		/// </summary>
		public EdgeFilter BackwardEdgeFilter { get; set; } = EdgeFilter.IncludeAll;

		public Func<Edge, bool> ContinueBackwardEdge { get; set; }

		public Func<Edge, bool> ContinueEdge { get; set; }

		public Func<Edge, bool> ContinueForwardEdge { get; set; }

		public Func<Node, bool> ContinueNode { get; set; }

		/// <summary>
		/// Gets or sets a flag whether the visiting should stop when it encounters a filtered node.
		/// </summary>
		public bool FilteredNodeStop { get; set; }

		/// <summary>
		/// Gets or sets the filter when going from the head to a tail (basically moving into the future).
		/// </summary>
		public EdgeFilter ForwardEdgeFilter { get; set; } = EdgeFilter.IncludeAll;

		/// <summary>
		/// Gets or sets a filter for all node types, which is applied if the edge can be
		/// followed.
		/// </summary>
		public NodeFilter NodeFilter { get; set; } = NodeFilter.IncludeAll;

		/// <summary>
		/// Gets or sets a lambda function to call when visiting a edge moving backward.
		/// </summary>
		public Action<Edge> VisitBackwardEdge { get; set; }

		public Action<EndpointEdgeCollection> VisitBackwardEdges { get; set; }

		/// <summary>
		/// Gets or sets a lambda function to call when visiting a edge. This is called after
		/// the <c>VisitForwardEdge</c> and <c>VisitBackwardEdge</c> functions.
		/// </summary>
		public Action<Edge> VisitEdge { get; set; }

		/// <summary>
		/// Gets or sets a lambda function to call when visiting a edge moving forward.
		/// </summary>
		public Action<Edge> VisitForwardEdge { get; set; }

		public Action<EndpointEdgeCollection> VisitForwardEdges { get; set; }

		/// <summary>
		/// Gets or sets the lambda function to call when visiting a node after all filtering.
		/// </summary>
		public Action<Node> VisitNode { get; set; }

		/// <summary>
		/// Recursively goes through the entire graph starting with the initial node.
		/// 
		/// The order of the visited nodes is not defined.
		/// </summary>
		public void Visit(NodeGraph graph, Node initialNode)
		{
			// Build up a list of nodes that we are starting with. We will keep rebuilding the list
			// to avoid a stack overflow condition. We will, however also include a failsafe to avoid
			// infinite loops. In addition, to avoid recursion, we will keep track of the nodes and edges
			// we've already seen to avoid processing them again.
			var list = new List<Node> {initialNode};
			var seenNodes = new HashSet<Node>();
			var seenEdges = new HashSet<Edge>();

			for (var rounds = 0; rounds < 8192; rounds++)
			{
				// Go through the nodes in the list, processing each one.
				var nextList = new List<Node>();

				foreach (var node in list)
				{
					// See if we are filtering out this node. If we are, then don't bother.
					if (seenNodes.Contains(node) || NodeFilter.Exclude(node))
					{
						continue;
					}

					seenNodes.Add(node);

					// Visit this node, if one has been provided.
					VisitNode?.Invoke(node);

					// See if we want to continue from this node.
					var keepGoing = ContinueNode?.Invoke(node) ?? true;

					if (!keepGoing)
					{
						continue;
					}

					// Go through all of the incoming and outgoing nodes and process those.
					VisitEnds(
						graph,
						seenEdges,
						nextList,
						node.InEdges,
						BackwardEdgeFilter,
						VisitBackwardEdge,
						VisitBackwardEdges,
						ContinueBackwardEdge,
						e => e.HeadNode);
					VisitEnds(
						graph,
						seenEdges,
						nextList,
						node.OutEdges,
						ForwardEdgeFilter,
						VisitForwardEdge,
						VisitForwardEdges,
						ContinueForwardEdge,
						e => e.TailNode);
				}

				// If our next list is empty, then we're done.
				if (nextList.Count == 0)
				{
					return;
				}

				// Once we've gone through the list, replace the old with the new and then go again.
				list = nextList;
			}

			// If we got this far, we appears to be having a problem (or we don't have a large enough
			// graph).
			throw new InvalidOperationException("Had excessive number of rounds, breaking out.");
		}

		private void VisitEnds(
			NodeGraph graph,
			HashSet<Edge> seenEdges,
			List<Node> nextList,
			EndpointEdgeCollection endEdges,
			EdgeFilter endEdgeFilter,
			Action<Edge> visitEndEdge,
			Action<EndpointEdgeCollection> visitEndEdges,
			Func<Edge, bool> continueEdge,
			Func<Edge, Node> getEndNode)
		{
			// Filter out the list of edge nodes.
			var edges = endEdges.Filter(graph, endEdgeFilter);

			// Visit the edges as a whole first.
			visitEndEdges?.Invoke(edges);

			// Go through the list of edges we are processing.
			foreach (var edge in edges)
			{
				// See if we are filtering out this one.
				if (seenEdges.Contains(edge))
				{
					continue;
				}

				seenEdges.Add(edge);

				// Visit this node.
				visitEndEdge?.Invoke(edge);
				VisitEdge?.Invoke(edge);

				// See if we want to continue the edge.
				var add = ContinueEdge?.Invoke(edge) ?? true;

				if (!add)
				{
					continue;
				}

				add = continueEdge?.Invoke(edge) ?? true;

				if (!add)
				{
					continue;
				}

				// Add the other node to the list.
				nextList.Add(getEndNode(edge));
			}
		}
	}
}
