---
generator: GirlKillDragon v0.0.30
ad: Read D. Moonfire's non-procedural novels at https://fedran.com/
---

# One

This chapter is from Alex Greeney's point of view. Cord is 0.00 km away
from the start of the novel. It is located in Quellie and it is a lake. A
detailed description of Cord during morning. Alex, Nena Blettiff, Olly
Mey, Elly Griffer, Vin Smith, Yvonna Harr, Ben Grove, Eliz Her, Elt
Condez, Juanicole Zar, and Laren Gutmartin are already present. A
detailed description of Alex. They are 19 years old. They were born at
Cord in Quellie. A detailed description of Nena. They are 19 years old.
They were born at Meremonder in Quellie. A detailed description of Olly.
They are 16 years old. They were born at Gianaughem in Quellie. A
detailed description of Elly. They are 16 years old. They were born at
Kazming in Quellie. A detailed description of Vin. They are 17 years old.
They were born at Schann in Quellie. A detailed description of Yvonna.
They are 16 years old. They were born at Nyder in Quellie. A detailed
description of Ben. They are 19 years old. They were born at Turis in
Quellie. A detailed description of Eliz. They are 17 years old. They were
born at Jards in Quellie. A detailed description of Elt. They are 20
years old. They were born at Sley in Quellie. A detailed description of
Juanicole. They are 18 years old. They were born at Curlowski in Quellie.
A detailed description of Laren. They are 19 years old. They were born at
Robin in Quellie.

Something terrible happened.

Alex and Elt begin to fight.  Alex fights alone.  Elt fights alone.

Elt died.

Alex finish fighting.

Alex begin some time alone.

# Two

This chapter is from Robert Luna's point of view. Curr is 1,833.86 km
away from the start of the novel. It is located in Rizzian and it is a
town. A detailed description of Curr during morning. Robert is already
present. A detailed description of Robert. They are 214 years old. They
were born at Curr in Rizzian.

Robert leaves Curr on the route to Corthy.

# Three

This chapter is from Alex's point of view. A medium description of Cord
during afternoon. Alex, Nena, Olly, Elly, Vin, Yvonna, Ben, Eliz,
Juanicole, and Laren are already present. A reminder description of Alex.
A reminder description of Nena. A reminder description of Olly. A
reminder description of Elly. A reminder description of Vin. A reminder
description of Yvonna. A reminder description of Ben. A reminder
description of Eliz. A reminder description of Juanicole. A reminder
description of Laren.

Alex finish their time together.

Olly joins Alex.

Alex and Olly leave Cord on the route to Shiyamatsu.

# Four

This chapter is from Robert's point of view. Corthy is 1,725.96 km away
from the start of the novel. It is located in Rizzian and it is a river.
A detailed description of Corthy during early morning.

Robert begin some time alone.

# Five

This chapter is from Robert's point of view. A reminder description of
Corthy during afternoon. Robert is already present. A reminder
description of Robert.

Robert finish their time together.

Robert begins a trial.

# Six

This chapter is from Robert's point of view. A reminder description of
Corthy during very early morning. Robert is already present. A reminder
description of Robert.

Robert finishes his trial.

Robert starts to train alone.

# Seven

This chapter is from Robert's point of view. A reminder description of
Corthy during afternoon. Robert is already present. A reminder
description of Robert.

Robert finishes his training.

Robert leaves Corthy on the route to Iano.

# Eight

This chapter is from Alex's point of view. Shiyamatsu is 251.64 km away
from the start of the novel. It is located in Quellie and it is a farm. A
detailed description of Shiyamatsu during evening. Mizue Suzaki is
already present. A detailed description of Mizue. They are 17 years old.
They were born at Mcellien in Ritt.

Alex and Olly begin some time together.

# Nine

This chapter is from Alex's point of view. A reminder description of
Shiyamatsu during early morning. Alex, Olly, and Mizue are already
present. A reminder description of Alex. A reminder description of Olly.
A reminder description of Mizue.

Alex and Olly finish their time together.

Alex, Olly, and Mizue begin to fight.  Alex fights along with Olly.
Mizue fights alone.

Olly died.

Mizue died.

Alex finish fighting.

Alex starts to train alone.

# Ten

This chapter is from Alex's point of view. A reminder description of
Shiyamatsu during late morning. Alex is already present. A reminder
description of Alex.

Alex finishes his training.

Alex begins a trial.

# Eleven

This chapter is from Alex's point of view. A reminder description of
Shiyamatsu during early evening. Alex is already present. A reminder
description of Alex.

Alex finishes his trial.

Alex leaves Shiyamatsu on the route to Ermeyer.

# Twelve

This chapter is from Robert's point of view. Iano is 3,002.15 km away
from the start of the novel. It is located in Wood and it is a forest. A
detailed description of Iano during very early morning. Gene Mcdanier,
Yer Ring, and Brian Cobson are already present. A detailed description of
Gene. They are 17 years old. They were born at Ibra in Ber. A detailed
description of Yer. They are 16 years old. They were born at Guiling in
Suthal. A detailed description of Brian. They are 20 years old. They were
born at Cona in Wood.

Robert begins a trial.

# Thirteen

This chapter is from Robert's point of view. A reminder description of
Iano during noon. Robert, Gene, Yer, and Brian are already present. A
reminder description of Robert. A reminder description of Gene. A
reminder description of Yer. A reminder description of Brian.

Robert finishes his trial.

Robert leaves Iano on the route to Abstell.

# Fourteen

This chapter is from Alex's point of view. Ermeyer is 374.39 km away from
the start of the novel. It is located in Ritt and it is a lake. A
detailed description of Ermeyer during early evening. Zenta Takae is
already present. A detailed description of Zenta. They are 19 years old.
They were born at Boendo in Ele.

Alex begins a trial.

# Fifteen

This chapter is from Alex's point of view. A reminder description of
Ermeyer during evening. Alex and Zenta are already present. A reminder
description of Alex. A reminder description of Zenta.

Alex finishes his trial.

Alex leaves Ermeyer on the route to Salmer.

# Sixteen

This chapter is from Alex's point of view. Salmer is 492.36 km away from
the start of the novel. It is located in Ritt and it is a tundra. A
detailed description of Salmer during evening.

Alex begins a trial.

# Seventeen

This chapter is from Alex's point of view. A reminder description of
Salmer during morning. Alex is already present. A reminder description of
Alex.

Alex finishes his trial.

Alex leaves Salmer on the route to Tuniuez.

# Eighteen

This chapter is from Robert's point of view. Abstell is 2,220.08 km away
from the start of the novel. It is located in Ber and it is a tundra. A
detailed description of Abstell during near midnight.

Robert begins a trial.

# Nineteen

This chapter is from Robert's point of view. A reminder description of
Abstell during late morning. Robert is already present. A reminder
description of Robert.

Robert finishes his trial.

Robert begin some time alone.

# Twenty

This chapter is from Alex's point of view. Tuniuez is 630.29 km away from
the start of the novel. It is located in Ele and it is a forest. A
detailed description of Tuniuez during late morning.

Alex leaves Tuniuez on the route to Ermeyer.

# Twenty One

This chapter is from Robert's point of view. A medium description of
Abstell during early evening. Robert is already present. A reminder
description of Robert.

Robert finish their time together.

Robert leaves Abstell on the route to Munsteimek.

# Twenty Two

This chapter is from Alex's point of view. A medium description of
Ermeyer during late morning. Zenta is already present. A reminder
description of Zenta.

Alex begin some time alone.

# Twenty Three

This chapter is from Alex's point of view. A reminder description of
Ermeyer during late evening. Alex and Zenta are already present. A
reminder description of Alex. A reminder description of Zenta.

Alex finish their time together.

Alex and Zenta begin to fight.  Alex fights alone.  Zenta fights alone.

Zenta died.

Alex finish fighting.

Alex starts to train alone.

# Twenty Four

This chapter is from Alex's point of view. A reminder description of
Ermeyer during early morning. Alex is already present. A reminder
description of Alex.

Alex finishes his training.

Alex leaves Ermeyer on the route to Bos.

# Twenty Five

This chapter is from Robert's point of view. Munsteimek is 3,562.72 km
away from the start of the novel. It is located in Luiste and it is a
town. A detailed description of Munsteimek during morning. Vickie Giley
and Berly Howan are already present. A detailed description of Vickie.
They are 16 years old. They were born at Sulerman in Larsen. A detailed
description of Berly. They are 18 years old. They were born at Pal in
Luiste.

Robert begins a trial.

# Twenty Six

This chapter is from Robert's point of view. A reminder description of
Munsteimek during late morning. Robert, Vickie, and Berly are already
present. A reminder description of Robert. A reminder description of
Vickie. A reminder description of Berly.

Robert finishes his trial.

Berly joins Robert.

Robert, Vickie, and Berly begin to fight.  Robert fights along with
Berly.  Vickie fights alone.

Vickie died.

Robert and Berly finish fighting.

Robert and Berly begin some time together.

# Twenty Seven

This chapter is from Robert's point of view. A reminder description of
Munsteimek during late evening. Robert and Berly are already present. A
reminder description of Robert. A reminder description of Berly.

Robert and Berly finish their time together.

Robert and Berly leave Munsteimek on the route to Poardues.

# Twenty Eight

This chapter is from Alex's point of view. Bos is 656.35 km away from the
start of the novel. It is located in Ele and it is a forest. A detailed
description of Bos during afternoon.

Alex starts to train alone.

# Twenty Nine

This chapter is from Robert's point of view. Poardues is 2,966.80 km away
from the start of the novel. It is located in Wood and it is a tundra. A
detailed description of Poardues during afternoon.

# Thirty

This chapter is from Alex's point of view. A medium description of Bos
during midnight. Alex is already present. A reminder description of Alex.

Alex finishes his training.

Alex begins a trial.

# Thirty One

This chapter is from Alex's point of view. A reminder description of Bos
during morning. Alex is already present. A reminder description of Alex.

Alex finishes his trial.

Alex leaves Bos on the route to Dufor.

# Thirty Two

This chapter is from Robert's point of view. A medium description of
Poardues during late morning. Robert and Berly are already present. A
reminder description of Robert. A reminder description of Berly.

Robert and Berly leave Poardues on the route to Munsteimek.

# Thirty Three

This chapter is from Robert's point of view. A medium description of
Munsteimek during late morning.

Robert and Berly start to train together.

# Thirty Four

This chapter is from Robert's point of view. A reminder description of
Munsteimek during early evening. Robert and Berly are already present. A
reminder description of Robert. A reminder description of Berly.

Robert and Berly finish their training.

Robert and Berly leave Munsteimek on the route to Petrowski.

# Thirty Five

This chapter is from Alex's point of view. Dufor is 928.58 km away from
the start of the novel. It is located in Gowders and it is a mountain. A
detailed description of Dufor during afternoon. Gakuma Tsumi and Fred
Nighto are already present. A detailed description of Gakuma. They are 17
years old. They were born at Sarcio in Gowders. A detailed description of
Fred. They are 17 years old. They were born at Zhon in Chet.

Alex begins a trial.

# Thirty Six

This chapter is from Alex's point of view. A reminder description of
Dufor during late evening. Alex, Gakuma, and Fred are already present. A
reminder description of Alex. A reminder description of Gakuma. A
reminder description of Fred.

Alex finishes his trial.

Alex begin some time alone.

# Thirty Seven

This chapter is from Robert's point of view. Petrowski is 2,129.62 km
away from the start of the novel. It is located in Ber and it is a farm.
A detailed description of Petrowski during near midnight.

Robert and Berly leave Petrowski on the route to Beierino.

# Thirty Eight

This chapter is from Alex's point of view. A medium description of Dufor
during early morning. Alex, Gakuma, and Fred are already present. A
reminder description of Alex. A reminder description of Gakuma. A
reminder description of Fred.

Alex finish their time together.

Alex starts to train alone.

# Thirty Nine

This chapter is from Robert's point of view. Beierino is 2,093.44 km away
from the start of the novel. It is located in Ber and it is a farm. A
detailed description of Beierino during early morning.

Robert and Berly start to train together.

# Forty

This chapter is from Alex's point of view. A medium description of Dufor
during afternoon. Alex, Gakuma, and Fred are already present. A reminder
description of Alex. A reminder description of Gakuma. A reminder
description of Fred.

Alex finishes his training.

Alex leaves Dufor on the route to Craaffert.

# Forty One

This chapter is from Robert's point of view. A medium description of
Beierino during early evening. Robert and Berly are already present. A
reminder description of Robert. A reminder description of Berly.

Robert and Berly finish their training.

Robert and Berly begin some time together.

# Forty Two

This chapter is from Alex's point of view. Craaffert is 956.82 km away
from the start of the novel. It is located in Gowders and it is a town. A
detailed description of Craaffert during evening.

# Forty Three

This chapter is from Alex's point of view. A reminder description of
Craaffert during very early morning. Alex is already present. A reminder
description of Alex.

Alex leaves Craaffert on the route to Pez.

# Forty Four

This chapter is from Robert's point of view. A medium description of
Beierino during early morning. Robert and Berly are already present. A
reminder description of Robert. A reminder description of Berly.

Robert and Berly finish their time together.

Robert and Berly begin a trial.

# Forty Five

This chapter is from Robert's point of view. A reminder description of
Beierino during early afternoon. Robert and Berly are already present. A
reminder description of Robert. A reminder description of Berly.

Robert and Berly finish their trial.

Robert and Berly leave Beierino on the route to Abstell.

# Forty Six

This chapter is from Robert's point of view. A medium description of
Abstell during early afternoon.

Robert and Berly start to train together.

# Forty Seven

This chapter is from Robert's point of view. A reminder description of
Abstell during evening. Robert and Berly are already present. A reminder
description of Robert. A reminder description of Berly.

Robert and Berly finish their training.

Robert and Berly leave Abstell on the route to Iano.

# Forty Eight

This chapter is from Robert's point of view. A medium description of Iano
during evening. Gene, Yer, and Brian are already present. A reminder
description of Gene. A reminder description of Yer. A reminder
description of Brian.

Yer joins Robert.

Robert, Gene, Yer, and Berly begin to fight.  Robert fights along with
Yer and Berly.  Gene fights alone.

Yer died.

Gene died.

Robert and Berly finish fighting.

Robert and Berly begin some time together.

# Forty Nine

This chapter is from Kazuha Tsu's point of view. Doming is 1,544.73 km
away from the start of the novel. It is located in Yokes and it is a
town. A detailed description of Doming during early morning. Kazuha,
Takarataru Yamano, and Teresa Davier are already present. A detailed
description of Kazuha. They are 18 years old. They were born at Schulta
in Cring. A detailed description of Takarataru. They are 19 years old.
They were born at Branetton in Gowders. A detailed description of Teresa.
They are 19 years old. They were born at Baugo in Chet.

Kazuha died.

Yer died.

# Fifty

This chapter is from Robert's point of view. A medium description of Iano
during early morning. Robert, Brian, and Berly are already present. A
reminder description of Robert. A reminder description of Brian. A
reminder description of Berly.

Robert and Berly finish their time together.

Robert, Brian, and Berly begin to fight.  Robert fights along with Berly.
Brian fights alone.

Brian died.

Robert and Berly finish fighting.

Robert and Berly start to train together.

# Fifty One

This chapter is from Robert's point of view. A reminder description of
Iano during morning. Robert and Berly are already present. A reminder
description of Robert. A reminder description of Berly.

Robert and Berly finish their training.

Robert and Berly leave Iano on the route to Doming.

# Fifty Two

This chapter is from Robert's point of view. A medium description of
Doming during late morning. Takarataru and Teresa are already present. A
reminder description of Takarataru. A reminder description of Teresa.

Takarataru joins Robert and Yer.

Robert, Takarataru, and Berly begin some time together.

# Fifty Three

This chapter is from Alex's point of view. Pez is 1,141.74 km away from
the start of the novel. It is located in Gowders and it is a mountain. A
detailed description of Pez during afternoon.

Alex leaves Pez on the route to Dufor.

# Fifty Four

This chapter is from Alex's point of view. A medium description of Dufor
during late evening. Gakuma and Fred are already present. A reminder
description of Gakuma. A reminder description of Fred.

Fred joins Alex and Olly.

Gakuma joins Alex and Olly.

Alex, Gakuma, and Fred leave Dufor on the route to Larolle.

# Fifty Five

This chapter is from Robert's point of view. A medium description of
Doming during late evening. Robert, Takarataru, Berly, and Teresa are
already present. A reminder description of Robert. A reminder description
of Takarataru. A reminder description of Berly. A reminder description of
Teresa.

Robert, Takarataru, and Berly finish their time together.

Robert, Kazuha, Takarataru, and Berly begin to fight.  Robert fights
along with Takarataru and Berly.  Kazuha fights alone.

Robert and Berly finish fighting.

Robert, Berly, and Teresa begin to fight.  Robert fights along with
Berly.  Teresa fights alone.

Berly died.

Teresa died.

Robert finish fighting.

Robert begins a trial.

# Fifty Six

This chapter is from Kanori Narikawa's point of view. Gard is 552.36 km
away from the start of the novel. It is located in Ritt and it is a
mountain. A detailed description of Gard during very early morning.
Kanori, Shishōhei Oka, Haru Manakakuro, Itsue Uchi, Jōichi Besshi, Yōji
Tsuchi, Zenkichi Furumoto, Hisa Terajima, Chinae Ishimoto, Etsumi
Nabutani, Yuri Tsuki, Mane Ebisu, Sanae Kamitsuda, Suke Jishio, Miyaka
Kashi, Tadanari Imoto, Yūsuke Hiro, Sako Utani, Omi Katsu, Manako Furu,
Renai Saneta, Nodoka Hashiomi, Genjirō Ichiki, Tatsugi Satomo, Otoha
Funakasu, Atsunao Uechi, Hidemi Ōtsui, Otome Iwatsumeda, Yoritaka Hisako,
Nanae Funetsuba, Umeko Yashita, Hisakiko Kami, Narunosuki Oshimata, Rumi
Aikawanaoe, Yayo Taki, and Keichi Ishizumeda are already present. A
detailed description of Kanori. They are 16 years old. They were born at
Russardi in Ele. A detailed description of Shishōhei. They are 20 years
old. They were born at Vano in Ele. A detailed description of Haru. They
are 18 years old. They were born at Boulhell in Cring. A detailed
description of Itsue. They are 16 years old. They were born at Staff in
Ele. A detailed description of Jōichi. They are 20 years old. They were
born at Pagnol in Ele. A detailed description of Yōji. They are 20 years
old. They were born at Mirz in Ele. A detailed description of Zenkichi.
They are 16 years old. They were born at Kormo in Ele. A detailed
description of Hisa. They are 16 years old. They were born at Cross in
Ele. A detailed description of Chinae. They are 16 years old. They were
born at Tommett in Ele. A detailed description of Etsumi. They are 19
years old. They were born at Bockwortot in Ele. A detailed description of
Yuri. They are 16 years old. They were born at Opperdines in Ele. A
detailed description of Mane. They are 19 years old. They were born at
Lis in Ele. A detailed description of Sanae. They are 16 years old. They
were born at Bilsger in Cring. A detailed description of Suke. They are
18 years old. They were born at Heis in Ele. A detailed description of
Miyaka. They are 19 years old. They were born at Neidelo in Cring. A
detailed description of Tadanari. They are 20 years old. They were born
at Weston in Ele. A detailed description of Yūsuke. They are 20 years
old. They were born at Kimleck in Ele. A detailed description of Sako.
They are 17 years old. They were born at Blin in Cring. A detailed
description of Omi. They are 16 years old. They were born at Rus in Ele.
A detailed description of Manako. They are 20 years old. They were born
at Hooperix in Ele. A detailed description of Renai. They are 20 years
old. They were born at Nes in Ele. A detailed description of Nodoka. They
are 17 years old. They were born at Krang in Cring. A detailed
description of Genjirō. They are 20 years old. They were born at Wein in
Cring. A detailed description of Tatsugi. They are 19 years old. They
were born at Reide in Ele. A detailed description of Otoha. They are 17
years old. They were born at Paswaters in Ele. A detailed description of
Atsunao. They are 17 years old. They were born at Mathero in Cring. A
detailed description of Hidemi. They are 18 years old. They were born at
Billmond in Ele. A detailed description of Otome. They are 18 years old.
They were born at Man in Cring. A detailed description of Yoritaka. They
are 20 years old. They were born at Newa in Ele. A detailed description
of Nanae. They are 20 years old. They were born at Fecht in Ele. A
detailed description of Umeko. They are 19 years old. They were born at
Keitsma in Ele. A detailed description of Hisakiko. They are 17 years
old. They were born at Dro in Ele. A detailed description of Narunosuki.
They are 17 years old. They were born at Besuraino in Ele. A detailed
description of Rumi. They are 20 years old. They were born at Bes in
Cring. A detailed description of Yayo. They are 16 years old. They were
born at Broo in Ele. A detailed description of Keichi. They are 17 years
old. They were born at Beven in Ele.

Haru leaves Gard on the route to Camp.

# Fifty Seven

This chapter is from Robert's point of view. A medium description of
Doming during very early morning. Robert and Takarataru are already
present. A reminder description of Robert. A reminder description of
Takarataru.

Robert finishes his trial.

Robert starts to train alone.

# Fifty Eight

This chapter is from Robert's point of view. A reminder description of
Doming during morning. Robert and Takarataru are already present. A
reminder description of Robert. A reminder description of Takarataru.

Robert finishes his training.

Robert leaves Doming on the route to Gard.

# Fifty Nine

This chapter is from Alex's point of view. Larolle is 1,062.25 km away
from the start of the novel. It is located in Gowders and it is a farm. A
detailed description of Larolle during near midnight. Louise Diaz is
already present. A detailed description of Louise. They are 20 years old.
They were born at Ning in Chet.

Alex, Gakuma, and Fred begin some time together.

# Sixty

This chapter is from Alex's point of view. A reminder description of
Larolle during late morning. Alex, Gakuma, Fred, and Louise are already
present. A reminder description of Alex. A reminder description of
Gakuma. A reminder description of Fred. A reminder description of Louise.

Alex, Gakuma, and Fred finish their time together.

Alex, Gakuma, and Fred start to train together.

# Sixty One

This chapter is from Haru's point of view. Camp is 745.37 km away from
the start of the novel. It is located in Ele and it is a tundra. A
detailed description of Camp during afternoon.

Haru begins a trial.

# Sixty Two

This chapter is from Robert's point of view. A medium description of Gard
during early evening. Kanori, Shishōhei, Itsue, Jōichi, Yōji, Zenkichi,
Hisa, Chinae, Etsumi, Yuri, Mane, Sanae, Suke, Miyaka, Tadanari, Yūsuke,
Sako, Omi, Manako, Renai, Nodoka, Genjirō, Tatsugi, Otoha, Atsunao,
Hidemi, Otome, Yoritaka, Nanae, Umeko, Hisakiko, Narunosuki, Rumi, Yayo,
and Keichi are already present. A reminder description of Kanori. A
reminder description of Shishōhei. A reminder description of Itsue. A
reminder description of Jōichi. A reminder description of Yōji. A
reminder description of Zenkichi. A reminder description of Hisa. A
reminder description of Chinae. A reminder description of Etsumi. A
reminder description of Yuri. A reminder description of Mane. A reminder
description of Sanae. A reminder description of Suke. A reminder
description of Miyaka. A reminder description of Tadanari. A reminder
description of Yūsuke. A reminder description of Sako. A reminder
description of Omi. A reminder description of Manako. A reminder
description of Renai. A reminder description of Nodoka. A reminder
description of Genjirō. A reminder description of Tatsugi. A reminder
description of Otoha. A reminder description of Atsunao. A reminder
description of Hidemi. A reminder description of Otome. A reminder
description of Yoritaka. A reminder description of Nanae. A reminder
description of Umeko. A reminder description of Hisakiko. A reminder
description of Narunosuki. A reminder description of Rumi. A reminder
description of Yayo. A reminder description of Keichi.

Tatsugi joins Robert.

# Sixty Three

This chapter is from Alex's point of view. A medium description of
Larolle during evening. Alex, Gakuma, Fred, and Louise are already
present. A reminder description of Alex. A reminder description of
Gakuma. A reminder description of Fred. A reminder description of Louise.

Alex, Gakuma, and Fred finish their training.

Alex, Gakuma, and Fred begin a trial.

# Sixty Four

This chapter is from Haru's point of view. A medium description of Camp
during late evening. Haru is already present. A reminder description of
Haru.

Haru finishes her trial.

Haru leaves Camp on the route to Gard.

# Sixty Five

This chapter is from Robert's point of view. A medium description of Gard
during late evening. Robert, Kanori, Shishōhei, Itsue, Jōichi, Yōji,
Zenkichi, Hisa, Chinae, Etsumi, Yuri, Mane, Sanae, Suke, Miyaka,
Tadanari, Yūsuke, Sako, Omi, Manako, Renai, Nodoka, Genjirō, Tatsugi,
Otoha, Atsunao, Hidemi, Otome, Yoritaka, Nanae, Umeko, Hisakiko,
Narunosuki, Rumi, Yayo, and Keichi are already present. A reminder
description of Robert. A reminder description of Kanori. A reminder
description of Shishōhei. A reminder description of Itsue. A reminder
description of Jōichi. A reminder description of Yōji. A reminder
description of Zenkichi. A reminder description of Hisa. A reminder
description of Chinae. A reminder description of Etsumi. A reminder
description of Yuri. A reminder description of Mane. A reminder
description of Sanae. A reminder description of Suke. A reminder
description of Miyaka. A reminder description of Tadanari. A reminder
description of Yūsuke. A reminder description of Sako. A reminder
description of Omi. A reminder description of Manako. A reminder
description of Renai. A reminder description of Nodoka. A reminder
description of Genjirō. A reminder description of Tatsugi. A reminder
description of Otoha. A reminder description of Atsunao. A reminder
description of Hidemi. A reminder description of Otome. A reminder
description of Yoritaka. A reminder description of Nanae. A reminder
description of Umeko. A reminder description of Hisakiko. A reminder
description of Narunosuki. A reminder description of Rumi. A reminder
description of Yayo. A reminder description of Keichi.

Haru died.

# Sixty Six

This chapter is from Alex's point of view. A medium description of
Larolle during very early morning. Alex, Gakuma, Fred, and Louise are
already present. A reminder description of Alex. A reminder description
of Gakuma. A reminder description of Fred. A reminder description of
Louise.

Alex, Gakuma, and Fred finish their trial.

Louise joins Alex, Olly, Gakuma, and Fred.

Alex, Gakuma, Fred, and Louise leave Larolle on the route to Towka.

# Sixty Seven

This chapter is from Alex's point of view. Towka is 1,292.82 km away from
the start of the novel. It is located in Chet and it is a river. A
detailed description of Towka during midnight. Carol Fostano, Joseph
Dunnings, Joshua Lin, Mary Atthell, Clifford Evan, Lindsay Reed, and
Amparon Baller are already present. A detailed description of Carol. They
are 19 years old. They were born at Lussow in Yokes. A detailed
description of Joseph. They are 16 years old. They were born at Meady in
Chet. A detailed description of Joshua. They are 17 years old. They were
born at Brado in Chet. A detailed description of Mary. They are 19 years
old. They were born at Buch in Yokes. A detailed description of Clifford.
They are 20 years old. They were born at Oleserry in Yokes. A detailed
description of Lindsay. They are 20 years old. They were born at Gotton
in Chet. A detailed description of Amparon. They are 18 years old. They
were born at Poman in Yokes.

Alex, Gakuma, Fred, and Louise start to train together.

# Sixty Eight

This chapter is from Alex's point of view. A reminder description of
Towka during early afternoon. Alex, Gakuma, Carol, Joseph, Fred, Joshua,
Mary, Clifford, Louise, Lindsay, and Amparon are already present. A
reminder description of Alex. A reminder description of Gakuma. A
reminder description of Carol. A reminder description of Joseph. A
reminder description of Fred. A reminder description of Joshua. A
reminder description of Mary. A reminder description of Clifford. A
reminder description of Louise. A reminder description of Lindsay. A
reminder description of Amparon.

Alex, Gakuma, Fred, and Louise finish their training.

Alex, Gakuma, Fred, Clifford, and Louise begin to fight.  Alex fights
along with Gakuma, Fred, and Louise.  Clifford fights alone.

Clifford leaves Towka on the route to Viscias.

Fred leaves Towka on the route to Marades.

# Sixty Nine

This chapter is from Clifford's point of view. Viscias is 1,467.62 km
away from the start of the novel. It is located in Yokes and it is a
swamp. A detailed description of Viscias during early afternoon.

Clifford leaves.

# Seventy

This chapter is from Fred's point of view. Marades is 1,527.17 km away
from the start of the novel. It is located in Yokes and it is a forest. A
detailed description of Marades during early afternoon.

Fred leaves.

# Seventy One

This chapter is from Alex's point of view. A medium description of Towka
during late evening. Alex, Gakuma, Carol, Joseph, Joshua, Mary, Louise,
Lindsay, and Amparon are already present. A reminder description of Alex.
A reminder description of Gakuma. A reminder description of Carol. A
reminder description of Joseph. A reminder description of Joshua. A
reminder description of Mary. A reminder description of Louise. A
reminder description of Lindsay. A reminder description of Amparon.

Mary died.

# Seventy Two

This chapter is from Alex's point of view. A reminder description of
Towka during early morning. Alex, Gakuma, Carol, Joseph, Joshua, Louise,
Lindsay, and Amparon are already present. A reminder description of Alex.
A reminder description of Gakuma. A reminder description of Carol. A
reminder description of Joseph. A reminder description of Joshua. A
reminder description of Louise. A reminder description of Lindsay. A
reminder description of Amparon.

Alex, Gakuma, and Louise leave Towka on the route to Thagy.

# Seventy Three

This chapter is from Alex's point of view. Thagy is 1,516.89 km away from
the start of the novel. It is located in Yokes and it is a forest. A
detailed description of Thagy during very early morning.

# Seventy Four

This chapter is from Carol's point of view. A medium description of Towka
during afternoon. Carol, Joseph, Joshua, Lindsay, and Amparon are already
present. A reminder description of Carol. A reminder description of
Joseph. A reminder description of Joshua. A reminder description of
Lindsay. A reminder description of Amparon.

Carol died.

# Seventy Five

This chapter is from Alex's point of view. A medium description of Thagy
during near midnight. Alex, Gakuma, and Louise are already present. A
reminder description of Alex. A reminder description of Gakuma. A
reminder description of Louise.

Alex, Gakuma, and Louise leave Thagy on the route to Sutlan.

# Seventy Six

This chapter is from Alex's point of view. Sutlan is 1,546.80 km away
from the start of the novel. It is located in Yokes and it is a city. A
detailed description of Sutlan during early morning.

Alex, Gakuma, and Louise leave Sutlan on the route to Towka.

# Seventy Seven

This chapter is from Alex's point of view. A medium description of Towka
during very early morning. Joseph, Joshua, Lindsay, and Amparon are
already present. A reminder description of Joseph. A reminder description
of Joshua. A reminder description of Lindsay. A reminder description of
Amparon.

Alex, Gakuma, Louise, and Amparon begin to fight.  Alex fights along with
Gakuma and Louise.  Amparon fights alone.

# Seventy Eight

This chapter is from Alex's point of view. A reminder description of
Towka during early morning. Alex, Gakuma, Joseph, Joshua, Louise,
Lindsay, and Amparon are already present. A reminder description of Alex.
A reminder description of Gakuma. A reminder description of Joseph. A
reminder description of Joshua. A reminder description of Louise. A
reminder description of Lindsay. A reminder description of Amparon.

Amparon died.

Gakuma died.

Louise died.

Alex finish fighting.

Alex finish fighting.

Alex begin some time alone.

# Seventy Nine

This chapter is from Alex's point of view. A reminder description of
Towka during early afternoon. Alex, Joseph, Joshua, and Lindsay are
already present. A reminder description of Alex. A reminder description
of Joseph. A reminder description of Joshua. A reminder description of
Lindsay.

Alex finish their time together.

Alex leaves Towka on the route to Rancassey.

# Eighty

This chapter is from Alex's point of view. Rancassey is 1,560.44 km away
from the start of the novel. It is located in Yokes and it is a swamp. A
detailed description of Rancassey during early evening.

Alex begins a trial.

# Eighty One

This chapter is from Alex's point of view. A reminder description of
Rancassey during late evening. Alex is already present. A reminder
description of Alex.

Alex finishes his trial.

Alex starts to train alone.

# Eighty Two

This chapter is from Alex's point of view. A reminder description of
Rancassey during very early morning. Alex is already present. A reminder
description of Alex.

Alex finishes his training.

Alex begin some time alone.

# Eighty Three

This chapter is from Alex's point of view. A reminder description of
Rancassey during morning. Alex is already present. A reminder description
of Alex.

Alex finish their time together.

Alex leaves Rancassey on the route to Towka.

# Eighty Four

This chapter is from Alex's point of view. A medium description of Towka
during morning. Joseph, Joshua, and Lindsay are already present. A
reminder description of Joseph. A reminder description of Joshua. A
reminder description of Lindsay.

Joshua joins Alex, Olly, Gakuma, and Fred.

Alex and Joshua leave Towka on the route to Dit.

# Eighty Five

This chapter is from Alex's point of view. Dit is 1,396.15 km away from
the start of the novel. It is located in Yokes and it is a mountain. A
detailed description of Dit during very early morning.

Alex and Joshua leave Dit on the route to Rae.

# Eighty Six

This chapter is from Alex's point of view. Rae is 1,693.40 km away from
the start of the novel. It is located in Rizzian and it is a city. A
detailed description of Rae during afternoon.

# Eighty Seven

This chapter is from Alex's point of view. A reminder description of Rae
during evening. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua leave Rae on the route to Towka.

# Eighty Eight

This chapter is from Alex's point of view. A medium description of Towka
during evening. Joseph and Lindsay are already present. A reminder
description of Joseph. A reminder description of Lindsay.

Alex and Joshua leave Towka on the route to Low.

# Eighty Nine

This chapter is from Alex's point of view. Low is 1,319.17 km away from
the start of the novel. It is located in Chet and it is a tundra. A
detailed description of Low during midnight. Katheres Crown is already
present. A detailed description of Katheres. They are 18 years old. They
were born at Peger in Yokes.

Alex and Joshua start to train together.

# Ninety

This chapter is from Alex's point of view. A reminder description of Low
during late morning. Alex, Joshua, and Katheres are already present. A
reminder description of Alex. A reminder description of Joshua. A
reminder description of Katheres.

Alex and Joshua finish their training.

Alex, Joshua, and Katheres begin to fight.  Alex fights along with
Joshua.  Katheres fights alone.

Katheres died.

Alex and Joshua finish fighting.

Alex and Joshua leave Low on the route to Edriz.

# Ninety One

This chapter is from Alex's point of view. Edriz is 1,387.72 km away from
the start of the novel. It is located in Chet and it is a tundra. A
detailed description of Edriz during midnight.

Alex and Joshua begin a trial.

# Ninety Two

This chapter is from Alex's point of view. A reminder description of
Edriz during morning. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua finish their trial.

Alex and Joshua begin some time together.

# Ninety Three

This chapter is from Alex's point of view. A reminder description of
Edriz during afternoon. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua finish their time together.

Alex and Joshua start to train together.

# Ninety Four

This chapter is from Alex's point of view. A reminder description of
Edriz during midnight. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua finish their training.

Alex and Joshua leave Edriz on the route to Ebel.

# Ninety Five

This chapter is from Alex's point of view. Ebel is 1,626.23 km away from
the start of the novel. It is located in Yokes and it is a river. A
detailed description of Ebel during midnight.

Alex and Joshua begin some time together.

# Ninety Six

This chapter is from Alex's point of view. A reminder description of Ebel
during early morning. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua finish their time together.

Alex and Joshua start to train together.

# Ninety Seven

This chapter is from Alex's point of view. A reminder description of Ebel
during afternoon. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua finish their training.

Alex and Joshua leave Ebel on the route to Ama.

# Ninety Eight

This chapter is from Alex's point of view. Ama is 1,860.71 km away from
the start of the novel. It is located in Rizzian and it is a farm. A
detailed description of Ama during early afternoon.

Alex and Joshua start to train together.

# Ninety Nine

This chapter is from Alex's point of view. A reminder description of Ama
during late evening. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua finish their training.

Alex and Joshua begin a trial.

# One Hundred

This chapter is from Alex's point of view. A reminder description of Ama
during late morning. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua finish their trial.

Alex and Joshua begin some time together.

# One Hundred And One

This chapter is from Alex's point of view. A reminder description of Ama
during late evening. Alex and Joshua are already present. A reminder
description of Alex. A reminder description of Joshua.

Alex and Joshua finish their time together.

Alex and Joshua leave Ama on the route to Arcong.

# One Hundred And Two

This chapter is from Alex's point of view. Arcong is 1,931.49 km away
from the start of the novel. It is located in Rizzian and it is a forest.
A detailed description of Arcong during late morning.

Alex and Joshua start to train together.

# One Hundred And Three

This chapter is from Alex's point of view. A reminder description of
Arcong during near midnight. Alex and Joshua are already present. A
reminder description of Alex. A reminder description of Joshua.

Alex and Joshua finish their training.

Alex and Joshua begin a trial.

# One Hundred And Four

This chapter is from Alex's point of view. A reminder description of
Arcong during very early morning. Alex and Joshua are already present. A
reminder description of Alex. A reminder description of Joshua.

Alex and Joshua finish their trial.

Alex and Joshua begin some time together.

# One Hundred And Five

This chapter is from Alex's point of view. A reminder description of
Arcong during late morning. Alex and Joshua are already present. A
reminder description of Alex. A reminder description of Joshua.

Alex and Joshua finish their time together.

Alex and Joshua leave Arcong on the route to Laudetica.

# One Hundred And Six

This chapter is from Alex's point of view. Laudetica is 1,991.34 km away
from the start of the novel. It is located in Ber and it is a town. A
detailed description of Laudetica during near midnight.

Alex and Joshua begin a trial.

# One Hundred And Seven

This chapter is from Alex's point of view. A reminder description of
Laudetica during early morning. Alex and Joshua are already present. A
reminder description of Alex. A reminder description of Joshua.

Alex and Joshua finish their trial.

Alex and Joshua start to train together.

# One Hundred And Eight

This chapter is from Alex's point of view. A reminder description of
Laudetica during early evening. Alex and Joshua are already present. A
reminder description of Alex. A reminder description of Joshua.

Alex and Joshua finish their training.

Alex and Joshua begin some time together.

# One Hundred And Nine

This chapter is from Alex's point of view. A reminder description of
Laudetica during early morning. Alex and Joshua are already present. A
reminder description of Alex. A reminder description of Joshua.

Alex and Joshua finish their time together.

Alex and Joshua leave Laudetica on the route to Moorian.

# One Hundred And Ten

This chapter is from Alex's point of view. Moorian is 2,185.45 km away
from the start of the novel. It is located in Ber and it is a river. A
detailed description of Moorian during late evening. Kennis Johns is
already present. A detailed description of Kennis. They are 18 years old.
They were born at Rey in Suthal.

Kennis joins Alex and Joshua.

Alex, Joshua, and Kennis leave Moorian on the route to Towka.

# One Hundred And Eleven

This chapter is from Alex's point of view. A medium description of Towka
during midnight. Joseph and Lindsay are already present. A reminder
description of Joseph. A reminder description of Lindsay.

Alex, Joshua, and Kennis leave Towka on the route to Beagy.

# One Hundred And Twelve

This chapter is from Alex's point of view. Beagy is 1,560.46 km away from
the start of the novel. It is located in Yokes and it is a swamp. A
detailed description of Beagy during early morning.

Alex, Joshua, and Kennis leave Beagy on the route to Kratt.

# One Hundred And Thirteen

This chapter is from Alex's point of view. Kratt is 1,818.75 km away from
the start of the novel. It is located in Rizzian and it is a lake. A
detailed description of Kratt during morning.

# One Hundred And Fourteen

This chapter is from Alex's point of view. A reminder description of
Kratt during late evening. Alex, Joshua, and Kennis are already present.
A reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis leave Kratt on the route to Kammas.

# One Hundred And Fifteen

This chapter is from Alex's point of view. Kammas is 1,865.34 km away
from the start of the novel. It is located in Rizzian and it is a
mountain. A detailed description of Kammas during morning.

Alex, Joshua, and Kennis start to train together.

# One Hundred And Sixteen

This chapter is from Alex's point of view. A reminder description of
Kammas during early evening. Alex, Joshua, and Kennis are already
present. A reminder description of Alex. A reminder description of
Joshua. A reminder description of Kennis.

Alex, Joshua, and Kennis finish their training.

Alex, Joshua, and Kennis begin a trial.

# One Hundred And Seventeen

This chapter is from Alex's point of view. A reminder description of
Kammas during late evening. Alex, Joshua, and Kennis are already present.
A reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis finish their trial.

Alex, Joshua, and Kennis begin some time together.

# One Hundred And Eighteen

This chapter is from Alex's point of view. A reminder description of
Kammas during morning. Alex, Joshua, and Kennis are already present. A
reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis finish their time together.

Alex, Joshua, and Kennis leave Kammas on the route to Towka.

# One Hundred And Nineteen

This chapter is from Alex's point of view. A medium description of Towka
during evening. Joseph and Lindsay are already present. A reminder
description of Joseph. A reminder description of Lindsay.

Alex, Joshua, Lindsay, and Kennis begin to fight.  Alex fights along with
Joshua and Kennis.  Lindsay fights alone.

Lindsay died.

Alex, Joshua, and Kennis finish fighting.

Alex, Joshua, and Kennis leave Towka on the route to Kner.

# One Hundred And Twenty

This chapter is from Alex's point of view. Kner is 1,569.51 km away from
the start of the novel. It is located in Yokes and it is a swamp. A
detailed description of Kner during early morning.

Alex, Joshua, and Kennis leave Kner on the route to Lle.

# One Hundred And Twenty One

This chapter is from Alex's point of view. Lle is 1,622.50 km away from
the start of the novel. It is located in Yokes and it is a farm. A
detailed description of Lle during early afternoon.

Alex, Joshua, and Kennis start to train together.

# One Hundred And Twenty Two

This chapter is from Alex's point of view. A reminder description of Lle
during evening. Alex, Joshua, and Kennis are already present. A reminder
description of Alex. A reminder description of Joshua. A reminder
description of Kennis.

Alex, Joshua, and Kennis finish their training.

Alex, Joshua, and Kennis begin a trial.

# One Hundred And Twenty Three

This chapter is from Alex's point of view. A reminder description of Lle
during early morning. Alex, Joshua, and Kennis are already present. A
reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis finish their trial.

Alex, Joshua, and Kennis leave Lle on the route to Rum.

# One Hundred And Twenty Four

This chapter is from Alex's point of view. Rum is 1,822.52 km away from
the start of the novel. It is located in Rizzian and it is a river. A
detailed description of Rum during late evening.

Alex, Joshua, and Kennis begin some time together.

# One Hundred And Twenty Five

This chapter is from Alex's point of view. A reminder description of Rum
during early morning. Alex, Joshua, and Kennis are already present. A
reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis finish their time together.

Alex, Joshua, and Kennis leave Rum on the route to Mogadom.

# One Hundred And Twenty Six

This chapter is from Alex's point of view. Mogadom is 2,111.22 km away
from the start of the novel. It is located in Ber and it is a forest. A
detailed description of Mogadom during afternoon.

Alex, Joshua, and Kennis start to train together.

# One Hundred And Twenty Seven

This chapter is from Alex's point of view. A reminder description of
Mogadom during very early morning. Alex, Joshua, and Kennis are already
present. A reminder description of Alex. A reminder description of
Joshua. A reminder description of Kennis.

Alex, Joshua, and Kennis finish their training.

Alex, Joshua, and Kennis leave Mogadom on the route to Fly.

# One Hundred And Twenty Eight

This chapter is from Alex's point of view. Fly is 2,249.77 km away from
the start of the novel. It is located in Ber and it is a swamp. A
detailed description of Fly during early morning.

Alex, Joshua, and Kennis start to train together.

# One Hundred And Twenty Nine

This chapter is from Alex's point of view. A reminder description of Fly
during noon. Alex, Joshua, and Kennis are already present. A reminder
description of Alex. A reminder description of Joshua. A reminder
description of Kennis.

Alex, Joshua, and Kennis finish their training.

Alex, Joshua, and Kennis begin a trial.

# One Hundred And Thirty

This chapter is from Alex's point of view. A reminder description of Fly
during afternoon. Alex, Joshua, and Kennis are already present. A
reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis finish their trial.

Alex, Joshua, and Kennis leave Fly on the route to Kner.

# One Hundred And Thirty One

This chapter is from Alex's point of view. A medium description of Kner
during afternoon.

Alex, Joshua, and Kennis begin a trial.

# One Hundred And Thirty Two

This chapter is from Alex's point of view. A reminder description of Kner
during very early morning. Alex, Joshua, and Kennis are already present.
A reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis finish their trial.

Alex, Joshua, and Kennis begin some time together.

# One Hundred And Thirty Three

This chapter is from Alex's point of view. A reminder description of Kner
during early afternoon. Alex, Joshua, and Kennis are already present. A
reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis finish their time together.

Alex, Joshua, and Kennis start to train together.

# One Hundred And Thirty Four

This chapter is from Alex's point of view. A reminder description of Kner
during near midnight. Alex, Joshua, and Kennis are already present. A
reminder description of Alex. A reminder description of Joshua. A
reminder description of Kennis.

Alex, Joshua, and Kennis finish their training.

Alex, Joshua, and Kennis leave Kner on the route to Towka.

# One Hundred And Thirty Five

This chapter is from Alex's point of view. A medium description of Towka
during near midnight. Joseph is already present. A reminder description
of Joseph.

Alex, Joseph, Joshua, and Kennis begin to fight.  Alex fights along with
Joshua and Kennis.  Joseph fights alone.

Alex, Mary, and Kennis begin to fight.  Alex fights along with Kennis.
Mary fights alone.

Joseph leaves Towka on the route to Holz.

Alex and Kennis finish fighting.

Joshua leaves Towka on the route to Fli.

Alex and Kennis leave Towka on the route to Nasissy.

# One Hundred And Thirty Six

This chapter is from Joseph's point of view. Holz is 1,511.02 km away
from the start of the novel. It is located in Yokes and it is a mountain.
A detailed description of Holz during near midnight.

Joseph leaves.

# One Hundred And Thirty Seven

This chapter is from Joshua's point of view. Fli is 1,940.13 km away from
the start of the novel. It is located in Rizzian and it is a tundra. A
detailed description of Fli during near midnight. Ernon Belauzy and
Brancis Carbozeman are already present. A detailed description of Ernon.
They are 18 years old. They were born at Meehle in Rizzian. A detailed
description of Brancis. They are 19 years old. They were born at Butt in
Rizzian.

Joshua and Ernon begin to fight.  Joshua fights alone.  Ernon fights
alone.

Ernon died.

Joshua finish fighting.

Joshua and Brancis begin to fight.  Joshua fights alone.  Brancis fights
alone.

Brancis died.

Joshua finish fighting.

Joshua begins a trial.

# One Hundred And Thirty Eight

This chapter is from Alex's point of view. Nasissy is 1,345.50 km away
from the start of the novel. It is located in Chet and it is a city. A
detailed description of Nasissy during morning.

Alex and Kennis begin some time together.

# One Hundred And Thirty Nine

This chapter is from Joshua's point of view. A medium description of Fli
during late morning. Joshua is already present. A reminder description of
Joshua.

Joshua finishes his trial.

Joshua starts to train alone.

# One Hundred And Forty

This chapter is from Alex's point of view. A medium description of
Nasissy during noon. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis leave Nasissy on the route to Towka.

# One Hundred And Forty One

This chapter is from Alex's point of view. A medium description of Towka
during noon.

Alex and Kennis finish fighting.

Alex, Carol, and Kennis begin to fight.  Alex fights along with Kennis.
Carol fights alone.

Alex and Kennis leave Towka on the route to Lie.

# One Hundred And Forty Two

This chapter is from Joshua's point of view. A medium description of Fli
during afternoon. Joshua is already present. A reminder description of
Joshua.

Joshua finishes his training.

Joshua leaves.

# One Hundred And Forty Three

This chapter is from Alex's point of view. Lie is 1,564.90 km away from
the start of the novel. It is located in Yokes and it is a river. A
detailed description of Lie during evening.

# One Hundred And Forty Four

This chapter is from Alex's point of view. A reminder description of Lie
during very early morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis leave Lie on the route to Towka.

# One Hundred And Forty Five

This chapter is from Alex's point of view. A medium description of Towka
during very early morning.

Alex and Kennis finish fighting.

Alex and Kennis leave Towka on the route to Flagodara.

# One Hundred And Forty Six

This chapter is from Alex's point of view. Flagodara is 1,452.52 km away
from the start of the novel. It is located in Yokes and it is a city. A
detailed description of Flagodara during morning.

Alex and Kennis begin a trial.

# One Hundred And Forty Seven

This chapter is from Alex's point of view. A reminder description of
Flagodara during late morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis start to train together.

# One Hundred And Forty Eight

This chapter is from Alex's point of view. A reminder description of
Flagodara during early evening. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Flagodara on the route to Towka.

# One Hundred And Forty Nine

This chapter is from Alex's point of view. A medium description of Towka
during early evening.

Alex and Kennis leave Towka on the route to Billi.

# One Hundred And Fifty

This chapter is from Alex's point of view. Billi is 1,336.73 km away from
the start of the novel. It is located in Chet and it is a lake. A
detailed description of Billi during very early morning.

Alex and Kennis start to train together.

# One Hundred And Fifty One

This chapter is from Alex's point of view. A reminder description of
Billi during late morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis begin a trial.

# One Hundred And Fifty Two

This chapter is from Alex's point of view. A reminder description of
Billi during early evening. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis begin some time together.

# One Hundred And Fifty Three

This chapter is from Alex's point of view. A reminder description of
Billi during evening. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis leave Billi on the route to Towka.

# One Hundred And Fifty Four

This chapter is from Alex's point of view. A medium description of Towka
during evening.

Alex and Kennis begin a trial.

# One Hundred And Fifty Five

This chapter is from Alex's point of view. A reminder description of
Towka during evening. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis leave Towka on the route to Hie.

# One Hundred And Fifty Six

This chapter is from Alex's point of view. Hie is 1,407.21 km away from
the start of the novel. It is located in Yokes and it is a swamp. A
detailed description of Hie during evening.

Alex and Kennis leave Hie on the route to Honer.

# One Hundred And Fifty Seven

This chapter is from Alex's point of view. Honer is 1,527.48 km away from
the start of the novel. It is located in Yokes and it is a farm. A
detailed description of Honer during evening. Regory Man, William Bee,
and Edward Orrea are already present. A detailed description of Regory.
They are 18 years old. They were born at Botnier in Yokes. A detailed
description of William. They are 20 years old. They were born at Kalezake
in Yokes. A detailed description of Edward. They are 18 years old. They
were born at Fiell in Rizzian.

Alex and Kennis leave Honer on the route to Dunnett.

# One Hundred And Fifty Eight

This chapter is from Alex's point of view. Dunnett is 1,725.42 km away
from the start of the novel. It is located in Rizzian and it is a city. A
detailed description of Dunnett during late morning. Nathancis Turne,
Lisa Holmorgan, Jason Ell, and Ler Grie are already present. A detailed
description of Nathancis. They are 20 years old. They were born at Stad
in Rizzian. A detailed description of Lisa. They are 20 years old. They
were born at Row in Rizzian. A detailed description of Jason. They are 20
years old. They were born at Cris in Rizzian. A detailed description of
Ler. They are 16 years old. They were born at Has in Rizzian.

Alex, Ler, and Kennis begin to fight.  Alex fights along with Kennis.
Ler fights alone.

Ler died.

Alex and Kennis finish fighting.

Alex and Kennis start to train together.

# One Hundred And Fifty Nine

This chapter is from Alex's point of view. A reminder description of
Dunnett during evening. Alex, Nathancis, Lisa, Jason, and Kennis are
already present. A reminder description of Alex. A reminder description
of Nathancis. A reminder description of Lisa. A reminder description of
Jason. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex, Jason, and Kennis begin to fight.  Alex fights along with Kennis.
Jason fights alone.

Jason died.

Alex and Kennis finish fighting.

Alex, Nathancis, and Kennis begin to fight.  Alex fights along with
Kennis.  Nathancis fights alone.

Nathancis died.

Alex and Kennis finish fighting.

Alex, Lisa, and Kennis begin to fight.  Alex fights along with Kennis.
Lisa fights alone.

Lisa died.

Alex and Kennis finish fighting.

Alex and Kennis begin some time together.

# One Hundred And Sixty

This chapter is from Alex's point of view. A reminder description of
Dunnett during early morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis begin a trial.

# One Hundred And Sixty One

This chapter is from Alex's point of view. A reminder description of
Dunnett during afternoon. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis leave Dunnett on the route to Honer.

# One Hundred And Sixty Two

This chapter is from Alex's point of view. A medium description of Honer
during afternoon. Regory, William, and Edward are already present. A
reminder description of Regory. A reminder description of William. A
reminder description of Edward.

Edward joins Alex.

Alex, Edward, and Kennis start to train together.

# One Hundred And Sixty Three

This chapter is from Alex's point of view. A reminder description of
Honer during evening. Alex, Regory, William, Edward, and Kennis are
already present. A reminder description of Alex. A reminder description
of Regory. A reminder description of William. A reminder description of
Edward. A reminder description of Kennis.

Alex, Edward, and Kennis finish their training.

Alex, Regory, Edward, and Kennis begin to fight.  Alex fights along with
Edward and Kennis.  Regory fights alone.

William joins Alex and Fred.

Edward died.

Regory died.

Alex, William, and Kennis finish fighting.

Alex, William, and Kennis begin a trial.

# One Hundred And Sixty Four

This chapter is from Alex's point of view. A reminder description of
Honer during late evening. Alex, William, and Kennis are already present.
A reminder description of Alex. A reminder description of William. A
reminder description of Kennis.

Alex, William, and Kennis finish their trial.

Alex, William, and Kennis begin some time together.

# One Hundred And Sixty Five

This chapter is from Alex's point of view. A reminder description of
Honer during late morning. Alex, William, and Kennis are already present.
A reminder description of Alex. A reminder description of William. A
reminder description of Kennis.

Alex, William, and Kennis finish their time together.

Alex, William, and Kennis leave Honer on the route to Hullig.

# One Hundred And Sixty Six

This chapter is from Alex's point of view. Hullig is 1,651.54 km away
from the start of the novel. It is located in Rizzian and it is a farm. A
detailed description of Hullig during late morning. Jose Simpson, Ruth
Baker, and Pat Frey are already present. A detailed description of Jose.
They are 18 years old. They were born at Seth in Rizzian. A detailed
description of Ruth. They are 19 years old. They were born at Vanrow in
Rizzian. A detailed description of Pat. They are 16 years old. They were
born at Tukinger in Rizzian.

Alex, William, and Kennis start to train together.

# One Hundred And Sixty Seven

This chapter is from Alex's point of view. A reminder description of
Hullig during afternoon. Alex, William, Jose, Ruth, Pat, and Kennis are
already present. A reminder description of Alex. A reminder description
of William. A reminder description of Jose. A reminder description of
Ruth. A reminder description of Pat. A reminder description of Kennis.

Alex, William, and Kennis finish their training.

Alex, William, Ruth, and Kennis begin to fight.  Alex fights along with
William and Kennis.  Ruth fights alone.

Ruth died.

Alex, William, and Kennis finish fighting.

Alex, William, and Kennis begin a trial.

# One Hundred And Sixty Eight

This chapter is from Alex's point of view. A reminder description of
Hullig during very early morning. Alex, William, Jose, Pat, and Kennis
are already present. A reminder description of Alex. A reminder
description of William. A reminder description of Jose. A reminder
description of Pat. A reminder description of Kennis.

Alex, William, and Kennis finish their trial.

Alex, William, Pat, and Kennis begin to fight.  Alex fights along with
William and Kennis.  Pat fights alone.

Pat died.

Alex, William, and Kennis finish fighting.

Alex, William, and Kennis begin some time together.

# One Hundred And Sixty Nine

This chapter is from Alex's point of view. A reminder description of
Hullig during early afternoon. Alex, William, Jose, and Kennis are
already present. A reminder description of Alex. A reminder description
of William. A reminder description of Jose. A reminder description of
Kennis.

Alex, William, and Kennis finish their time together.

Jose joins Alex and William.

Alex, William, Jose, and Kennis leave Hullig on the route to Jan.

# One Hundred And Seventy

This chapter is from Alex's point of view. Jan is 1,664.08 km away from
the start of the novel. It is located in Rizzian and it is a farm. A
detailed description of Jan during afternoon. Michael Hatmons is already
present. A detailed description of Michael. They are 19 years old. They
were born at Der in Rizzian.

Alex, William, Jose, and Kennis begin some time together.

# One Hundred And Seventy One

This chapter is from Alex's point of view. A reminder description of Jan
during late evening. Alex, William, Michael, Jose, and Kennis are already
present. A reminder description of Alex. A reminder description of
William. A reminder description of Michael. A reminder description of
Jose. A reminder description of Kennis.

Alex, William, Jose, and Kennis finish their time together.

Alex, William, Jose, and Kennis begin a trial.

# One Hundred And Seventy Two

This chapter is from Alex's point of view. A reminder description of Jan
during midnight. Alex, William, Michael, Jose, and Kennis are already
present. A reminder description of Alex. A reminder description of
William. A reminder description of Michael. A reminder description of
Jose. A reminder description of Kennis.

Alex, William, Jose, and Kennis finish their trial.

Alex, William, Jose, and Kennis start to train together.

# One Hundred And Seventy Three

This chapter is from Vin's point of view. Pennan is 438.40 km away from
the start of the novel. It is located in Ritt and it is a swamp. A
detailed description of Pennan during late morning. Iehironobu Yoshi and
Kyuki Ishisa are already present. A detailed description of Iehironobu.
They are 20 years old. They were born at Koyarde in Ele. A detailed
description of Kyuki. They are 20 years old. They were born at Ocheva in
Ele.

Vin died.

Kyuki died.

# One Hundred And Seventy Four

This chapter is from Alex's point of view. A medium description of Jan
during early afternoon. Alex, William, Michael, Jose, and Kennis are
already present. A reminder description of Alex. A reminder description
of William. A reminder description of Michael. A reminder description of
Jose. A reminder description of Kennis.

Alex, William, Jose, and Kennis finish their training.

Alex, William, Michael, Jose, and Kennis begin to fight.  Alex fights
along with William, Jose, and Kennis.  Michael fights alone.

William died.

Michael died.

Alex, Jose, and Kennis finish fighting.

Alex, Jose, and Kennis leave Jan on the route to Pullowskin.

# One Hundred And Seventy Five

This chapter is from Nena's point of view. A medium description of Cord
during early morning. Nena, Elly, Vin, Yvonna, Ben, Eliz, Juanicole, and
Laren are already present. A reminder description of Nena. A reminder
description of Elly. A reminder description of Vin. A reminder
description of Yvonna. A reminder description of Ben. A reminder
description of Eliz. A reminder description of Juanicole. A reminder
description of Laren.

Elly died.

Jose died.

# One Hundred And Seventy Six

This chapter is from Alex's point of view. Pullowskin is 1,806.11 km away
from the start of the novel. It is located in Rizzian and it is a town. A
detailed description of Pullowskin during early evening. Lissa Cleople,
Paulie Gregger, and Man Lyonson are already present. A detailed
description of Lissa. They are 18 years old. They were born at Gouze in
Ber. A detailed description of Paulie. They are 17 years old. They were
born at Um in Ber. A detailed description of Man. They are 19 years old.
They were born at Filley in Ber.

Alex, Jose, and Kennis begin a trial.

# One Hundred And Seventy Seven

This chapter is from Alex's point of view. A reminder description of
Pullowskin during midnight. Alex, Lissa, Paulie, Jose, Man, and Kennis
are already present. A reminder description of Alex. A reminder
description of Lissa. A reminder description of Paulie. A reminder
description of Jose. A reminder description of Man. A reminder
description of Kennis.

Alex, Jose, and Kennis finish their trial.

Man joins Alex and Jose.

Paulie joins Alex, Fred, and Joshua.

Alex, Paulie, Jose, Man, and Kennis start to train together.

# One Hundred And Seventy Eight

This chapter is from Alex's point of view. A reminder description of
Pullowskin during late morning. Alex, Lissa, Paulie, Jose, Man, and
Kennis are already present. A reminder description of Alex. A reminder
description of Lissa. A reminder description of Paulie. A reminder
description of Jose. A reminder description of Man. A reminder
description of Kennis.

Alex, Paulie, Jose, Man, and Kennis finish their training.

Alex, Paulie, Jose, Man, and Kennis begin some time together.

# One Hundred And Seventy Nine

This chapter is from Alex's point of view. A reminder description of
Pullowskin during late evening. Alex, Lissa, Paulie, Jose, Man, and
Kennis are already present. A reminder description of Alex. A reminder
description of Lissa. A reminder description of Paulie. A reminder
description of Jose. A reminder description of Man. A reminder
description of Kennis.

Alex, Paulie, Jose, Man, and Kennis finish their time together.

Alex, Lissa, Paulie, Jose, Man, and Kennis begin to fight.  Alex fights
along with Paulie, Jose, Man, and Kennis.  Lissa fights alone.

Paulie died.

Lissa died.

Alex, Jose, Man, and Kennis finish fighting.

Alex, Jose, Man, and Kennis leave Pullowskin on the route to Cord.

# One Hundred And Eighty

This chapter is from Alex's point of view. A medium description of Cord
during noon. Nena, Vin, Yvonna, Ben, Eliz, Juanicole, and Laren are
already present. A reminder description of Nena. A reminder description
of Vin. A reminder description of Yvonna. A reminder description of Ben.
A reminder description of Eliz. A reminder description of Juanicole. A
reminder description of Laren.

Alex, Jose, Laren, Man, and Kennis begin to fight.  Alex fights along
with Jose, Man, and Kennis.  Laren fights alone.

Laren died.

Alex, Jose, Man, and Kennis finish fighting.

Alex, Elly, Jose, Man, and Kennis begin to fight.  Alex fights along with
Jose, Man, and Kennis.  Elly fights alone.

Alex and Kennis finish fighting.

Vin joins Alex and Olly.

Alex, Vin, and Kennis leave Cord on the route to Toalsky.

# One Hundred And Eighty One

This chapter is from Alex's point of view. Toalsky is 223.23 km away from
the start of the novel. It is located in Quellie and it is a mountain. A
detailed description of Toalsky during morning.

Alex, Vin, and Kennis begin some time together.

# One Hundred And Eighty Two

This chapter is from Alex's point of view. A reminder description of
Toalsky during early evening. Alex, Vin, and Kennis are already present.
A reminder description of Alex. A reminder description of Vin. A reminder
description of Kennis.

Alex, Vin, and Kennis finish their time together.

Alex, Vin, and Kennis start to train together.

# One Hundred And Eighty Three

This chapter is from Alex's point of view. A reminder description of
Toalsky during early morning. Alex, Vin, and Kennis are already present.
A reminder description of Alex. A reminder description of Vin. A reminder
description of Kennis.

Alex, Vin, and Kennis finish their training.

Alex, Vin, and Kennis begin a trial.

# One Hundred And Eighty Four

This chapter is from Alex's point of view. A reminder description of
Toalsky during morning. Alex, Vin, and Kennis are already present. A
reminder description of Alex. A reminder description of Vin. A reminder
description of Kennis.

Alex, Vin, and Kennis finish their trial.

Alex, Vin, and Kennis leave Toalsky on the route to Cord.

# One Hundred And Eighty Five

This chapter is from Alex's point of view. A medium description of Cord
during morning. Nena, Yvonna, Ben, Eliz, Jose, Juanicole, and Man are
already present. A reminder description of Nena. A reminder description
of Yvonna. A reminder description of Ben. A reminder description of Eliz.
A reminder description of Jose. A reminder description of Juanicole. A
reminder description of Man.

Nena joins Alex.

Alex, Nena, Vin, Yvonna, and Kennis begin to fight.  Alex fights along
with Nena, Vin, and Kennis.  Yvonna fights alone.

Juanicole joins Yvonna.

Eliz joins Yvonna.

Yvonna died.

Alex, Nena, Vin, and Kennis finish fighting.

Alex, Nena, Vin, and Kennis leave Cord on the route to Payetman.

# One Hundred And Eighty Six

This chapter is from Alex's point of view. Payetman is 182.57 km away
from the start of the novel. It is located in Quellie and it is a swamp.
A detailed description of Payetman during late evening.

# One Hundred And Eighty Seven

This chapter is from Alex's point of view. A reminder description of
Payetman during late morning. Alex, Nena, Vin, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Vin. A reminder description of Kennis.

Alex, Nena, Vin, and Kennis leave Payetman on the route to Cord.

# One Hundred And Eighty Eight

This chapter is from Alex's point of view. A medium description of Cord
during late morning. Ben, Eliz, Jose, Juanicole, and Man are already
present. A reminder description of Ben. A reminder description of Eliz. A
reminder description of Jose. A reminder description of Juanicole. A
reminder description of Man.

Alex, Nena, Vin, and Kennis begin a trial.

# One Hundred And Eighty Nine

This chapter is from Alex's point of view. A reminder description of Cord
during early evening. Alex, Nena, Vin, Ben, Eliz, Jose, Juanicole, Man,
and Kennis are already present. A reminder description of Alex. A
reminder description of Nena. A reminder description of Vin. A reminder
description of Ben. A reminder description of Eliz. A reminder
description of Jose. A reminder description of Juanicole. A reminder
description of Man. A reminder description of Kennis.

Alex, Nena, Vin, and Kennis finish their trial.

Alex, Nena, Vin, and Kennis start to train together.

# One Hundred And Ninety

This chapter is from Alex's point of view. A reminder description of Cord
during late evening. Alex, Nena, Vin, Ben, Eliz, Jose, Juanicole, Man,
and Kennis are already present. A reminder description of Alex. A
reminder description of Nena. A reminder description of Vin. A reminder
description of Ben. A reminder description of Eliz. A reminder
description of Jose. A reminder description of Juanicole. A reminder
description of Man. A reminder description of Kennis.

Alex, Nena, Vin, and Kennis finish their training.

Alex, Nena, Vin, and Kennis leave Cord on the route to Wirt.

# One Hundred And Ninety One

This chapter is from Alex's point of view. Wirt is 256.73 km away from
the start of the novel. It is located in Quellie and it is a river. A
detailed description of Wirt during midnight. Atsuharu Nakahara is
already present. A detailed description of Atsuharu. They are 16 years
old. They were born at Cowley in Ritt.

Alex, Nena, Vin, Atsuharu, and Kennis begin to fight.  Alex fights along
with Nena, Vin, and Kennis.  Atsuharu fights alone.

Atsuharu died.

Alex, Nena, Vin, and Kennis finish fighting.

Alex, Nena, Vin, and Kennis start to train together.

# One Hundred And Ninety Two

This chapter is from Alex's point of view. A reminder description of Wirt
during morning. Alex, Nena, Vin, and Kennis are already present. A
reminder description of Alex. A reminder description of Nena. A reminder
description of Vin. A reminder description of Kennis.

Alex, Nena, Vin, and Kennis finish their training.

Alex, Nena, Vin, and Kennis leave Wirt on the route to Pennan.

# One Hundred And Ninety Three

This chapter is from Alex's point of view. A medium description of Pennan
during evening. Iehironobu is already present. A reminder description of
Iehironobu.

Alex, Nena, Vin, Kyuki, and Kennis begin to fight.  Alex fights along
with Nena, Vin, and Kennis.  Kyuki fights alone.

Alex, Nena, and Kennis finish fighting.

Alex, Nena, Iehironobu, and Kennis begin to fight.  Alex fights along
with Nena and Kennis.  Iehironobu fights alone.

Iehironobu died.

Alex, Nena, and Kennis finish fighting.

Alex, Nena, and Kennis leave Pennan on the route to Dubon.

# One Hundred And Ninety Four

This chapter is from Alex's point of view. Dubon is 677.13 km away from
the start of the novel. It is located in Ele and it is a mountain. A
detailed description of Dubon during evening.

Alex, Nena, and Kennis begin some time together.

# One Hundred And Ninety Five

This chapter is from Alex's point of view. A reminder description of
Dubon during very early morning. Alex, Nena, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Kennis.

Alex, Nena, and Kennis finish their time together.

Alex, Nena, and Kennis begin a trial.

# One Hundred And Ninety Six

This chapter is from Alex's point of view. A reminder description of
Dubon during morning. Alex, Nena, and Kennis are already present. A
reminder description of Alex. A reminder description of Nena. A reminder
description of Kennis.

Alex, Nena, and Kennis finish their trial.

Alex, Nena, and Kennis leave Dubon on the route to Pholmit.

# One Hundred And Ninety Seven

This chapter is from Alex's point of view. Pholmit is 765.77 km away from
the start of the novel. It is located in Cring and it is a forest. A
detailed description of Pholmit during very early morning.

Alex, Nena, and Kennis begin a trial.

# One Hundred And Ninety Eight

This chapter is from Alex's point of view. A reminder description of
Pholmit during noon. Alex, Nena, and Kennis are already present. A
reminder description of Alex. A reminder description of Nena. A reminder
description of Kennis.

Alex, Nena, and Kennis finish their trial.

Alex, Nena, and Kennis begin some time together.

# One Hundred And Ninety Nine

This chapter is from Alex's point of view. A reminder description of
Pholmit during near midnight. Alex, Nena, and Kennis are already present.
A reminder description of Alex. A reminder description of Nena. A
reminder description of Kennis.

Alex, Nena, and Kennis finish their time together.

Alex, Nena, and Kennis start to train together.

# Two Hundred

This chapter is from Alex's point of view. A reminder description of
Pholmit during morning. Alex, Nena, and Kennis are already present. A
reminder description of Alex. A reminder description of Nena. A reminder
description of Kennis.

Alex, Nena, and Kennis finish their training.

Alex, Nena, and Kennis leave Pholmit on the route to Dubon.

# Two Hundred And One

This chapter is from Alex's point of view. A medium description of Dubon
during morning.

Alex, Nena, and Kennis leave Dubon on the route to Halka.

# Two Hundred And Two

This chapter is from Alex's point of view. Halka is 934.97 km away from
the start of the novel. It is located in Gowders and it is a mountain. A
detailed description of Halka during noon. Natsuko Asaka is already
present. A detailed description of Natsuko. They are 16 years old. They
were born at Pes in Gowders.

Alex, Nena, and Kennis begin some time together.

# Two Hundred And Three

This chapter is from Alex's point of view. A reminder description of
Halka during late evening. Alex, Nena, Natsuko, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Natsuko. A reminder description of Kennis.

Alex, Nena, and Kennis finish their time together.

Alex, Nena, and Kennis begin a trial.

# Two Hundred And Four

This chapter is from Alex's point of view. A reminder description of
Halka during very early morning. Alex, Nena, Natsuko, and Kennis are
already present. A reminder description of Alex. A reminder description
of Nena. A reminder description of Natsuko. A reminder description of
Kennis.

Alex, Nena, and Kennis finish their trial.

Alex, Nena, and Kennis leave Halka on the route to Demrahim.

# Two Hundred And Five

This chapter is from Alex's point of view. Demrahim is 957.18 km away
from the start of the novel. It is located in Gowders and it is a swamp.
A detailed description of Demrahim during morning.

# Two Hundred And Six

This chapter is from Alex's point of view. A reminder description of
Demrahim during noon. Alex, Nena, and Kennis are already present. A
reminder description of Alex. A reminder description of Nena. A reminder
description of Kennis.

Alex, Nena, and Kennis leave Demrahim on the route to Halka.

# Two Hundred And Seven

This chapter is from Alex's point of view. A medium description of Halka
during noon. Natsuko is already present. A reminder description of
Natsuko.

Natsuko joins Alex, Nena, Vin, and Paulie.

Alex, Nena, Natsuko, and Kennis start to train together.

# Two Hundred And Eight

This chapter is from Alex's point of view. A reminder description of
Halka during late evening. Alex, Nena, Natsuko, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Natsuko. A reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their training.

Alex, Nena, Natsuko, and Kennis leave Halka on the route to Dubon.

# Two Hundred And Nine

This chapter is from Alex's point of view. A medium description of Dubon
during late evening.

Alex, Nena, Natsuko, and Kennis leave Dubon on the route to Ion.

# Two Hundred And Ten

This chapter is from Alex's point of view. Ion is 805.83 km away from the
start of the novel. It is located in Cring and it is a forest. A detailed
description of Ion during late evening.

Alex, Nena, Natsuko, and Kennis begin some time together.

# Two Hundred And Eleven

This chapter is from Alex's point of view. A reminder description of Ion
during early morning. Alex, Nena, Natsuko, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Natsuko. A reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their time together.

Alex, Nena, Natsuko, and Kennis leave Ion on the route to Dubon.

# Two Hundred And Twelve

This chapter is from Alex's point of view. A medium description of Dubon
during early morning.

Alex, Nena, Natsuko, and Kennis start to train together.

# Two Hundred And Thirteen

This chapter is from Alex's point of view. A reminder description of
Dubon during early afternoon. Alex, Nena, Natsuko, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Natsuko. A reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their training.

Alex, Nena, Natsuko, and Kennis leave Dubon on the route to Pennan.

# Two Hundred And Fourteen

This chapter is from Alex's point of view. A medium description of Pennan
during early afternoon. Vin is already present. A reminder description of
Vin.

Alex, Nena, Natsuko, and Kennis begin some time together.

# Two Hundred And Fifteen

This chapter is from Alex's point of view. A reminder description of
Pennan during late evening. Alex, Nena, Vin, Natsuko, and Kennis are
already present. A reminder description of Alex. A reminder description
of Nena. A reminder description of Vin. A reminder description of
Natsuko. A reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their time together.

Alex, Nena, Natsuko, and Kennis start to train together.

# Two Hundred And Sixteen

This chapter is from Alex's point of view. A reminder description of
Pennan during very early morning. Alex, Nena, Vin, Natsuko, and Kennis
are already present. A reminder description of Alex. A reminder
description of Nena. A reminder description of Vin. A reminder
description of Natsuko. A reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their training.

Alex, Nena, Natsuko, and Kennis begin a trial.

# Two Hundred And Seventeen

This chapter is from Alex's point of view. A reminder description of
Pennan during morning. Alex, Nena, Vin, Natsuko, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Vin. A reminder description of Natsuko. A
reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their trial.

Alex, Nena, Natsuko, and Kennis leave Pennan on the route to Beul.

# Two Hundred And Eighteen

This chapter is from Alex's point of view. Beul is 675.68 km away from
the start of the novel. It is located in Ele and it is a swamp. A
detailed description of Beul during morning. Chūichi Ichiono is already
present. A detailed description of Chūichi. They are 20 years old. They
were born at Raj in Gowders.

Alex, Nena, Natsuko, and Kennis start to train together.

# Two Hundred And Nineteen

This chapter is from Alex's point of view. A reminder description of Beul
during late morning. Alex, Nena, Natsuko, Chūichi, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Natsuko. A reminder description of Chūichi. A
reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their training.

Alex, Nena, Natsuko, and Kennis begin some time together.

# Two Hundred And Twenty

This chapter is from Alex's point of view. A reminder description of Beul
during midnight. Alex, Nena, Natsuko, Chūichi, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Natsuko. A reminder description of Chūichi. A
reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their time together.

Alex, Nena, Natsuko, Chūichi, and Kennis begin to fight.  Alex fights
along with Nena, Natsuko, and Kennis.  Chūichi fights alone.

Chūichi died.

Alex, Nena, Natsuko, and Kennis finish fighting.

Alex, Nena, Natsuko, and Kennis begin a trial.

# Two Hundred And Twenty One

This chapter is from Alex's point of view. A reminder description of Beul
during early morning. Alex, Nena, Natsuko, and Kennis are already
present. A reminder description of Alex. A reminder description of Nena.
A reminder description of Natsuko. A reminder description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their trial.

Alex, Nena, Natsuko, and Kennis leave Beul on the route to Higan.

# Two Hundred And Twenty Two

This chapter is from Alex's point of view. Higan is 820.39 km away from
the start of the novel. It is located in Cring and it is a forest. A
detailed description of Higan during morning. Nori Dairata and Bunta Kite
are already present. A detailed description of Nori. They are 20 years
old. They were born at Loft in Gowders. A detailed description of Bunta.
They are 19 years old. They were born at Man in Gowders.

Alex, Nena, Natsuko, and Kennis begin some time together.

# Two Hundred And Twenty Three

This chapter is from Alex's point of view. A reminder description of
Higan during late evening. Alex, Nena, Nori, Natsuko, Bunta, and Kennis
are already present. A reminder description of Alex. A reminder
description of Nena. A reminder description of Nori. A reminder
description of Natsuko. A reminder description of Bunta. A reminder
description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their time together.

Alex, Nena, Natsuko, and Kennis begin a trial.

# Two Hundred And Twenty Four

This chapter is from Alex's point of view. A reminder description of
Higan during late morning. Alex, Nena, Nori, Natsuko, Bunta, and Kennis
are already present. A reminder description of Alex. A reminder
description of Nena. A reminder description of Nori. A reminder
description of Natsuko. A reminder description of Bunta. A reminder
description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their trial.

Alex, Nena, Natsuko, and Kennis start to train together.

# Two Hundred And Twenty Five

This chapter is from Alex's point of view. A reminder description of
Higan during early afternoon. Alex, Nena, Nori, Natsuko, Bunta, and
Kennis are already present. A reminder description of Alex. A reminder
description of Nena. A reminder description of Nori. A reminder
description of Natsuko. A reminder description of Bunta. A reminder
description of Kennis.

Alex, Nena, Natsuko, and Kennis finish their training.

Alex, Nena, Nori, Natsuko, and Kennis begin to fight.  Alex fights along
with Nena, Natsuko, and Kennis.  Nori fights alone.

Natsuko died.

Nena died.

Nori died.

Alex and Kennis finish fighting.

Alex, Bunta, and Kennis begin to fight.  Alex fights along with Kennis.
Bunta fights alone.

Bunta died.

Alex and Kennis finish fighting.

Alex and Kennis leave Higan on the route to Scudez.

# Two Hundred And Twenty Six

This chapter is from Alex's point of view. Scudez is 825.03 km away from
the start of the novel. It is located in Cring and it is a mountain. A
detailed description of Scudez during afternoon. Fujiko Umejima is
already present. A detailed description of Fujiko. They are 16 years old.
They were born at Kotzer in Gowders.

Alex and Kennis begin a trial.

# Two Hundred And Twenty Seven

This chapter is from Alex's point of view. A reminder description of
Scudez during afternoon. Alex, Fujiko, and Kennis are already present. A
reminder description of Alex. A reminder description of Fujiko. A
reminder description of Kennis.

Alex and Kennis finish their trial.

Alex, Fujiko, and Kennis begin to fight.  Alex fights along with Kennis.
Fujiko fights alone.

Fujiko died.

Alex and Kennis finish fighting.

Alex and Kennis begin some time together.

# Two Hundred And Twenty Eight

This chapter is from Alex's point of view. A reminder description of
Scudez during very early morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis start to train together.

# Two Hundred And Twenty Nine

This chapter is from Alex's point of view. A reminder description of
Scudez during early afternoon. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Scudez on the route to Horato.

# Two Hundred And Thirty

This chapter is from Alex's point of view. Horato is 1,100.68 km away
from the start of the novel. It is located in Gowders and it is a farm. A
detailed description of Horato during late evening. David Mooks is
already present. A detailed description of David. They are 20 years old.
They were born at Ikawa in Chet.

Alex, David, and Kennis begin to fight.  Alex fights along with Kennis.
David fights alone.

David died.

Alex and Kennis finish fighting.

Alex and Kennis begin a trial.

# Two Hundred And Thirty One

This chapter is from Alex's point of view. A reminder description of
Horato during morning. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis begin some time together.

# Two Hundred And Thirty Two

This chapter is from Alex's point of view. A reminder description of
Horato during early evening. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis leave Horato on the route to Zia.

# Two Hundred And Thirty Three

This chapter is from Alex's point of view. Zia is 1,204.97 km away from
the start of the novel. It is located in Chet and it is a mountain. A
detailed description of Zia during afternoon.

# Two Hundred And Thirty Four

This chapter is from Alex's point of view. A reminder description of Zia
during noon. Alex and Kennis are already present. A reminder description
of Alex. A reminder description of Kennis.

Alex and Kennis leave Zia on the route to Horato.

# Two Hundred And Thirty Five

This chapter is from Alex's point of view. A medium description of Horato
during noon.

Alex and Kennis start to train together.

# Two Hundred And Thirty Six

This chapter is from Alex's point of view. A reminder description of
Horato during evening. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Horato on the route to Cord.

# Two Hundred And Thirty Seven

This chapter is from Alex's point of view. A medium description of Cord
during very early morning. Ben, Eliz, Jose, Juanicole, and Man are
already present. A reminder description of Ben. A reminder description of
Eliz. A reminder description of Jose. A reminder description of
Juanicole. A reminder description of Man.

Alex and Kennis leave Cord on the route to Beuth.

# Two Hundred And Thirty Eight

This chapter is from Alex's point of view. Beuth is 31.63 km away from
the start of the novel. It is located in Quellie and it is a farm. A
detailed description of Beuth during morning.

Alex and Kennis begin a trial.

# Two Hundred And Thirty Nine

This chapter is from Alex's point of view. A reminder description of
Beuth during late morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis leave Beuth on the route to Roume.

# Two Hundred And Forty

This chapter is from Alex's point of view. Roume is 279.22 km away from
the start of the novel. It is located in Quellie and it is a river. A
detailed description of Roume during noon.

Alex and Kennis begin some time together.

# Two Hundred And Forty One

This chapter is from Alex's point of view. A reminder description of
Roume during late evening. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis start to train together.

# Two Hundred And Forty Two

This chapter is from Alex's point of view. A reminder description of
Roume during very early morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Roume on the route to Puttig.

# Two Hundred And Forty Three

This chapter is from Alex's point of view. Puttig is 402.80 km away from
the start of the novel. It is located in Ritt and it is a swamp. A
detailed description of Puttig during very early morning.

Alex and Kennis start to train together.

# Two Hundred And Forty Four

This chapter is from Alex's point of view. A reminder description of
Puttig during morning. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Puttig on the route to Cord.

# Two Hundred And Forty Five

This chapter is from Alex's point of view. A medium description of Cord
during near midnight. Ben, Eliz, Jose, Juanicole, and Man are already
present. A reminder description of Ben. A reminder description of Eliz. A
reminder description of Jose. A reminder description of Juanicole. A
reminder description of Man.

Alex, Ben, and Kennis begin to fight.  Alex fights along with Kennis.
Ben fights alone.

Ben died.

Alex and Kennis finish fighting.

Alex and Kennis leave Cord on the route to Civiehill.

# Two Hundred And Forty Six

This chapter is from Alex's point of view. Civiehill is 89.75 km away
from the start of the novel. It is located in Quellie and it is a lake. A
detailed description of Civiehill during early evening.

Alex and Kennis start to train together.

# Two Hundred And Forty Seven

This chapter is from Alex's point of view. A reminder description of
Civiehill during early evening. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis begin some time together.

# Two Hundred And Forty Eight

This chapter is from Alex's point of view. A reminder description of
Civiehill during very early morning. Alex and Kennis are already present.
A reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis begin a trial.

# Two Hundred And Forty Nine

This chapter is from Alex's point of view. A reminder description of
Civiehill during morning. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis leave Civiehill on the route to Dam.

# Two Hundred And Fifty

This chapter is from Alex's point of view. Dam is 380.09 km away from the
start of the novel. It is located in Ritt and it is a swamp. A detailed
description of Dam during evening.

# Two Hundred And Fifty One

This chapter is from Alex's point of view. A reminder description of Dam
during early morning. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis leave Dam on the route to Civiehill.

# Two Hundred And Fifty Two

This chapter is from Alex's point of view. A medium description of
Civiehill during early morning.

Alex and Kennis leave Civiehill on the route to Duffinson.

# Two Hundred And Fifty Three

This chapter is from Alex's point of view. Duffinson is 310.06 km away
from the start of the novel. It is located in Quellie and it is a city. A
detailed description of Duffinson during very early morning. Penee
Dewellace, Katsushi Sako, Yoshi Akagata, Jūbei Kojiri, Otoharuko Aka,
Toshi Sunagi, and Riko Eda are already present. A detailed description of
Penee. They are 20 years old. They were born at Croux in Quellie. A
detailed description of Katsushi. They are 17 years old. They were born
at Gaglianis in Ritt. A detailed description of Yoshi. They are 16 years
old. They were born at Bre in Ele. A detailed description of Jūbei. They
are 18 years old. They were born at Shine in Ritt. A detailed description
of Otoharuko. They are 19 years old. They were born at Brizzi in Ritt. A
detailed description of Toshi. They are 19 years old. They were born at
Cagnes in Ele. A detailed description of Riko. They are 17 years old.
They were born at Fers in Ritt.

Alex and Kennis leave Duffinson on the route to Paa.

# Two Hundred And Fifty Four

This chapter is from Alex's point of view. Paa is 525.90 km away from the
start of the novel. It is located in Ritt and it is a town. A detailed
description of Paa during late evening.

Alex and Kennis begin some time together.

# Two Hundred And Fifty Five

This chapter is from Alex's point of view. A reminder description of Paa
during late evening. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis begin a trial.

# Two Hundred And Fifty Six

This chapter is from Alex's point of view. A reminder description of Paa
during early morning. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis start to train together.

# Two Hundred And Fifty Seven

This chapter is from Alex's point of view. A reminder description of Paa
during evening. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Paa on the route to Duffinson.

# Two Hundred And Fifty Eight

This chapter is from Alex's point of view. A medium description of
Duffinson during evening. Penee, Katsushi, Yoshi, Jūbei, Otoharuko,
Toshi, and Riko are already present. A reminder description of Penee. A
reminder description of Katsushi. A reminder description of Yoshi. A
reminder description of Jūbei. A reminder description of Otoharuko. A
reminder description of Toshi. A reminder description of Riko.

Penee joins Alex.

Alex, Penee, Yoshi, and Kennis begin to fight.  Alex fights along with
Penee and Kennis.  Yoshi fights alone.

Alex and Kennis leave Duffinson on the route to Rackennon.

# Two Hundred And Fifty Nine

This chapter is from Alex's point of view. A reminder description of
Duffinson during evening. Alex, Penee, Katsushi, Yoshi, Jūbei, Otoharuko,
Toshi, Riko, and Kennis are already present. A reminder description of
Alex. A reminder description of Penee. A reminder description of
Katsushi. A reminder description of Yoshi. A reminder description of
Jūbei. A reminder description of Otoharuko. A reminder description of
Toshi. A reminder description of Riko. A reminder description of Kennis.

Yoshi leaves Duffinson on the route to Sipessel.

Penee leaves Duffinson on the route to Cal.

# Two Hundred And Sixty

This chapter is from Yoshi's point of view. Sipessel is 491.72 km away
from the start of the novel. It is located in Ritt and it is a river. A
detailed description of Sipessel during evening.

Yoshi leaves.

# Two Hundred And Sixty One

This chapter is from Penee's point of view. Cal is 592.37 km away from
the start of the novel. It is located in Ele and it is a farm. A detailed
description of Cal during evening.

Penee leaves.

# Two Hundred And Sixty Two

This chapter is from Alex's point of view. Rackennon is 526.39 km away
from the start of the novel. It is located in Ritt and it is a city. A
detailed description of Rackennon during early afternoon.

Alex and Kennis leave Rackennon on the route to Vigoldamek.

# Two Hundred And Sixty Three

This chapter is from Alex's point of view. Vigoldamek is 784.05 km away
from the start of the novel. It is located in Cring and it is a river. A
detailed description of Vigoldamek during early evening.

# Two Hundred And Sixty Four

This chapter is from Alex's point of view. A reminder description of
Vigoldamek during evening. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis leave Vigoldamek on the route to Duffinson.

# Two Hundred And Sixty Five

This chapter is from Alex's point of view. A medium description of
Duffinson during evening. Katsushi, Jūbei, Otoharuko, Toshi, and Riko are
already present. A reminder description of Katsushi. A reminder
description of Jūbei. A reminder description of Otoharuko. A reminder
description of Toshi. A reminder description of Riko.

Alex and Kennis finish fighting.

Alex, Jūbei, and Kennis begin to fight.  Alex fights along with Kennis.
Jūbei fights alone.

Jūbei leaves Duffinson on the route to State.

Alex and Kennis leave Duffinson on the route to Bathneer.

# Two Hundred And Sixty Six

This chapter is from Jūbei's point of view. State is 549.88 km away from
the start of the novel. It is located in Ritt and it is a swamp. A
detailed description of State during evening.

Jūbei leaves.

# Two Hundred And Sixty Seven

This chapter is from Alex's point of view. Bathneer is 434.74 km away
from the start of the novel. It is located in Ritt and it is a swamp. A
detailed description of Bathneer during late evening.

# Two Hundred And Sixty Eight

This chapter is from Alex's point of view. A reminder description of
Bathneer during late evening. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis leave Bathneer on the route to Duffinson.

# Two Hundred And Sixty Nine

This chapter is from Alex's point of view. A medium description of
Duffinson during late evening. Katsushi, Otoharuko, Toshi, and Riko are
already present. A reminder description of Katsushi. A reminder
description of Otoharuko. A reminder description of Toshi. A reminder
description of Riko.

Alex and Kennis finish fighting.

Alex and Kennis begin a trial.

# Two Hundred And Seventy

This chapter is from Alex's point of view. A reminder description of
Duffinson during very early morning. Alex, Katsushi, Otoharuko, Toshi,
Riko, and Kennis are already present. A reminder description of Alex. A
reminder description of Katsushi. A reminder description of Otoharuko. A
reminder description of Toshi. A reminder description of Riko. A reminder
description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis start to train together.

# Two Hundred And Seventy One

This chapter is from Alex's point of view. A reminder description of
Duffinson during morning. Alex, Katsushi, Otoharuko, Toshi, Riko, and
Kennis are already present. A reminder description of Alex. A reminder
description of Katsushi. A reminder description of Otoharuko. A reminder
description of Toshi. A reminder description of Riko. A reminder
description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Duffinson on the route to Mckel.

# Two Hundred And Seventy Two

This chapter is from Alex's point of view. Mckel is 388.81 km away from
the start of the novel. It is located in Ritt and it is a tundra. A
detailed description of Mckel during late evening. Motoyohisa Izumi is
already present. A detailed description of Motoyohisa. They are 20 years
old. They were born at Bring in Ele.

Alex and Kennis start to train together.

# Two Hundred And Seventy Three

This chapter is from Alex's point of view. A reminder description of
Mckel during morning. Alex, Motoyohisa, and Kennis are already present. A
reminder description of Alex. A reminder description of Motoyohisa. A
reminder description of Kennis.

Alex and Kennis finish their training.

Motoyohisa joins Alex.

Alex, Motoyohisa, and Kennis begin some time together.

# Two Hundred And Seventy Four

This chapter is from Alex's point of view. A reminder description of
Mckel during noon. Alex, Motoyohisa, and Kennis are already present. A
reminder description of Alex. A reminder description of Motoyohisa. A
reminder description of Kennis.

Alex, Motoyohisa, and Kennis finish their time together.

Alex, Motoyohisa, and Kennis begin a trial.

# Two Hundred And Seventy Five

This chapter is from Alex's point of view. A reminder description of
Mckel during afternoon. Alex, Motoyohisa, and Kennis are already present.
A reminder description of Alex. A reminder description of Motoyohisa. A
reminder description of Kennis.

Alex, Motoyohisa, and Kennis finish their trial.

Alex, Motoyohisa, and Kennis leave Mckel on the route to Duffinson.

# Two Hundred And Seventy Six

This chapter is from Alex's point of view. A medium description of
Duffinson during afternoon. Katsushi, Otoharuko, Toshi, and Riko are
already present. A reminder description of Katsushi. A reminder
description of Otoharuko. A reminder description of Toshi. A reminder
description of Riko.

Katsushi joins Alex and Penee.

Alex, Katsushi, Motoyohisa, and Kennis leave Duffinson on the route to
Osman.

# Two Hundred And Seventy Seven

This chapter is from Alex's point of view. Osman is 435.44 km away from
the start of the novel. It is located in Ritt and it is a forest. A
detailed description of Osman during afternoon. Ryūsuke Nara, Rie Ashi,
and Wako Ueno are already present. A detailed description of Ryūsuke.
They are 17 years old. They were born at Dorf in Ritt. A detailed
description of Rie. They are 18 years old. They were born at Cleglione in
Ritt. A detailed description of Wako. They are 19 years old. They were
born at Lottar in Ele.

Alex, Katsushi, Motoyohisa, Rie, and Kennis begin to fight.  Alex fights
along with Katsushi, Motoyohisa, and Kennis.  Rie fights alone.

Katsushi died.

Rie died.

Alex, Motoyohisa, and Kennis finish fighting.

Alex, Motoyohisa, and Kennis start to train together.

# Two Hundred And Seventy Eight

This chapter is from Alex's point of view. A reminder description of
Osman during very early morning. Alex, Ryūsuke, Motoyohisa, Wako, and
Kennis are already present. A reminder description of Alex. A reminder
description of Ryūsuke. A reminder description of Motoyohisa. A reminder
description of Wako. A reminder description of Kennis.

Alex, Motoyohisa, and Kennis finish their training.

Alex, Motoyohisa, and Kennis begin a trial.

# Two Hundred And Seventy Nine

This chapter is from Alex's point of view. A reminder description of
Osman during morning. Alex, Ryūsuke, Motoyohisa, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Ryūsuke. A reminder description of Motoyohisa. A reminder description
of Wako. A reminder description of Kennis.

Alex, Motoyohisa, and Kennis finish their trial.

Alex, Motoyohisa, and Kennis leave Osman on the route to Gado.

# Two Hundred And Eighty

This chapter is from Alex's point of view. Gado is 732.31 km away from
the start of the novel. It is located in Ele and it is a forest. A
detailed description of Gado during early evening.

# Two Hundred And Eighty One

This chapter is from Alex's point of view. A reminder description of Gado
during early morning. Alex, Motoyohisa, and Kennis are already present. A
reminder description of Alex. A reminder description of Motoyohisa. A
reminder description of Kennis.

Alex, Motoyohisa, and Kennis leave Gado on the route to Osman.

# Two Hundred And Eighty Two

This chapter is from Alex's point of view. A medium description of Osman
during early morning. Ryūsuke and Wako are already present. A reminder
description of Ryūsuke. A reminder description of Wako.

Alex, Motoyohisa, and Kennis leave Osman on the route to Pencione.

# Two Hundred And Eighty Three

This chapter is from Alex's point of view. Pencione is 663.74 km away
from the start of the novel. It is located in Ele and it is a lake. A
detailed description of Pencione during very early morning.

Alex, Motoyohisa, and Kennis start to train together.

# Two Hundred And Eighty Four

This chapter is from Alex's point of view. A reminder description of
Pencione during early morning. Alex, Motoyohisa, and Kennis are already
present. A reminder description of Alex. A reminder description of
Motoyohisa. A reminder description of Kennis.

Alex, Motoyohisa, and Kennis finish their training.

Alex, Motoyohisa, and Kennis leave Pencione on the route to Osman.

# Two Hundred And Eighty Five

This chapter is from Alex's point of view. A medium description of Osman
during early morning. Ryūsuke and Wako are already present. A reminder
description of Ryūsuke. A reminder description of Wako.

Alex, Ryūsuke, Motoyohisa, and Kennis begin to fight.  Alex fights along
with Motoyohisa and Kennis.  Ryūsuke fights alone.

Ryūsuke died.

Alex, Motoyohisa, and Kennis finish fighting.

Alex, Motoyohisa, and Kennis begin some time together.

# Two Hundred And Eighty Six

This chapter is from Alex's point of view. A reminder description of
Osman during early afternoon. Alex, Motoyohisa, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Motoyohisa. A reminder description of Wako. A reminder description of
Kennis.

Alex, Motoyohisa, and Kennis finish their time together.

Wako joins Alex and Motoyohisa.

Alex, Motoyohisa, Wako, and Kennis leave Osman on the route to Gland.

# Two Hundred And Eighty Seven

This chapter is from Alex's point of view. Gland is 607.21 km away from
the start of the novel. It is located in Ele and it is a river. A
detailed description of Gland during very early morning.

Alex, Motoyohisa, Wako, and Kennis begin a trial.

# Two Hundred And Eighty Eight

This chapter is from Alex's point of view. A reminder description of
Gland during morning. Alex, Motoyohisa, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Motoyohisa. A reminder description of Wako. A reminder description of
Kennis.

Alex, Motoyohisa, Wako, and Kennis finish their trial.

Alex, Motoyohisa, Wako, and Kennis start to train together.

# Two Hundred And Eighty Nine

This chapter is from Alex's point of view. A reminder description of
Gland during early evening. Alex, Motoyohisa, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Motoyohisa. A reminder description of Wako. A reminder description of
Kennis.

Alex, Motoyohisa, Wako, and Kennis finish their training.

Alex, Motoyohisa, Wako, and Kennis begin some time together.

# Two Hundred And Ninety

This chapter is from Alex's point of view. A reminder description of
Gland during early morning. Alex, Motoyohisa, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Motoyohisa. A reminder description of Wako. A reminder description of
Kennis.

Alex, Motoyohisa, Wako, and Kennis finish their time together.

Alex, Motoyohisa, Wako, and Kennis leave Gland on the route to Sunsolds.

# Two Hundred And Ninety One

This chapter is from Alex's point of view. Sunsolds is 666.81 km away
from the start of the novel. It is located in Ele and it is a mountain. A
detailed description of Sunsolds during afternoon. Chie Waki, Fumi Hara,
Chiemikue Ōbara, Motokiko Hirayami, Honami Orimoto, Miho Akami, and
Yahideo Ino are already present. A detailed description of Chie. They are
16 years old. They were born at Ston in Ele. A detailed description of
Fumi. They are 18 years old. They were born at From in Cring. A detailed
description of Chiemikue. They are 17 years old. They were born at
Tramoth in Gowders. A detailed description of Motokiko. They are 19 years
old. They were born at Pot in Ele. A detailed description of Honami. They
are 18 years old. They were born at Rend in Cring. A detailed description
of Miho. They are 19 years old. They were born at Dekarvin in Cring. A
detailed description of Yahideo. They are 20 years old. They were born at
Lick in Cring.

Alex, Motoyohisa, Wako, and Kennis begin a trial.

# Two Hundred And Ninety Two

This chapter is from Alex's point of view. A reminder description of
Sunsolds during midnight. Alex, Chie, Fumi, Chiemikue, Motokiko, Honami,
Miho, Yahideo, Motoyohisa, Wako, and Kennis are already present. A
reminder description of Alex. A reminder description of Chie. A reminder
description of Fumi. A reminder description of Chiemikue. A reminder
description of Motokiko. A reminder description of Honami. A reminder
description of Miho. A reminder description of Yahideo. A reminder
description of Motoyohisa. A reminder description of Wako. A reminder
description of Kennis.

Alex, Motoyohisa, Wako, and Kennis finish their trial.

Alex, Motoyohisa, Wako, and Kennis leave Sunsolds on the route to
Langvyen.

# Two Hundred And Ninety Three

This chapter is from Alex's point of view. Langvyen is 789.22 km away
from the start of the novel. It is located in Cring and it is a city. A
detailed description of Langvyen during very early morning. Noboru Fujin
is already present. A detailed description of Noboru. They are 19 years
old. They were born at Chata in Gowders.

Alex, Motoyohisa, Noboru, Wako, and Kennis begin to fight.  Alex fights
along with Motoyohisa, Wako, and Kennis.  Noboru fights alone.

Motoyohisa died.

Noboru died.

Alex, Wako, and Kennis finish fighting.

Alex, Wako, and Kennis start to train together.

# Two Hundred And Ninety Four

This chapter is from Alex's point of view. A reminder description of
Langvyen during early morning. Alex, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of Wako.
A reminder description of Kennis.

Alex, Wako, and Kennis finish their training.

Alex, Wako, and Kennis begin a trial.

# Two Hundred And Ninety Five

This chapter is from Alex's point of view. A reminder description of
Langvyen during early afternoon. Alex, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of Wako.
A reminder description of Kennis.

Alex, Wako, and Kennis finish their trial.

Alex, Wako, and Kennis leave Langvyen on the route to Sunsolds.

# Two Hundred And Ninety Six

This chapter is from Alex's point of view. A medium description of
Sunsolds during afternoon. Chie, Fumi, Chiemikue, Motokiko, Honami, Miho,
and Yahideo are already present. A reminder description of Chie. A
reminder description of Fumi. A reminder description of Chiemikue. A
reminder description of Motokiko. A reminder description of Honami. A
reminder description of Miho. A reminder description of Yahideo.

Honami joins Alex, Katsushi, and Paulie.

Alex, Chie, Honami, Wako, and Kennis begin to fight.  Alex fights along
with Honami, Wako, and Kennis.  Chie fights alone.

Alex, Wako, and Kennis finish fighting.

Honami leaves Sunsolds on the route to Gobel.

Alex, Wako, and Kennis start to train together.

Chie leaves Sunsolds on the route to Lam.

# Two Hundred And Ninety Seven

This chapter is from Honami's point of view. Gobel is 904.23 km away from
the start of the novel. It is located in Gowders and it is a farm. A
detailed description of Gobel during afternoon.

Honami leaves.

# Two Hundred And Ninety Eight

This chapter is from Chie's point of view. Lam is 701.19 km away from the
start of the novel. It is located in Ele and it is a lake. A detailed
description of Lam during afternoon.

Chie leaves.

# Two Hundred And Ninety Nine

This chapter is from Alex's point of view. A medium description of
Sunsolds during very early morning. Alex, Fumi, Chiemikue, Motokiko,
Miho, Yahideo, Wako, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Motokiko. A reminder
description of Miho. A reminder description of Yahideo. A reminder
description of Wako. A reminder description of Kennis.

Alex, Wako, and Kennis finish their training.

Alex, Wako, and Kennis begin some time together.

# Three Hundred

This chapter is from Alex's point of view. A reminder description of
Sunsolds during noon. Alex, Fumi, Chiemikue, Motokiko, Miho, Yahideo,
Wako, and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Chiemikue. A
reminder description of Motokiko. A reminder description of Miho. A
reminder description of Yahideo. A reminder description of Wako. A
reminder description of Kennis.

Alex, Wako, and Kennis finish their time together.

Alex, Wako, and Kennis leave Sunsolds on the route to Col.

# Three Hundred And One

This chapter is from Alex's point of view. Col is 762.80 km away from the
start of the novel. It is located in Cring and it is a river. A detailed
description of Col during morning. Ryūki Sugi, Daizo Enobuki, Fukumika
Kumura, Hideji Akagamō, Etsumiko Noguchida, Rumiko Yuhara, Itsumiki
Teraokawa, and Ippei Yanashida are already present. A detailed
description of Ryūki. They are 16 years old. They were born at Iffmancero
in Gowders. A detailed description of Daizo. They are 17 years old. They
were born at Scherten in Gowders. A detailed description of Fukumika.
They are 19 years old. They were born at Catester in Cring. A detailed
description of Hideji. They are 16 years old. They were born at Cont in
Cring. A detailed description of Etsumiko. They are 16 years old. They
were born at Robe in Gowders. A detailed description of Rumiko. They are
16 years old. They were born at Ogan in Gowders. A detailed description
of Itsumiki. They are 17 years old. They were born at Rami in Cring. A
detailed description of Ippei. They are 17 years old. They were born at
Cord in Cring.

Ryūki joins Alex and Olly.

Alex, Ryūki, Wako, and Kennis leave Col on the route to Rue.

# Three Hundred And Two

This chapter is from Alex's point of view. Rue is 839.42 km away from the
start of the novel. It is located in Cring and it is a mountain. A
detailed description of Rue during near midnight. Tamamiko Okidoro is
already present. A detailed description of Tamamiko. They are 20 years
old. They were born at Mank in Gowders.

Tamamiko joins Alex, Ryūki, and Motoyohisa.

Alex, Ryūki, Tamamiko, Wako, and Kennis start to train together.

# Three Hundred And Three

This chapter is from Alex's point of view. A reminder description of Rue
during morning. Alex, Ryūki, Tamamiko, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of Ryūki.
A reminder description of Tamamiko. A reminder description of Wako. A
reminder description of Kennis.

Alex, Ryūki, Tamamiko, Wako, and Kennis finish their training.

Alex, Ryūki, Tamamiko, Wako, and Kennis begin some time together.

# Three Hundred And Four

This chapter is from Alex's point of view. A reminder description of Rue
during afternoon. Alex, Ryūki, Tamamiko, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of Ryūki.
A reminder description of Tamamiko. A reminder description of Wako. A
reminder description of Kennis.

Alex, Ryūki, Tamamiko, Wako, and Kennis finish their time together.

Alex, Ryūki, Tamamiko, Wako, and Kennis leave Rue on the route to Col.

# Three Hundred And Five

This chapter is from Alex's point of view. A medium description of Col
during afternoon. Daizo, Fukumika, Hideji, Etsumiko, Rumiko, Itsumiki,
and Ippei are already present. A reminder description of Daizo. A
reminder description of Fukumika. A reminder description of Hideji. A
reminder description of Etsumiko. A reminder description of Rumiko. A
reminder description of Itsumiki. A reminder description of Ippei.

Rumiko joins Alex, Ryūki, Motoyohisa, and Tamamiko.

Alex, Ryūki, Tamamiko, Rumiko, Wako, and Kennis begin some time together.

# Three Hundred And Six

This chapter is from Alex's point of view. A reminder description of Col
during early evening. Alex, Ryūki, Daizo, Fukumika, Hideji, Tamamiko,
Etsumiko, Rumiko, Itsumiki, Ippei, Wako, and Kennis are already present.
A reminder description of Alex. A reminder description of Ryūki. A
reminder description of Daizo. A reminder description of Fukumika. A
reminder description of Hideji. A reminder description of Tamamiko. A
reminder description of Etsumiko. A reminder description of Rumiko. A
reminder description of Itsumiki. A reminder description of Ippei. A
reminder description of Wako. A reminder description of Kennis.

Alex, Ryūki, Tamamiko, Rumiko, Wako, and Kennis finish their time
together.

Fukumika joins Alex, Ryūki, and Paulie.

Itsumiki joins Alex, Ryūki, Fukumika, Motoyohisa, Tamamiko, and Rumiko.

Alex, Ryūki, Daizo, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and
Kennis begin to fight.  Alex fights along with Ryūki, Fukumika, Tamamiko,
Rumiko, Itsumiki, Wako, and Kennis.  Daizo fights alone.

Daizo died.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis
finish fighting.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis start
to train together.

# Three Hundred And Seven

This chapter is from Alex's point of view. A reminder description of Col
during near midnight. Alex, Ryūki, Fukumika, Hideji, Tamamiko, Etsumiko,
Rumiko, Itsumiki, Ippei, Wako, and Kennis are already present. A reminder
description of Alex. A reminder description of Ryūki. A reminder
description of Fukumika. A reminder description of Hideji. A reminder
description of Tamamiko. A reminder description of Etsumiko. A reminder
description of Rumiko. A reminder description of Itsumiki. A reminder
description of Ippei. A reminder description of Wako. A reminder
description of Kennis.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis
finish their training.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis begin
a trial.

# Three Hundred And Eight

This chapter is from Alex's point of view. A reminder description of Col
during very early morning. Alex, Ryūki, Fukumika, Hideji, Tamamiko,
Etsumiko, Rumiko, Itsumiki, Ippei, Wako, and Kennis are already present.
A reminder description of Alex. A reminder description of Ryūki. A
reminder description of Fukumika. A reminder description of Hideji. A
reminder description of Tamamiko. A reminder description of Etsumiko. A
reminder description of Rumiko. A reminder description of Itsumiki. A
reminder description of Ippei. A reminder description of Wako. A reminder
description of Kennis.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis
finish their trial.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis leave
Col on the route to Zei.

# Three Hundred And Nine

This chapter is from Alex's point of view. Zei is 927.07 km away from the
start of the novel. It is located in Gowders and it is a mountain. A
detailed description of Zei during late morning.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis start
to train together.

# Three Hundred And Ten

This chapter is from Alex's point of view. A reminder description of Zei
during early evening. Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki,
Wako, and Kennis are already present. A reminder description of Alex. A
reminder description of Ryūki. A reminder description of Fukumika. A
reminder description of Tamamiko. A reminder description of Rumiko. A
reminder description of Itsumiki. A reminder description of Wako. A
reminder description of Kennis.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis
finish their training.

Alex, Ryūki, Fukumika, Tamamiko, Rumiko, Itsumiki, Wako, and Kennis leave
Zei on the route to Col.

# Three Hundred And Eleven

This chapter is from Alex's point of view. A medium description of Col
during early evening. Hideji, Etsumiko, and Ippei are already present. A
reminder description of Hideji. A reminder description of Etsumiko. A
reminder description of Ippei.

Alex, Ryūki, Fukumika, Hideji, Tamamiko, Rumiko, Itsumiki, Wako, and
Kennis begin to fight.  Alex fights along with Ryūki, Fukumika, Tamamiko,
Rumiko, Itsumiki, Wako, and Kennis.  Hideji fights alone.

Tamamiko, Etsumiko, Rumiko, and Itsumiki begin to fight.  Tamamiko fights
along with Rumiko and Itsumiki.  Etsumiko fights alone.

Etsumiko died.

Tamamiko, Rumiko, and Itsumiki finish fighting.

Ippei joins Itsumiki.

Itsumiki died.

Motoyohisa died.

Hideji died.

Alex, Ryūki, Fukumika, Wako, and Kennis finish fighting.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Col on the route to Leh.

# Three Hundred And Twelve

This chapter is from Alex's point of view. Leh is 842.06 km away from the
start of the novel. It is located in Cring and it is a city. A detailed
description of Leh during morning.

Alex, Ryūki, Fukumika, Wako, and Kennis begin some time together.

# Three Hundred And Thirteen

This chapter is from Alex's point of view. A reminder description of Leh
during noon. Alex, Ryūki, Fukumika, Wako, and Kennis are already present.
A reminder description of Alex. A reminder description of Ryūki. A
reminder description of Fukumika. A reminder description of Wako. A
reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their time together.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Leh on the route to
Arnutte.

# Three Hundred And Fourteen

This chapter is from Alex's point of view. Arnutte is 1,117.57 km away
from the start of the novel. It is located in Gowders and it is a city. A
detailed description of Arnutte during evening.

# Three Hundred And Fifteen

This chapter is from Alex's point of view. A reminder description of
Arnutte during very early morning. Alex, Ryūki, Fukumika, Wako, and
Kennis are already present. A reminder description of Alex. A reminder
description of Ryūki. A reminder description of Fukumika. A reminder
description of Wako. A reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Arnutte on the route to
Leh.

# Three Hundred And Sixteen

This chapter is from Alex's point of view. A medium description of Leh
during very early morning.

Alex, Ryūki, Fukumika, Wako, and Kennis start to train together.

# Three Hundred And Seventeen

This chapter is from Alex's point of view. A reminder description of Leh
during late morning. Alex, Ryūki, Fukumika, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of Ryūki.
A reminder description of Fukumika. A reminder description of Wako. A
reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their training.

Alex, Ryūki, Fukumika, Wako, and Kennis begin a trial.

# Three Hundred And Eighteen

This chapter is from Alex's point of view. A reminder description of Leh
during near midnight. Alex, Ryūki, Fukumika, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of Ryūki.
A reminder description of Fukumika. A reminder description of Wako. A
reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their trial.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Leh on the route to Stauta.

# Three Hundred And Nineteen

This chapter is from Alex's point of view. Stauta is 1,049.20 km away
from the start of the novel. It is located in Gowders and it is a tundra.
A detailed description of Stauta during early evening. Sta Jone is
already present. A detailed description of Sta. They are 17 years old.
They were born at Ziebelottl in Chet.

Alex, Ryūki, Fukumika, Sta, Wako, and Kennis begin to fight.  Alex fights
along with Ryūki, Fukumika, Wako, and Kennis.  Sta fights alone.

Sta died.

Alex, Ryūki, Fukumika, Wako, and Kennis finish fighting.

Alex, Ryūki, Fukumika, Wako, and Kennis begin some time together.

# Three Hundred And Twenty

This chapter is from Alex's point of view. A reminder description of
Stauta during very early morning. Alex, Ryūki, Fukumika, Wako, and Kennis
are already present. A reminder description of Alex. A reminder
description of Ryūki. A reminder description of Fukumika. A reminder
description of Wako. A reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their time together.

Alex, Ryūki, Fukumika, Wako, and Kennis start to train together.

# Three Hundred And Twenty One

This chapter is from Alex's point of view. A reminder description of
Stauta during early morning. Alex, Ryūki, Fukumika, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Ryūki. A reminder description of Fukumika. A reminder description of
Wako. A reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their training.

Alex, Ryūki, Fukumika, Wako, and Kennis begin a trial.

# Three Hundred And Twenty Two

This chapter is from Alex's point of view. A reminder description of
Stauta during morning. Alex, Ryūki, Fukumika, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Ryūki. A reminder description of Fukumika. A reminder description of
Wako. A reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their trial.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Stauta on the route to
Dyes.

# Three Hundred And Twenty Three

This chapter is from Alex's point of view. Dyes is 1,262.01 km away from
the start of the novel. It is located in Chet and it is a town. A
detailed description of Dyes during very early morning. Patricia Wall is
already present. A detailed description of Patricia. They are 18 years
old. They were born at Gets in Chet.

Alex, Ryūki, Fukumika, Wako, and Kennis begin some time together.

# Three Hundred And Twenty Four

This chapter is from Alex's point of view. A reminder description of Dyes
during early morning. Alex, Ryūki, Fukumika, Patricia, Wako, and Kennis
are already present. A reminder description of Alex. A reminder
description of Ryūki. A reminder description of Fukumika. A reminder
description of Patricia. A reminder description of Wako. A reminder
description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their time together.

Alex, Ryūki, Fukumika, Wako, and Kennis begin a trial.

# Three Hundred And Twenty Five

This chapter is from Alex's point of view. A reminder description of Dyes
during morning. Alex, Ryūki, Fukumika, Patricia, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Ryūki. A reminder description of Fukumika. A reminder description of
Patricia. A reminder description of Wako. A reminder description of
Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their trial.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Dyes on the route to Wals.

# Three Hundred And Twenty Six

This chapter is from Alex's point of view. Wals is 1,358.61 km away from
the start of the novel. It is located in Chet and it is a farm. A
detailed description of Wals during very early morning.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Wals on the route to
Desideca.

# Three Hundred And Twenty Seven

This chapter is from Alex's point of view. Desideca is 1,472.59 km away
from the start of the novel. It is located in Yokes and it is a river. A
detailed description of Desideca during very early morning.

Alex, Ryūki, Fukumika, Wako, and Kennis start to train together.

# Three Hundred And Twenty Eight

This chapter is from Alex's point of view. A reminder description of
Desideca during noon. Alex, Ryūki, Fukumika, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of Ryūki.
A reminder description of Fukumika. A reminder description of Wako. A
reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their training.

Alex, Ryūki, Fukumika, Wako, and Kennis begin a trial.

# Three Hundred And Twenty Nine

This chapter is from Alex's point of view. A reminder description of
Desideca during afternoon. Alex, Ryūki, Fukumika, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Ryūki. A reminder description of Fukumika. A reminder description of
Wako. A reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their trial.

Alex, Ryūki, Fukumika, Wako, and Kennis begin some time together.

# Three Hundred And Thirty

This chapter is from Alex's point of view. A reminder description of
Desideca during evening. Alex, Ryūki, Fukumika, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Ryūki. A reminder description of Fukumika. A reminder description of
Wako. A reminder description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their time together.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Desideca on the route to
Villberkl.

# Three Hundred And Thirty One

This chapter is from Alex's point of view. Villberkl is 1,597.54 km away
from the start of the novel. It is located in Yokes and it is a swamp. A
detailed description of Villberkl during late evening.

Alex, Ryūki, Fukumika, Wako, and Kennis leave Villberkl on the route to
Avorker.

# Three Hundred And Thirty Two

This chapter is from Alex's point of view. Avorker is 1,718.69 km away
from the start of the novel. It is located in Rizzian and it is a lake. A
detailed description of Avorker during late evening. Don Pater, Dia
Prueger, and Virgie Payton are already present. A detailed description of
Don. They are 17 years old. They were born at Alson in Ber. A detailed
description of Dia. They are 18 years old. They were born at Boev in
Rizzian. A detailed description of Virgie. They are 18 years old. They
were born at Shabe in Rizzian.

Alex, Ryūki, Fukumika, Wako, and Kennis begin a trial.

# Three Hundred And Thirty Three

This chapter is from Alex's point of view. A reminder description of
Avorker during midnight. Alex, Ryūki, Fukumika, Don, Dia, Virgie, Wako,
and Kennis are already present. A reminder description of Alex. A
reminder description of Ryūki. A reminder description of Fukumika. A
reminder description of Don. A reminder description of Dia. A reminder
description of Virgie. A reminder description of Wako. A reminder
description of Kennis.

Alex, Ryūki, Fukumika, Wako, and Kennis finish their trial.

Alex, Ryūki, Fukumika, Don, Wako, and Kennis begin to fight.  Alex fights
along with Ryūki, Fukumika, Wako, and Kennis.  Don fights alone.

Fukumika died.

Ryūki died.

Don died.

Alex, Wako, and Kennis finish fighting.

Alex, Wako, and Kennis begin some time together.

# Three Hundred And Thirty Four

This chapter is from Alex's point of view. A reminder description of
Avorker during early afternoon. Alex, Dia, Virgie, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Dia. A reminder description of Virgie. A reminder description of Wako.
A reminder description of Kennis.

Alex, Wako, and Kennis finish their time together.

Virgie joins Alex.

Alex, Virgie, Wako, and Kennis start to train together.

# Three Hundred And Thirty Five

This chapter is from Alex's point of view. A reminder description of
Avorker during early evening. Alex, Dia, Virgie, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Dia. A reminder description of Virgie. A reminder description of Wako.
A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their training.

Alex, Dia, Virgie, Wako, and Kennis begin to fight.  Alex fights along
with Virgie, Wako, and Kennis.  Dia fights alone.

Dia died.

Alex, Virgie, Wako, and Kennis finish fighting.

Alex, Virgie, Wako, and Kennis leave Avorker on the route to Desideca.

# Three Hundred And Thirty Six

This chapter is from Alex's point of view. A medium description of
Desideca during early evening.

Alex, Virgie, Wako, and Kennis leave Desideca on the route to Dyes.

# Three Hundred And Thirty Seven

This chapter is from Alex's point of view. A medium description of Dyes
during early evening. Patricia is already present. A reminder description
of Patricia.

Alex, Virgie, Wako, and Kennis start to train together.

# Three Hundred And Thirty Eight

This chapter is from Alex's point of view. A reminder description of Dyes
during late evening. Alex, Patricia, Virgie, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Patricia. A reminder description of Virgie. A reminder description of
Wako. A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their training.

Alex, Virgie, Wako, and Kennis leave Dyes on the route to Sturzi.

# Three Hundred And Thirty Nine

This chapter is from Alex's point of view. Sturzi is 1,417.36 km away
from the start of the novel. It is located in Yokes and it is a town. A
detailed description of Sturzi during early morning.

Alex, Virgie, Wako, and Kennis begin a trial.

# Three Hundred And Forty

This chapter is from Alex's point of view. A reminder description of
Sturzi during afternoon. Alex, Virgie, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Virgie. A reminder description of Wako. A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their trial.

Alex, Virgie, Wako, and Kennis start to train together.

# Three Hundred And Forty One

This chapter is from Alex's point of view. A reminder description of
Sturzi during early morning. Alex, Virgie, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Virgie. A reminder description of Wako. A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their training.

Alex, Virgie, Wako, and Kennis begin some time together.

# Three Hundred And Forty Two

This chapter is from Alex's point of view. A reminder description of
Sturzi during late morning. Alex, Virgie, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Virgie. A reminder description of Wako. A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their time together.

Alex, Virgie, Wako, and Kennis leave Sturzi on the route to Aveld.

# Three Hundred And Forty Three

This chapter is from Alex's point of view. Aveld is 1,528.62 km away from
the start of the novel. It is located in Yokes and it is a tundra. A
detailed description of Aveld during morning.

Alex, Virgie, Wako, and Kennis begin some time together.

# Three Hundred And Forty Four

This chapter is from Alex's point of view. A reminder description of
Aveld during late evening. Alex, Virgie, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Virgie. A reminder description of Wako. A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their time together.

Alex, Virgie, Wako, and Kennis start to train together.

# Three Hundred And Forty Five

This chapter is from Alex's point of view. A reminder description of
Aveld during very early morning. Alex, Virgie, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Virgie. A reminder description of Wako. A reminder description of
Kennis.

Alex, Virgie, Wako, and Kennis finish their training.

Alex, Virgie, Wako, and Kennis leave Aveld on the route to Mogler.

# Three Hundred And Forty Six

This chapter is from Alex's point of view. Mogler is 1,627.83 km away
from the start of the novel. It is located in Yokes and it is a town. A
detailed description of Mogler during late evening.

Alex, Virgie, Wako, and Kennis begin a trial.

# Three Hundred And Forty Seven

This chapter is from Alex's point of view. A reminder description of
Mogler during very early morning. Alex, Virgie, Wako, and Kennis are
already present. A reminder description of Alex. A reminder description
of Virgie. A reminder description of Wako. A reminder description of
Kennis.

Alex, Virgie, Wako, and Kennis finish their trial.

Alex, Virgie, Wako, and Kennis leave Mogler on the route to Simichoeke.

# Three Hundred And Forty Eight

This chapter is from Alex's point of view. Simichoeke is 1,834.47 km away
from the start of the novel. It is located in Rizzian and it is a forest.
A detailed description of Simichoeke during evening.

Alex, Virgie, Wako, and Kennis begin some time together.

# Three Hundred And Forty Nine

This chapter is from Alex's point of view. A reminder description of
Simichoeke during morning. Alex, Virgie, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Virgie. A reminder description of Wako. A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their time together.

Alex, Virgie, Wako, and Kennis leave Simichoeke on the route to Mogler.

# Three Hundred And Fifty

This chapter is from Alex's point of view. A medium description of Mogler
during morning.

Alex, Virgie, Wako, and Kennis start to train together.

# Three Hundred And Fifty One

This chapter is from Alex's point of view. A reminder description of
Mogler during evening. Alex, Virgie, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Virgie. A reminder description of Wako. A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their training.

Alex, Virgie, Wako, and Kennis leave Mogler on the route to Aveld.

# Three Hundred And Fifty Two

This chapter is from Alex's point of view. A medium description of Aveld
during late evening.

Alex, Virgie, Wako, and Kennis begin a trial.

# Three Hundred And Fifty Three

This chapter is from Alex's point of view. A reminder description of
Aveld during late evening. Alex, Virgie, Wako, and Kennis are already
present. A reminder description of Alex. A reminder description of
Virgie. A reminder description of Wako. A reminder description of Kennis.

Alex, Virgie, Wako, and Kennis finish their trial.

Alex, Virgie, Wako, and Kennis leave Aveld on the route to Dyes.

# Three Hundred And Fifty Four

This chapter is from Alex's point of view. A medium description of Dyes
during early morning. Patricia is already present. A reminder description
of Patricia.

Alex, Patricia, Virgie, Wako, and Kennis begin to fight.  Alex fights
along with Virgie, Wako, and Kennis.  Patricia fights alone.

Virgie died.

Wako died.

Patricia died.

Alex and Kennis finish fighting.

Alex and Kennis leave Dyes on the route to Liewine.

# Three Hundred And Fifty Five

This chapter is from Alex's point of view. Liewine is 1,303.80 km away
from the start of the novel. It is located in Chet and it is a swamp. A
detailed description of Liewine during early afternoon.

Alex and Kennis begin a trial.

# Three Hundred And Fifty Six

This chapter is from Alex's point of view. A reminder description of
Liewine during evening. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis begin some time together.

# Three Hundred And Fifty Seven

This chapter is from Alex's point of view. A reminder description of
Liewine during very early morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis start to train together.

# Three Hundred And Fifty Eight

This chapter is from Alex's point of view. A reminder description of
Liewine during late morning. Alex and Kennis are already present. A
reminder description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Liewine on the route to Waka.

# Three Hundred And Fifty Nine

This chapter is from Alex's point of view. Waka is 1,586.71 km away from
the start of the novel. It is located in Yokes and it is a forest. A
detailed description of Waka during evening. Wandi Grayman is already
present. A detailed description of Wandi. They are 18 years old. They
were born at Herry in Rizzian.

Alex and Kennis begin a trial.

# Three Hundred And Sixty

This chapter is from Alex's point of view. A reminder description of Waka
during late evening. Alex, Wandi, and Kennis are already present. A
reminder description of Alex. A reminder description of Wandi. A reminder
description of Kennis.

Alex and Kennis finish their trial.

Alex and Kennis leave Waka on the route to Fine.

# Three Hundred And Sixty One

This chapter is from Alex's point of view. Fine is 1,698.37 km away from
the start of the novel. It is located in Rizzian and it is a city. A
detailed description of Fine during evening.

Alex and Kennis begin some time together.

# Three Hundred And Sixty Two

This chapter is from Alex's point of view. A reminder description of Fine
during early morning. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their time together.

Alex and Kennis start to train together.

# Three Hundred And Sixty Three

This chapter is from Alex's point of view. A reminder description of Fine
during afternoon. Alex and Kennis are already present. A reminder
description of Alex. A reminder description of Kennis.

Alex and Kennis finish their training.

Alex and Kennis leave Fine on the route to Waka.

# Three Hundred And Sixty Four

This chapter is from Alex's point of view. A medium description of Waka
during afternoon. Wandi is already present. A reminder description of
Wandi.

Wandi joins Alex.

Alex, Wandi, and Kennis begin some time together.

# Three Hundred And Sixty Five

This chapter is from Alex's point of view. A reminder description of Waka
during evening. Alex, Wandi, and Kennis are already present. A reminder
description of Alex. A reminder description of Wandi. A reminder
description of Kennis.

Alex, Wandi, and Kennis finish their time together.

Alex, Wandi, and Kennis leave Waka on the route to Col.

# Three Hundred And Sixty Six

This chapter is from Alex's point of view. A medium description of Col
during midnight. Tamamiko, Rumiko, and Ippei are already present. A
reminder description of Tamamiko. A reminder description of Rumiko. A
reminder description of Ippei.

Alex, Wandi, and Kennis leave Col on the route to Esberg.

# Three Hundred And Sixty Seven

This chapter is from Alex's point of view. Esberg is 912.08 km away from
the start of the novel. It is located in Gowders and it is a town. A
detailed description of Esberg during early morning. Kurō Sasao, Thomas
Dar, Atsuo Suzuoka, and Rieko Fujikawa are already present. A detailed
description of Kurō. They are 17 years old. They were born at Murner in
Gowders. A detailed description of Thomas. They are 19 years old. They
were born at Petrippy in Chet. A detailed description of Atsuo. They are
19 years old. They were born at Wensmer in Gowders. A detailed
description of Rieko. They are 18 years old. They were born at Koli in
Gowders.

Alex, Wandi, and Kennis leave Esberg on the route to Reins.

# Three Hundred And Sixty Eight

This chapter is from Robert's point of view. A medium description of Gard
during afternoon. Robert, Kanori, Shishōhei, Itsue, Jōichi, Yōji,
Zenkichi, Hisa, Chinae, Etsumi, Yuri, Mane, Sanae, Suke, Miyaka,
Tadanari, Yūsuke, Sako, Omi, Manako, Renai, Nodoka, Genjirō, Tatsugi,
Otoha, Atsunao, Hidemi, Otome, Yoritaka, Nanae, Umeko, Hisakiko,
Narunosuki, Rumi, Yayo, and Keichi are already present. A reminder
description of Robert. A reminder description of Kanori. A reminder
description of Shishōhei. A reminder description of Itsue. A reminder
description of Jōichi. A reminder description of Yōji. A reminder
description of Zenkichi. A reminder description of Hisa. A reminder
description of Chinae. A reminder description of Etsumi. A reminder
description of Yuri. A reminder description of Mane. A reminder
description of Sanae. A reminder description of Suke. A reminder
description of Miyaka. A reminder description of Tadanari. A reminder
description of Yūsuke. A reminder description of Sako. A reminder
description of Omi. A reminder description of Manako. A reminder
description of Renai. A reminder description of Nodoka. A reminder
description of Genjirō. A reminder description of Tatsugi. A reminder
description of Otoha. A reminder description of Atsunao. A reminder
description of Hidemi. A reminder description of Otome. A reminder
description of Yoritaka. A reminder description of Nanae. A reminder
description of Umeko. A reminder description of Hisakiko. A reminder
description of Narunosuki. A reminder description of Rumi. A reminder
description of Yayo. A reminder description of Keichi.

Yōji died.

Jōichi died.

# Three Hundred And Sixty Nine

This chapter is from Alex's point of view. Reins is 1,131.75 km away from
the start of the novel. It is located in Gowders and it is a farm. A
detailed description of Reins during very early morning.

Alex, Wandi, and Kennis begin a trial.

# Three Hundred And Seventy

This chapter is from Alex's point of view. A reminder description of
Reins during early morning. Alex, Wandi, and Kennis are already present.
A reminder description of Alex. A reminder description of Wandi. A
reminder description of Kennis.

Alex, Wandi, and Kennis finish their trial.

Alex, Wandi, and Kennis leave Reins on the route to Esberg.

# Three Hundred And Seventy One

This chapter is from Alex's point of view. A medium description of Esberg
during early morning. Kurō, Thomas, Atsuo, and Rieko are already present.
A reminder description of Kurō. A reminder description of Thomas. A
reminder description of Atsuo. A reminder description of Rieko.

Alex, Wandi, and Kennis begin some time together.

# Three Hundred And Seventy Two

This chapter is from Alex's point of view. A reminder description of
Esberg during early afternoon. Alex, Kurō, Thomas, Wandi, Atsuo, Rieko,
and Kennis are already present. A reminder description of Alex. A
reminder description of Kurō. A reminder description of Thomas. A
reminder description of Wandi. A reminder description of Atsuo. A
reminder description of Rieko. A reminder description of Kennis.

Alex, Wandi, and Kennis finish their time together.

Alex, Wandi, and Kennis start to train together.

# Three Hundred And Seventy Three

This chapter is from Alex's point of view. A reminder description of
Esberg during afternoon. Alex, Kurō, Thomas, Wandi, Atsuo, Rieko, and
Kennis are already present. A reminder description of Alex. A reminder
description of Kurō. A reminder description of Thomas. A reminder
description of Wandi. A reminder description of Atsuo. A reminder
description of Rieko. A reminder description of Kennis.

Alex, Wandi, and Kennis finish their training.

Rieko joins Alex and Wandi.

Alex, Wandi, Rieko, and Kennis begin a trial.

# Three Hundred And Seventy Four

This chapter is from Alex's point of view. A reminder description of
Esberg during midnight. Alex, Kurō, Thomas, Wandi, Atsuo, Rieko, and
Kennis are already present. A reminder description of Alex. A reminder
description of Kurō. A reminder description of Thomas. A reminder
description of Wandi. A reminder description of Atsuo. A reminder
description of Rieko. A reminder description of Kennis.

Alex, Wandi, Rieko, and Kennis finish their trial.

Alex, Kurō, Wandi, Rieko, and Kennis begin to fight.  Alex fights along
with Wandi, Rieko, and Kennis.  Kurō fights alone.

Kurō and Atsuo begin to fight.  Kurō fights alone.  Atsuo fights alone.

Atsuo died.

Kurō finish fighting.

Kurō died.

Alex, Wandi, Rieko, and Kennis finish fighting.

Alex, Thomas, Wandi, Rieko, and Kennis begin to fight.  Alex fights along
with Wandi, Rieko, and Kennis.  Thomas fights alone.

Thomas died.

Alex, Wandi, Rieko, and Kennis finish fighting.

Alex, Wandi, Rieko, and Kennis leave Esberg on the route to Sunsolds.

# Three Hundred And Seventy Five

This chapter is from Alex's point of view. A medium description of
Sunsolds during evening. Fumi, Chiemikue, Motokiko, Miho, and Yahideo are
already present. A reminder description of Fumi. A reminder description
of Chiemikue. A reminder description of Motokiko. A reminder description
of Miho. A reminder description of Yahideo.

Alex, Miho, Wandi, Rieko, and Kennis begin to fight.  Alex fights along
with Wandi, Rieko, and Kennis.  Miho fights alone.

Wandi died.

Miho died.

Alex and Kennis finish fighting.

Fumi joins Alex, Penee, Nena, and Olly.

Chiemikue joins Alex, Olly, Fumi, and Fred.

Alex, Fumi, Chiemikue, and Kennis leave Sunsolds on the route to Zell.

# Three Hundred And Seventy Six

This chapter is from Alex's point of view. Zell is 695.27 km away from
the start of the novel. It is located in Ele and it is a lake. A detailed
description of Zell during very early morning. Tadatoshi Nomoto, Sawai
Tani, Minatsuka Ushi, and Hanase Ogiharu are already present. A detailed
description of Tadatoshi. They are 16 years old. They were born at Reut
in Gowders. A detailed description of Sawai. They are 16 years old. They
were born at Krotton in Gowders. A detailed description of Minatsuka.
They are 20 years old. They were born at Geieris in Cring. A detailed
description of Hanase. They are 16 years old. They were born at Hitski in
Ele.

Alex, Fumi, Chiemikue, and Kennis start to train together.

# Three Hundred And Seventy Seven

This chapter is from Alex's point of view. A reminder description of Zell
during morning. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka,
Hanase, and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Chiemikue. A
reminder description of Tadatoshi. A reminder description of Sawai. A
reminder description of Minatsuka. A reminder description of Hanase. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, and Kennis finish their training.

Alex, Fumi, Chiemikue, and Kennis begin some time together.

# Three Hundred And Seventy Eight

This chapter is from Alex's point of view. A reminder description of Zell
during evening. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka,
Hanase, and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Chiemikue. A
reminder description of Tadatoshi. A reminder description of Sawai. A
reminder description of Minatsuka. A reminder description of Hanase. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, and Kennis finish their time together.

Tadatoshi joins Alex, Fumi, Chiemikue, Ryūki, and Fukumika.

Alex, Fumi, Chiemikue, Tadatoshi, and Kennis begin a trial.

# Three Hundred And Seventy Nine

This chapter is from Alex's point of view. A reminder description of Zell
during morning. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka,
Hanase, and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Chiemikue. A
reminder description of Tadatoshi. A reminder description of Sawai. A
reminder description of Minatsuka. A reminder description of Hanase. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, and Kennis finish their trial.

Minatsuka joins Alex, Fumi, Chiemikue, Tadatoshi, and Virgie.

Sawai joins Alex, Fumi, Chiemikue, Ryūki, Fukumika, Tadatoshi, and
William.

Hanase joins Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, and
Wandi.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Hanase, and Kennis
leave Zell on the route to Lue.

# Three Hundred And Eighty

This chapter is from Alex's point of view. Lue is 979.58 km away from the
start of the novel. It is located in Gowders and it is a farm. A detailed
description of Lue during afternoon. Nami Oki is already present. A
detailed description of Nami. They are 16 years old. They were born at
Gilrahm in Gowders.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Hanase, and Kennis
begin some time together.

# Three Hundred And Eighty One

This chapter is from Alex's point of view. A reminder description of Lue
during evening. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami,
Hanase, and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Chiemikue. A
reminder description of Tadatoshi. A reminder description of Sawai. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Hanase, and Kennis
finish their time together.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Hanase, and Kennis
begin a trial.

# Three Hundred And Eighty Two

This chapter is from Alex's point of view. A reminder description of Lue
during early morning. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka,
Nami, Hanase, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Sawai. A reminder description of Minatsuka. A reminder description of
Nami. A reminder description of Hanase. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Hanase, and Kennis
finish their trial.

Nami joins Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, and Wandi.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis start to train together.

# Three Hundred And Eighty Three

This chapter is from Alex's point of view. A reminder description of Lue
during late morning. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka,
Nami, Hanase, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Sawai. A reminder description of Minatsuka. A reminder description of
Nami. A reminder description of Hanase. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis leave Lue on the route to Madamer.

# Three Hundred And Eighty Four

This chapter is from Alex's point of view. Madamer is 1,127.18 km away
from the start of the novel. It is located in Gowders and it is a tundra.
A detailed description of Madamer during early evening.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis begin some time together.

# Three Hundred And Eighty Five

This chapter is from Alex's point of view. A reminder description of
Madamer during midnight. Alex, Fumi, Chiemikue, Tadatoshi, Sawai,
Minatsuka, Nami, Hanase, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Sawai. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis finish their time together.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis start to train together.

# Three Hundred And Eighty Six

This chapter is from Alex's point of view. A reminder description of
Madamer during noon. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka,
Nami, Hanase, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Sawai. A reminder description of Minatsuka. A reminder description of
Nami. A reminder description of Hanase. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis begin a trial.

# Three Hundred And Eighty Seven

This chapter is from Alex's point of view. A reminder description of
Madamer during evening. Alex, Fumi, Chiemikue, Tadatoshi, Sawai,
Minatsuka, Nami, Hanase, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Sawai. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis finish their trial.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis leave Madamer on the route to Sunsolds.

# Three Hundred And Eighty Eight

This chapter is from Alex's point of view. A medium description of
Sunsolds during very early morning. Motokiko, Yahideo, and Rieko are
already present. A reminder description of Motokiko. A reminder
description of Yahideo. A reminder description of Rieko.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, and
Kennis leave Sunsolds on the route to Bray.

# Three Hundred And Eighty Nine

This chapter is from Alex's point of view. Bray is 693.59 km away from
the start of the novel. It is located in Ele and it is a lake. A detailed
description of Bray during morning. Daiki Inuganuma and Yuka Ōka are
already present. A detailed description of Daiki. They are 19 years old.
They were born at Rhey in Gowders. A detailed description of Yuka. They
are 19 years old. They were born at Discher in Gowders.

Yuka joins Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, and
Hanase.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis begin a trial.

# Three Hundred And Ninety

This chapter is from Alex's point of view. A reminder description of Bray
during early afternoon. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Daiki,
Minatsuka, Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Sawai. A reminder description of Daiki. A reminder
description of Minatsuka. A reminder description of Nami. A reminder
description of Hanase. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish their trial.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Daiki, Minatsuka, Nami, Hanase,
Yuka, and Kennis begin to fight.  Alex fights along with Fumi, Chiemikue,
Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka, and Kennis.  Daiki
fights alone.

Daiki died.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish fighting.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis begin some time together.

# Three Hundred And Ninety One

This chapter is from Alex's point of view. A reminder description of Bray
during very early morning. Alex, Fumi, Chiemikue, Tadatoshi, Sawai,
Minatsuka, Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Sawai. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish their time together.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis start to train together.

# Three Hundred And Ninety Two

This chapter is from Alex's point of view. A reminder description of Bray
during early morning. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka,
Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Sawai. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Bray on the route to Kuesnelli.

# Three Hundred And Ninety Three

This chapter is from Alex's point of view. Kuesnelli is 933.08 km away
from the start of the novel. It is located in Gowders and it is a forest.
A detailed description of Kuesnelli during early morning.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Kuesnelli on the route to Mager.

# Three Hundred And Ninety Four

This chapter is from Alex's point of view. Mager is 1,004.01 km away from
the start of the novel. It is located in Gowders and it is a city. A
detailed description of Mager during late evening.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis start to train together.

# Three Hundred And Ninety Five

This chapter is from Alex's point of view. A reminder description of
Mager during morning. Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka,
Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Sawai. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Mager on the route to Kuesnelli.

# Three Hundred And Ninety Six

This chapter is from Alex's point of view. A medium description of
Kuesnelli during morning.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Kuesnelli on the route to Shedlinko.

# Three Hundred And Ninety Seven

This chapter is from Alex's point of view. Shedlinko is 1,107.79 km away
from the start of the novel. It is located in Gowders and it is a farm. A
detailed description of Shedlinko during evening.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis start to train together.

# Three Hundred And Ninety Eight

This chapter is from Alex's point of view. A reminder description of
Shedlinko during near midnight. Alex, Fumi, Chiemikue, Tadatoshi, Sawai,
Minatsuka, Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Sawai. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Shedlinko on the route to Sunsolds.

# Three Hundred And Ninety Nine

This chapter is from Alex's point of view. A medium description of
Sunsolds during early morning. Motokiko, Yahideo, and Rieko are already
present. A reminder description of Motokiko. A reminder description of
Yahideo. A reminder description of Rieko.

Alex, Fumi, Chiemikue, Motokiko, Tadatoshi, Sawai, Minatsuka, Nami,
Hanase, Yuka, and Kennis begin to fight.  Alex fights along with Fumi,
Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka, and Kennis.
Motokiko fights alone.

Motokiko died.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish fighting.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Yahideo, Minatsuka, Nami,
Hanase, Yuka, and Kennis begin to fight.  Alex fights along with Fumi,
Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka, and Kennis.
Yahideo fights alone.

Yahideo died.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish fighting.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Sunsolds on the route to Bel.

# Four Hundred

This chapter is from Alex's point of view. Bel is 711.62 km away from the
start of the novel. It is located in Ele and it is a town. A detailed
description of Bel during early afternoon. Miwakari Takagi is already
present. A detailed description of Miwakari. They are 16 years old. They
were born at Scup in Gowders.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Bel on the route to Monzan.

# Four Hundred And One

This chapter is from Alex's point of view. Monzan is 1,007.13 km away
from the start of the novel. It is located in Gowders and it is a tundra.
A detailed description of Monzan during very early morning.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis start to train together.

# Four Hundred And Two

This chapter is from Alex's point of view. A reminder description of
Monzan during late morning. Alex, Fumi, Chiemikue, Tadatoshi, Sawai,
Minatsuka, Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Sawai. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Monzan on the route to Rembartel.

# Four Hundred And Three

This chapter is from Alex's point of view. Rembartel is 1,108.09 km away
from the start of the novel. It is located in Gowders and it is a farm. A
detailed description of Rembartel during morning. Nelia Canney and Wilma
Pittler are already present. A detailed description of Nelia. They are 16
years old. They were born at Bushwiese in Chet. A detailed description of
Wilma. They are 20 years old. They were born at Amacguider in Chet.

Alex, Fumi, Chiemikue, Tadatoshi, Sawai, Nelia, Minatsuka, Nami, Hanase,
Yuka, and Kennis begin to fight.  Alex fights along with Fumi, Chiemikue,
Tadatoshi, Sawai, Minatsuka, Nami, Hanase, Yuka, and Kennis.  Nelia
fights alone.

Sawai died.

Wilma joins Nelia.

Nelia died.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish fighting.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis begin some time together.

# Four Hundred And Four

This chapter is from Alex's point of view. A reminder description of
Rembartel during early afternoon. Alex, Fumi, Chiemikue, Tadatoshi,
Minatsuka, Nami, Hanase, Wilma, Yuka, and Kennis are already present. A
reminder description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Minatsuka. A reminder description of Nami. A reminder
description of Hanase. A reminder description of Wilma. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish their time together.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis begin a trial.

# Four Hundred And Five

This chapter is from Alex's point of view. A reminder description of
Rembartel during late evening. Alex, Fumi, Chiemikue, Tadatoshi,
Minatsuka, Nami, Hanase, Wilma, Yuka, and Kennis are already present. A
reminder description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Minatsuka. A reminder description of Nami. A reminder
description of Hanase. A reminder description of Wilma. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish their trial.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis start to train together.

# Four Hundred And Six

This chapter is from Alex's point of view. A reminder description of
Rembartel during morning. Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka,
Nami, Hanase, Wilma, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Minatsuka. A reminder description of Nami. A reminder
description of Hanase. A reminder description of Wilma. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis leave Rembartel on the route to Bel.

# Four Hundred And Seven

This chapter is from Alex's point of view. A medium description of Bel
during morning. Miwakari is already present. A reminder description of
Miwakari.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis start to train together.

# Four Hundred And Eight

This chapter is from Alex's point of view. A reminder description of Bel
during late evening. Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami,
Miwakari, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Minatsuka. A reminder description of Nami. A reminder
description of Miwakari. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Miwakari, Hanase,
Yuka, and Kennis begin to fight.  Alex fights along with Fumi, Chiemikue,
Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and Kennis.  Miwakari fights
alone.

Miwakari died.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish fighting.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis begin some time together.

# Four Hundred And Nine

This chapter is from Alex's point of view. A reminder description of Bel
during early morning. Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami,
Hanase, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Minatsuka. A reminder description of Nami. A reminder description of
Hanase. A reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish their time together.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis begin a trial.

# Four Hundred And Ten

This chapter is from Alex's point of view. A reminder description of Bel
during late morning. Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami,
Hanase, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Minatsuka. A reminder description of Nami. A reminder description of
Hanase. A reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish their trial.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis leave Bel on the route to Aut.

# Four Hundred And Eleven

This chapter is from Alex's point of view. Aut is 841.29 km away from the
start of the novel. It is located in Cring and it is a tundra. A detailed
description of Aut during noon. Chōbyō Banemari is already present. A
detailed description of Chōbyō. They are 16 years old. They were born at
Satchke in Gowders.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis begin some time together.

# Four Hundred And Twelve

This chapter is from Alex's point of view. A reminder description of Aut
during early evening. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Minatsuka, Nami, Hanase, Yuka, and
Kennis finish their time together.

Chōbyō joins Alex, Fumi, Chiemikue, Ryūki, Fukumika, Tadatoshi, and
Sawai.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase, Yuka,
and Kennis begin a trial.

# Four Hundred And Thirteen

This chapter is from Alex's point of view. A reminder description of Aut
during early morning. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish their trial.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase, Yuka,
and Kennis start to train together.

# Four Hundred And Fourteen

This chapter is from Alex's point of view. A reminder description of Aut
during early afternoon. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase, Yuka,
and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase, Yuka,
and Kennis leave Aut on the route to Buhl.

# Four Hundred And Fifteen

This chapter is from Alex's point of view. Buhl is 990.00 km away from
the start of the novel. It is located in Gowders and it is a lake. A
detailed description of Buhl during evening. Iemasamori Fuku is already
present. A detailed description of Iemasamori. They are 17 years old.
They were born at Valuo in Gowders.

Iemasamori joins Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Wandi, Nami, and Hanase.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Buhl on the route to Ling.

# Four Hundred And Sixteen

This chapter is from Alex's point of view. Ling is 1,290.87 km away from
the start of the novel. It is located in Chet and it is a town. A
detailed description of Ling during morning.

# Four Hundred And Seventeen

This chapter is from Alex's point of view. A reminder description of Ling
during evening. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Nami, Hanase, Iemasamori, Yuka, and Kennis are already present. A
reminder description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Ling on the route to Buhl.

# Four Hundred And Eighteen

This chapter is from Alex's point of view. A medium description of Buhl
during evening.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Nineteen

This chapter is from Alex's point of view. A reminder description of Buhl
during early morning. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Chiemikue. A reminder description of Tadatoshi.
A reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Nami. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis begin some time together.

# Four Hundred And Twenty

This chapter is from Alex's point of view. A reminder description of Buhl
during noon. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their time together.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Twenty One

This chapter is from Alex's point of view. A reminder description of Buhl
during late evening. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Nami, Hanase, Iemasamori, Yuka, and Kennis are already present. A
reminder description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Buhl on the route to Kapparrino.

# Four Hundred And Twenty Two

This chapter is from Alex's point of view. Kapparrino is 1,053.33 km away
from the start of the novel. It is located in Gowders and it is a river.
A detailed description of Kapparrino during late morning.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Kapparrino on the route to Essel.

# Four Hundred And Twenty Three

This chapter is from Alex's point of view. Essel is 1,167.05 km away from
the start of the novel. It is located in Chet and it is a mountain. A
detailed description of Essel during late morning.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Essel on the route to Itsudd.

# Four Hundred And Twenty Four

This chapter is from Alex's point of view. Itsudd is 1,266.97 km away
from the start of the novel. It is located in Chet and it is a tundra. A
detailed description of Itsudd during early morning.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Twenty Five

This chapter is from Alex's point of view. A reminder description of
Itsudd during late morning. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Chiemikue. A reminder description of Tadatoshi.
A reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Nami. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Twenty Six

This chapter is from Alex's point of view. A reminder description of
Itsudd during midnight. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Chiemikue. A reminder description of Tadatoshi.
A reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Nami. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Itsudd on the route to Phau.

# Four Hundred And Twenty Seven

This chapter is from Alex's point of view. Phau is 1,477.82 km away from
the start of the novel. It is located in Yokes and it is a tundra. A
detailed description of Phau during early evening.

# Four Hundred And Twenty Eight

This chapter is from Alex's point of view. A reminder description of Phau
during noon. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Phau on the route to Itsudd.

# Four Hundred And Twenty Nine

This chapter is from Alex's point of view. A medium description of Itsudd
during noon.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis begin some time together.

# Four Hundred And Thirty

This chapter is from Alex's point of view. A reminder description of
Itsudd during evening. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Chiemikue. A reminder description of Tadatoshi.
A reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Nami. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their time together.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Itsudd on the route to Azevin.

# Four Hundred And Thirty One

This chapter is from Alex's point of view. Azevin is 1,514.01 km away
from the start of the novel. It is located in Yokes and it is a town. A
detailed description of Azevin during late evening.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Azevin on the route to Deshime.

# Four Hundred And Thirty Two

This chapter is from Alex's point of view. Deshime is 1,809.39 km away
from the start of the novel. It is located in Rizzian and it is a city. A
detailed description of Deshime during morning.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Deshime on the route to Routhewski.

# Four Hundred And Thirty Three

This chapter is from Alex's point of view. Routhewski is 1,929.48 km away
from the start of the novel. It is located in Rizzian and it is a river.
A detailed description of Routhewski during morning.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Routhewski on the route to Volz.

# Four Hundred And Thirty Four

This chapter is from Alex's point of view. Volz is 2,176.18 km away from
the start of the novel. It is located in Ber and it is a river. A
detailed description of Volz during late morning.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Thirty Five

This chapter is from Alex's point of view. A reminder description of Volz
during afternoon. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Nami, Hanase, Iemasamori, Yuka, and Kennis are already present. A
reminder description of Alex. A reminder description of Fumi. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Volz on the route to Essel.

# Four Hundred And Thirty Six

This chapter is from Alex's point of view. A medium description of Essel
during late morning.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Thirty Seven

This chapter is from Alex's point of view. A reminder description of
Essel during early afternoon. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Chiemikue. A reminder description of Tadatoshi.
A reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Nami. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis begin some time together.

# Four Hundred And Thirty Eight

This chapter is from Alex's point of view. A reminder description of
Essel during midnight. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Chiemikue. A reminder description of Tadatoshi.
A reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Nami. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their time together.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Thirty Nine

This chapter is from Alex's point of view. A reminder description of
Essel during late morning. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Chiemikue. A reminder description of Tadatoshi.
A reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Nami. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Essel on the route to Sunsolds.

# Four Hundred And Forty

This chapter is from Alex's point of view. A medium description of
Sunsolds during early afternoon. Rieko is already present. A reminder
description of Rieko.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Sunsolds on the route to Duffinson.

# Four Hundred And Forty One

This chapter is from Alex's point of view. A medium description of
Duffinson during early afternoon. Otoharuko, Toshi, and Riko are already
present. A reminder description of Otoharuko. A reminder description of
Toshi. A reminder description of Riko.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Duffinson on the route to Teway.

# Four Hundred And Forty Two

This chapter is from Alex's point of view. Teway is 333.03 km away from
the start of the novel. It is located in Quellie and it is a city. A
detailed description of Teway during early evening.

# Four Hundred And Forty Three

This chapter is from Alex's point of view. A reminder description of
Teway during late evening. Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Chiemikue. A reminder description of Tadatoshi.
A reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Nami. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami, Hanase,
Iemasamori, Yuka, and Kennis leave Teway on the route to Duffinson.

# Four Hundred And Forty Four

This chapter is from Alex's point of view. A medium description of
Duffinson during late evening. Otoharuko, Toshi, and Riko are already
present. A reminder description of Otoharuko. A reminder description of
Toshi. A reminder description of Riko.

Otoharuko joins Alex, Penee, Olly, Fumi, Vin, and Gakuma.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin some time together.

# Four Hundred And Forty Five

This chapter is from Alex's point of view. A reminder description of
Duffinson during early morning. Alex, Fumi, Otoharuko, Chiemikue, Toshi,
Tadatoshi, Riko, Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and
Kennis are already present. A reminder description of Alex. A reminder
description of Fumi. A reminder description of Otoharuko. A reminder
description of Chiemikue. A reminder description of Toshi. A reminder
description of Tadatoshi. A reminder description of Riko. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Nami. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their time together.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Duffinson on the route to Wey.

# Four Hundred And Forty Six

This chapter is from Alex's point of view. Wey is 543.07 km away from the
start of the novel. It is located in Ritt and it is a tundra. A detailed
description of Wey during early morning. Zenzo Uchiya is already present.
A detailed description of Zenzo. They are 17 years old. They were born at
Cromayr in Ele.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin some time together.

# Four Hundred And Forty Seven

This chapter is from Alex's point of view. A reminder description of Wey
during early afternoon. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Zenzo, Yuka, and Kennis are
already present. A reminder description of Alex. A reminder description
of Fumi. A reminder description of Otoharuko. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Chōbyō. A reminder description of Minatsuka. A reminder description of
Nami. A reminder description of Hanase. A reminder description of
Iemasamori. A reminder description of Zenzo. A reminder description of
Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their time together.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Forty Eight

This chapter is from Alex's point of view. A reminder description of Wey
during near midnight. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Zenzo, Yuka, and Kennis are
already present. A reminder description of Alex. A reminder description
of Fumi. A reminder description of Otoharuko. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Chōbyō. A reminder description of Minatsuka. A reminder description of
Nami. A reminder description of Hanase. A reminder description of
Iemasamori. A reminder description of Zenzo. A reminder description of
Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Zenzo, Yuka, and Kennis begin to fight.  Alex fights
along with Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Nami, Hanase, Iemasamori, Yuka, and Kennis.  Zenzo fights alone.

Zenzo died.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish fighting.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Forty Nine

This chapter is from Alex's point of view. A reminder description of Wey
during very early morning. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Wey on the route to Belli.

# Four Hundred And Fifty

This chapter is from Alex's point of view. Belli is 654.60 km away from
the start of the novel. It is located in Ele and it is a river. A
detailed description of Belli during very early morning.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Fifty One

This chapter is from Alex's point of view. A reminder description of
Belli during late morning. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Belli on the route to Gaway.

# Four Hundred And Fifty Two

This chapter is from Alex's point of view. Gaway is 709.91 km away from
the start of the novel. It is located in Ele and it is a mountain. A
detailed description of Gaway during late evening.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Fifty Three

This chapter is from Alex's point of view. A reminder description of
Gaway during morning. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Gaway on the route to Brin.

# Four Hundred And Fifty Four

This chapter is from Alex's point of view. Brin is 763.37 km away from
the start of the novel. It is located in Cring and it is a farm. A
detailed description of Brin during early evening.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Brin on the route to Belli.

# Four Hundred And Fifty Five

This chapter is from Alex's point of view. A medium description of Belli
during early morning.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Fifty Six

This chapter is from Alex's point of view. A reminder description of
Belli during afternoon. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin some time together.

# Four Hundred And Fifty Seven

This chapter is from Alex's point of view. A reminder description of
Belli during early morning. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their time together.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Belli on the route to Dion.

# Four Hundred And Fifty Eight

This chapter is from Alex's point of view. Dion is 674.31 km away from
the start of the novel. It is located in Ele and it is a river. A
detailed description of Dion during morning.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin some time together.

# Four Hundred And Fifty Nine

This chapter is from Alex's point of view. A reminder description of Dion
during late morning. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their time together.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Sixty

This chapter is from Alex's point of view. A reminder description of Dion
during evening. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Sixty One

This chapter is from Alex's point of view. A reminder description of Dion
during early morning. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Dion on the route to Asivall.

# Four Hundred And Sixty Two

This chapter is from Alex's point of view. Asivall is 868.28 km away from
the start of the novel. It is located in Cring and it is a lake. A
detailed description of Asivall during early evening.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Asivall on the route to Belli.

# Four Hundred And Sixty Three

This chapter is from Alex's point of view. A medium description of Belli
during late evening.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Belli on the route to Pent.

# Four Hundred And Sixty Four

This chapter is from Alex's point of view. Pent is 865.78 km away from
the start of the novel. It is located in Cring and it is a tundra. A
detailed description of Pent during early evening.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis start to train together.

# Four Hundred And Sixty Five

This chapter is from Alex's point of view. A reminder description of Pent
during late evening. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Sixty Six

This chapter is from Alex's point of view. A reminder description of Pent
during very early morning. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis begin some time together.

# Four Hundred And Sixty Seven

This chapter is from Alex's point of view. A reminder description of Pent
during early afternoon. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Nami, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Tadatoshi. A reminder description of Chōbyō. A
reminder description of Minatsuka. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis finish their time together.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka, Nami,
Hanase, Iemasamori, Yuka, and Kennis leave Pent on the route to Juard.

# Four Hundred And Sixty Eight

This chapter is from Alex's point of view. Juard is 948.23 km away from
the start of the novel. It is located in Gowders and it is a river. A
detailed description of Juard during early morning. Hisamichi Maruno is
already present. A detailed description of Hisamichi. They are 19 years
old. They were born at Seter in Gowders.

Hisamichi joins Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō,
Virgie, and Minatsuka.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis begin some time
together.

# Four Hundred And Sixty Nine

This chapter is from Alex's point of view. A reminder description of
Juard during early evening. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis
are already present. A reminder description of Alex. A reminder
description of Fumi. A reminder description of Otoharuko. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Nami. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis finish their time
together.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis start to train
together.

# Four Hundred And Seventy

This chapter is from Alex's point of view. A reminder description of
Juard during very early morning. Alex, Fumi, Otoharuko, Chiemikue,
Tadatoshi, Chōbyō, Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka,
and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Otoharuko. A
reminder description of Chiemikue. A reminder description of Tadatoshi. A
reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Hisamichi. A reminder description of Nami. A
reminder description of Hanase. A reminder description of Iemasamori. A
reminder description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis finish their
training.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Seventy One

This chapter is from Alex's point of view. A reminder description of
Juard during morning. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi,
Chōbyō, Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis
are already present. A reminder description of Alex. A reminder
description of Fumi. A reminder description of Otoharuko. A reminder
description of Chiemikue. A reminder description of Tadatoshi. A reminder
description of Chōbyō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Nami. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis leave Juard on the
route to Aile.

# Four Hundred And Seventy Two

This chapter is from Alex's point of view. Aile is 1,204.76 km away from
the start of the novel. It is located in Chet and it is a city. A
detailed description of Aile during late morning.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis start to train
together.

# Four Hundred And Seventy Three

This chapter is from Alex's point of view. A reminder description of Aile
during noon. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis are
already present. A reminder description of Alex. A reminder description
of Fumi. A reminder description of Otoharuko. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Chōbyō. A reminder description of Minatsuka. A reminder description of
Hisamichi. A reminder description of Nami. A reminder description of
Hanase. A reminder description of Iemasamori. A reminder description of
Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis finish their
training.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis begin a trial.

# Four Hundred And Seventy Four

This chapter is from Alex's point of view. A reminder description of Aile
during afternoon. Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō,
Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis are
already present. A reminder description of Alex. A reminder description
of Fumi. A reminder description of Otoharuko. A reminder description of
Chiemikue. A reminder description of Tadatoshi. A reminder description of
Chōbyō. A reminder description of Minatsuka. A reminder description of
Hisamichi. A reminder description of Nami. A reminder description of
Hanase. A reminder description of Iemasamori. A reminder description of
Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Tadatoshi, Chōbyō, Minatsuka,
Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis leave Aile on the
route to Duffinson.

# Four Hundred And Seventy Five

This chapter is from Alex's point of view. A medium description of
Duffinson during early evening. Toshi and Riko are already present. A
reminder description of Toshi. A reminder description of Riko.

Toshi joins Alex, Fumi, Otoharuko, Chiemikue, Ryūki, Paulie, Fukumika,
and Natsuko.

Riko joins Alex, Fumi, Otoharuko, Chiemikue, Ryūki, Fukumika, Toshi,
Tadatoshi, William, and Sawai.

Alex, Fumi, Otoharuko, Chiemikue, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis leave
Duffinson on the route to Gard.

# Four Hundred And Seventy Six

This chapter is from Alex's point of view. A medium description of Gard
during evening. Robert, Kanori, Shishōhei, Itsue, Zenkichi, Hisa, Chinae,
Etsumi, Yuri, Mane, Sanae, Suke, Miyaka, Tadanari, Yūsuke, Sako, Omi,
Manako, Renai, Nodoka, Genjirō, Tatsugi, Otoha, Atsunao, Hidemi, Otome,
Yoritaka, Nanae, Umeko, Hisakiko, Narunosuki, Rumi, Yayo, and Keichi are
already present. A reminder description of Robert. A reminder description
of Kanori. A reminder description of Shishōhei. A reminder description of
Itsue. A reminder description of Zenkichi. A reminder description of
Hisa. A reminder description of Chinae. A reminder description of Etsumi.
A reminder description of Yuri. A reminder description of Mane. A
reminder description of Sanae. A reminder description of Suke. A reminder
description of Miyaka. A reminder description of Tadanari. A reminder
description of Yūsuke. A reminder description of Sako. A reminder
description of Omi. A reminder description of Manako. A reminder
description of Renai. A reminder description of Nodoka. A reminder
description of Genjirō. A reminder description of Tatsugi. A reminder
description of Otoha. A reminder description of Atsunao. A reminder
description of Hidemi. A reminder description of Otome. A reminder
description of Yoritaka. A reminder description of Nanae. A reminder
description of Umeko. A reminder description of Hisakiko. A reminder
description of Narunosuki. A reminder description of Rumi. A reminder
description of Yayo. A reminder description of Keichi.

Renai joins Alex, Fumi, Otoharuko, Chiemikue, Ryūki, Paulie, and
Fukumika.

Alex, Fumi, Otoharuko, Chiemikue, Renai, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis leave
Gard on the route to Rasbert.

# Four Hundred And Seventy Seven

This chapter is from Alex's point of view. Rasbert is 691.07 km away from
the start of the novel. It is located in Ele and it is a swamp. A
detailed description of Rasbert during near midnight. Amao Uehishii,
Chiko Aoma, Otomoji Sanemoto, Yūseizō Koguchino, and Yumiri Kanashima are
already present. A detailed description of Amao. They are 17 years old.
They were born at Tedt in Cring. A detailed description of Chiko. They
are 18 years old. They were born at Borbeso in Ele. A detailed
description of Otomoji. They are 20 years old. They were born at Barty in
Cring. A detailed description of Yūseizō. They are 16 years old. They
were born at Ty in Cring. A detailed description of Yumiri. They are 18
years old. They were born at Suck in Gowders.

Alex, Fumi, Otoharuko, Chiemikue, Renai, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis begin a
trial.

# Four Hundred And Seventy Eight

This chapter is from Alex's point of view. A reminder description of
Rasbert during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Renai, Toshi, Tadatoshi, Riko, Chōbyō, Otomoji, Minatsuka, Hisamichi,
Yūseizō, Nami, Yumiri, Hanase, Iemasamori, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Amao. A reminder description of Chiko. A
reminder description of Renai. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Otomoji. A
reminder description of Minatsuka. A reminder description of Hisamichi. A
reminder description of Yūseizō. A reminder description of Nami. A
reminder description of Yumiri. A reminder description of Hanase. A
reminder description of Iemasamori. A reminder description of Yuka. A
reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Renai, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis finish
their trial.

Alex, Fumi, Otoharuko, Chiemikue, Renai, Toshi, Tadatoshi, Riko, Chōbyō,
Otomoji, Minatsuka, Hisamichi, Nami, Hanase, Iemasamori, Yuka, and Kennis
begin to fight.  Alex fights along with Fumi, Otoharuko, Chiemikue,
Renai, Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Nami,
Hanase, Iemasamori, Yuka, and Kennis.  Otomoji fights alone.

Nami died.

Renai leaves Rasbert on the route to Cout.

# Four Hundred And Seventy Nine

This chapter is from Renai's point of view. Cout is 870.07 km away from
the start of the novel. It is located in Cring and it is a swamp. A
detailed description of Cout during evening.

# Four Hundred And Eighty

This chapter is from Renai's point of view. A reminder description of
Cout during morning. Renai is already present. A reminder description of
Renai.

Renai leaves Cout on the route to Rasbert.

# Four Hundred And Eighty One

This chapter is from Alex's point of view. A medium description of
Rasbert during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Otomoji, Minatsuka, Hisamichi, Yūseizō,
Yumiri, Hanase, Iemasamori, Yuka, and Kennis are already present. A
reminder description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Otomoji. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Yūseizō. A reminder
description of Yumiri. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Renai died.

Otomoji died.

Alex, Fumi, Otoharuko, Chiemikue, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Hanase, Iemasamori, Yuka, and Kennis finish
fighting.

Yumiri joins Alex, Fumi, Otoharuko, Chiemikue, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Wandi, and Nami.

Alex, Fumi, Otoharuko, Chiemikue, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and Kennis start
to train together.

# Four Hundred And Eighty Two

This chapter is from Alex's point of view. A reminder description of
Rasbert during evening. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yūseizō, Yumiri,
Hanase, Iemasamori, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yūseizō. A reminder description of Yumiri. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and Kennis finish
their training.

Alex, Fumi, Otoharuko, Chiemikue, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Yūseizō, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis begin to fight.  Alex fights along with Fumi, Otoharuko,
Chiemikue, Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri,
Hanase, Iemasamori, Yuka, and Kennis.  Yūseizō fights alone.

Yūseizō died.

Alex, Fumi, Otoharuko, Chiemikue, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and Kennis finish
fighting.

Amao joins Alex, Olly, Fumi, Otoharuko, Fred, and Chiemikue.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and Kennis begin
some time together.

# Four Hundred And Eighty Three

This chapter is from Alex's point of view. A reminder description of
Rasbert during very early morning. Alex, Fumi, Otoharuko, Chiemikue,
Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi,
Yumiri, Hanase, Iemasamori, Yuka, and Kennis are already present. A
reminder description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yumiri. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and Kennis finish
their time together.

Chiko joins Alex, Olly, Fumi, Otoharuko, Chiemikue, and Amao.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis leave Rasbert on the route to Los.

# Four Hundred And Eighty Four

This chapter is from Alex's point of view. Los is 778.46 km away from the
start of the novel. It is located in Cring and it is a mountain. A
detailed description of Los during evening. Ariaki Nagata is already
present. A detailed description of Ariaki. They are 17 years old. They
were born at Locian in Gowders.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis begin a trial.

# Four Hundred And Eighty Five

This chapter is from Alex's point of view. A reminder description of Los
during early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yumiri. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis start to train together.

# Four Hundred And Eighty Six

This chapter is from Alex's point of view. A reminder description of Los
during late morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yumiri. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis begin some time together.

# Four Hundred And Eighty Seven

This chapter is from Alex's point of view. A reminder description of Los
during early afternoon. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yumiri. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis finish their time together.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis leave Los on the route to Myer.

# Four Hundred And Eighty Eight

This chapter is from Alex's point of view. Myer is 828.89 km away from
the start of the novel. It is located in Cring and it is a river. A
detailed description of Myer during near midnight. Shi Enobu and Naohirō
Hozumoto are already present. A detailed description of Shi. They are 18
years old. They were born at Lefsey in Gowders. A detailed description of
Naohirō. They are 16 years old. They were born at Flant in Gowders.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Naohirō, Yuka,
and Kennis begin to fight.  Alex fights along with Fumi, Otoharuko,
Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka,
Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and Kennis.  Naohirō fights
alone.

Naohirō died.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis finish fighting.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis start to train together.

# Four Hundred And Eighty Nine

This chapter is from Alex's point of view. A reminder description of Myer
during early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi,
Hanase, Iemasamori, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis begin some time together.

# Four Hundred And Ninety

This chapter is from Alex's point of view. A reminder description of Myer
during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase,
Iemasamori, Yuka, and Kennis are already present. A reminder description
of Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Minatsuka. A
reminder description of Hisamichi. A reminder description of Yumiri. A
reminder description of Shi. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Hanase, Iemasamori, Yuka, and
Kennis finish their time together.

Shi joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Wandi, Nami, and Yumiri.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis leave Myer on the route to Los.

# Four Hundred And Ninety One

This chapter is from Alex's point of view. A medium description of Los
during morning. Ariaki is already present. A reminder description of
Ariaki.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis leave Los on the route to Krito.

# Four Hundred And Ninety Two

This chapter is from Alex's point of view. Krito is 1,000.09 km away from
the start of the novel. It is located in Gowders and it is a tundra. A
detailed description of Krito during early morning.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis leave Krito on the route to Ston.

# Four Hundred And Ninety Three

This chapter is from Alex's point of view. Ston is 1,018.17 km away from
the start of the novel. It is located in Gowders and it is a mountain. A
detailed description of Ston during morning.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis begin a trial.

# Four Hundred And Ninety Four

This chapter is from Alex's point of view. A reminder description of Ston
during late evening. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi,
Hanase, Iemasamori, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis start to train together.

# Four Hundred And Ninety Five

This chapter is from Alex's point of view. A reminder description of Ston
during early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi,
Hanase, Iemasamori, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis leave Ston on the route to Costeelas.

# Four Hundred And Ninety Six

This chapter is from Alex's point of view. Costeelas is 1,027.26 km away
from the start of the novel. It is located in Gowders and it is a farm. A
detailed description of Costeelas during morning.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis begin a trial.

# Four Hundred And Ninety Seven

This chapter is from Alex's point of view. A reminder description of
Costeelas during early evening. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi,
Hanase, Iemasamori, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Yuka, and
Kennis leave Costeelas on the route to Los.

# Four Hundred And Ninety Eight

This chapter is from Alex's point of view. A medium description of Los
during morning. Ariaki is already present. A reminder description of
Ariaki.

Ariaki joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, and
Iemasamori.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Ariaki,
Yuka, and Kennis leave Los on the route to Bridge.

# Four Hundred And Ninety Nine

This chapter is from Alex's point of view. Bridge is 981.28 km away from
the start of the novel. It is located in Gowders and it is a mountain. A
detailed description of Bridge during very early morning. Rank Blaker,
Christina Baught, Kiyohirō Ōhata, and Beve Cross are already present. A
detailed description of Rank. They are 16 years old. They were born at
Infels in Chet. A detailed description of Christina. They are 18 years
old. They were born at Bumba in Chet. A detailed description of Kiyohirō.
They are 17 years old. They were born at Wilcote in Gowders. A detailed
description of Beve. They are 16 years old. They were born at Schloski in
Chet.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Rank, Toshi, Tadatoshi,
Riko, Chōbyō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis begin to fight.  Alex fights along with Fumi,
Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō,
Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori, Ariaki, Yuka, and
Kennis.  Rank fights alone.

Rank died.

Kiyohirō joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Renai,
Toshi, Tadatoshi, Sawai, Riko, and Chōbyō.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis finish fighting.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis leave Bridge on the route to Lockey.

# Five Hundred

This chapter is from Alex's point of view. Lockey is 1,042.13 km away
from the start of the novel. It is located in Gowders and it is a forest.
A detailed description of Lockey during early afternoon.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis start to train together.

# Five Hundred And One

This chapter is from Alex's point of view. A reminder description of
Lockey during very early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Kiyohirō, Minatsuka, Hisamichi,
Yumiri, Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Amao. A reminder description of Chiko. A
reminder description of Toshi. A reminder description of Tadatoshi. A
reminder description of Riko. A reminder description of Chōbyō. A
reminder description of Kiyohirō. A reminder description of Minatsuka. A
reminder description of Hisamichi. A reminder description of Yumiri. A
reminder description of Shi. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis begin some time together.

# Five Hundred And Two

This chapter is from Alex's point of view. A reminder description of
Lockey during early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Kiyohirō, Minatsuka, Hisamichi,
Yumiri, Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Amao. A reminder description of Chiko. A
reminder description of Toshi. A reminder description of Tadatoshi. A
reminder description of Riko. A reminder description of Chōbyō. A
reminder description of Kiyohirō. A reminder description of Minatsuka. A
reminder description of Hisamichi. A reminder description of Yumiri. A
reminder description of Shi. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis finish their time together.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis begin a trial.

# Five Hundred And Three

This chapter is from Alex's point of view. A reminder description of
Lockey during late morning. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Kiyohirō, Minatsuka, Hisamichi,
Yumiri, Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis are already
present. A reminder description of Alex. A reminder description of Fumi.
A reminder description of Otoharuko. A reminder description of Chiemikue.
A reminder description of Amao. A reminder description of Chiko. A
reminder description of Toshi. A reminder description of Tadatoshi. A
reminder description of Riko. A reminder description of Chōbyō. A
reminder description of Kiyohirō. A reminder description of Minatsuka. A
reminder description of Hisamichi. A reminder description of Yumiri. A
reminder description of Shi. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis leave Lockey on the route to Bridge.

# Five Hundred And Four

This chapter is from Alex's point of view. A medium description of Bridge
during late morning. Christina and Beve are already present. A reminder
description of Christina. A reminder description of Beve.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis start to train together.

# Five Hundred And Five

This chapter is from Alex's point of view. A reminder description of
Bridge during evening. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Kiyohirō, Minatsuka,
Hisamichi, Yumiri, Shi, Beve, Hanase, Iemasamori, Ariaki, Yuka, and
Kennis are already present. A reminder description of Alex. A reminder
description of Fumi. A reminder description of Otoharuko. A reminder
description of Chiemikue. A reminder description of Amao. A reminder
description of Chiko. A reminder description of Toshi. A reminder
description of Tadatoshi. A reminder description of Riko. A reminder
description of Chōbyō. A reminder description of Christina. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Yumiri. A reminder
description of Shi. A reminder description of Beve. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Ariaki. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase, Iemasamori,
Ariaki, Yuka, and Kennis finish their training.

Christina joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Ryūki,
Fukumika, Renai, Toshi, Tadatoshi, Sawai, Riko, and Chōbyō.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis begin some time together.

# Five Hundred And Six

This chapter is from Alex's point of view. A reminder description of
Bridge during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Kiyohirō, Minatsuka,
Hisamichi, Yumiri, Shi, Beve, Hanase, Iemasamori, Ariaki, Yuka, and
Kennis are already present. A reminder description of Alex. A reminder
description of Fumi. A reminder description of Otoharuko. A reminder
description of Chiemikue. A reminder description of Amao. A reminder
description of Chiko. A reminder description of Toshi. A reminder
description of Tadatoshi. A reminder description of Riko. A reminder
description of Chōbyō. A reminder description of Christina. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Yumiri. A reminder
description of Shi. A reminder description of Beve. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Ariaki. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis finish their time together.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis begin a trial.

# Five Hundred And Seven

This chapter is from Alex's point of view. A reminder description of
Bridge during afternoon. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Kiyohirō, Minatsuka,
Hisamichi, Yumiri, Shi, Beve, Hanase, Iemasamori, Ariaki, Yuka, and
Kennis are already present. A reminder description of Alex. A reminder
description of Fumi. A reminder description of Otoharuko. A reminder
description of Chiemikue. A reminder description of Amao. A reminder
description of Chiko. A reminder description of Toshi. A reminder
description of Tadatoshi. A reminder description of Riko. A reminder
description of Chōbyō. A reminder description of Christina. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Yumiri. A reminder
description of Shi. A reminder description of Beve. A reminder
description of Hanase. A reminder description of Iemasamori. A reminder
description of Ariaki. A reminder description of Yuka. A reminder
description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis finish their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Beve,
Hanase, Iemasamori, Ariaki, Yuka, and Kennis begin to fight.  Alex fights
along with Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi,
Riko, Chōbyō, Christina, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi,
Hanase, Iemasamori, Ariaki, Yuka, and Kennis.  Beve fights alone.

Beve died.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis finish fighting.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi, Hanase,
Iemasamori, Ariaki, Yuka, and Kennis leave Bridge on the route to Sogna.

# Five Hundred And Eight

This chapter is from Alex's point of view. Sogna is 1,162.77 km away from
the start of the novel. It is located in Chet and it is a lake. A
detailed description of Sogna during early morning. Cher Ales is already
present. A detailed description of Cher. They are 20 years old. They were
born at Jopler in Chet.

Cher joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Ryūki,
Fukumika, Renai, Toshi, Tadatoshi, Sawai, Riko, Chōbyō, and Christina.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Yumiri, Shi,
Hanase, Iemasamori, Ariaki, Yuka, and Kennis leave Sogna on the route to
Morff.

# Five Hundred And Nine

This chapter is from Alex's point of view. Morff is 1,348.51 km away from
the start of the novel. It is located in Chet and it is a tundra. A
detailed description of Morff during early evening. Chris Sear is already
present. A detailed description of Chris. They are 16 years old. They
were born at Geon in Yokes.

Chris joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Christina, Cher, Kiyohirō, Virgie, Minatsuka,
and Hisamichi.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis start to train
together.

# Five Hundred And Ten

This chapter is from Alex's point of view. A reminder description of
Morff during early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Iemasamori, Ariaki,
Yuka, and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Otoharuko. A
reminder description of Chiemikue. A reminder description of Amao. A
reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Kiyohirō. A
reminder description of Minatsuka. A reminder description of Hisamichi. A
reminder description of Chris. A reminder description of Yumiri. A
reminder description of Shi. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis finish their training.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis leave Morff on the
route to Sogna.

# Five Hundred And Eleven

This chapter is from Alex's point of view. A medium description of Sogna
during early morning.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis begin some time
together.

# Five Hundred And Twelve

This chapter is from Alex's point of view. A reminder description of
Sogna during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Kiyohirō, Minatsuka,
Hisamichi, Chris, Yumiri, Shi, Hanase, Iemasamori, Ariaki, Yuka, and
Kennis are already present. A reminder description of Alex. A reminder
description of Fumi. A reminder description of Otoharuko. A reminder
description of Chiemikue. A reminder description of Amao. A reminder
description of Chiko. A reminder description of Toshi. A reminder
description of Tadatoshi. A reminder description of Riko. A reminder
description of Chōbyō. A reminder description of Christina. A reminder
description of Cher. A reminder description of Kiyohirō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Chris. A reminder description of Yumiri. A reminder
description of Shi. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis finish their time
together.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis leave Sogna on the
route to Brwolden.

# Five Hundred And Thirteen

This chapter is from Alex's point of view. Brwolden is 1,229.97 km away
from the start of the novel. It is located in Chet and it is a farm. A
detailed description of Brwolden during near midnight. Anie Willey, Jane
Port, and Dane McC are already present. A detailed description of Anie.
They are 20 years old. They were born at Mill in Yokes. A detailed
description of Jane. They are 19 years old. They were born at Bone in
Chet. A detailed description of Dane. They are 18 years old. They were
born at Krido in Chet.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Dane, Yuka, and Kennis begin to fight.
Alex fights along with Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi,
Chris, Yumiri, Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis.  Dane
fights alone.

Dane died.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis finish fighting.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis leave Brwolden on the
route to Leuve.

# Five Hundred And Fourteen

This chapter is from Alex's point of view. Leuve is 1,248.18 km away from
the start of the novel. It is located in Chet and it is a city. A
detailed description of Leuve during very early morning.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis leave Leuve on the
route to Harastille.

# Five Hundred And Fifteen

This chapter is from Alex's point of view. Harastille is 1,406.47 km away
from the start of the novel. It is located in Yokes and it is a river. A
detailed description of Harastille during late morning.

# Five Hundred And Sixteen

This chapter is from Alex's point of view. A reminder description of
Harastille during evening. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Kiyohirō, Minatsuka,
Hisamichi, Chris, Yumiri, Shi, Hanase, Iemasamori, Ariaki, Yuka, and
Kennis are already present. A reminder description of Alex. A reminder
description of Fumi. A reminder description of Otoharuko. A reminder
description of Chiemikue. A reminder description of Amao. A reminder
description of Chiko. A reminder description of Toshi. A reminder
description of Tadatoshi. A reminder description of Riko. A reminder
description of Chōbyō. A reminder description of Christina. A reminder
description of Cher. A reminder description of Kiyohirō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Chris. A reminder description of Yumiri. A reminder
description of Shi. A reminder description of Hanase. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri,
Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis leave Harastille on the
route to Brwolden.

# Five Hundred And Seventeen

This chapter is from Alex's point of view. A medium description of
Brwolden during evening. Anie and Jane are already present. A reminder
description of Anie. A reminder description of Jane.

Anie joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Renai, Toshi,
Tadatoshi, Sawai, Riko, Chōbyō, Christina, Cher, and Motoyohisa.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Iemasamori, Ariaki, Yuka, and Kennis leave Brwolden
on the route to Hagelm.

# Five Hundred And Eighteen

This chapter is from Alex's point of view. Hagelm is 1,458.37 km away
from the start of the novel. It is located in Yokes and it is a swamp. A
detailed description of Hagelm during early evening. Lino Ranks is
already present. A detailed description of Lino. They are 17 years old.
They were born at Nard in Yokes.

Lino joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka,
Hisamichi, Chris, Wandi, Yumiri, Shi, and Hanase.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis begin a
trial.

# Five Hundred And Nineteen

This chapter is from Alex's point of view. A reminder description of
Hagelm during very early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis begin
some time together.

# Five Hundred And Twenty

This chapter is from Alex's point of view. A reminder description of
Hagelm during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their time together.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis leave
Hagelm on the route to Osdy.

# Five Hundred And Twenty One

This chapter is from Alex's point of view. Osdy is 1,602.63 km away from
the start of the novel. It is located in Yokes and it is a tundra. A
detailed description of Osdy during early afternoon.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis leave
Osdy on the route to Brwolden.

# Five Hundred And Twenty Two

This chapter is from Alex's point of view. A medium description of
Brwolden during noon. Jane is already present. A reminder description of
Jane.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Jane, Lino, Iemasamori, Ariaki, Yuka, and Kennis
begin to fight.  Alex fights along with Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis.  Jane fights alone.

Jane died.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
fighting.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis begin
some time together.

# Five Hundred And Twenty Three

This chapter is from Alex's point of view. A reminder description of
Brwolden during near midnight. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their time together.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis begin a
trial.

# Five Hundred And Twenty Four

This chapter is from Alex's point of view. A reminder description of
Brwolden during early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis start to
train together.

# Five Hundred And Twenty Five

This chapter is from Alex's point of view. A reminder description of
Brwolden during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their training.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis leave
Brwolden on the route to Bella.

# Five Hundred And Twenty Six

This chapter is from Alex's point of view. Bella is 1,303.46 km away from
the start of the novel. It is located in Chet and it is a mountain. A
detailed description of Bella during near midnight.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis begin a
trial.

# Five Hundred And Twenty Seven

This chapter is from Alex's point of view. A reminder description of
Bella during late morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis leave
Bella on the route to Brwolden.

# Five Hundred And Twenty Eight

This chapter is from Alex's point of view. A medium description of
Brwolden during late morning.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis leave
Brwolden on the route to Wer.

# Five Hundred And Twenty Nine

This chapter is from Alex's point of view. Wer is 1,370.73 km away from
the start of the novel. It is located in Chet and it is a mountain. A
detailed description of Wer during afternoon. Susandice Reide is already
present. A detailed description of Susandice. They are 19 years old. They
were born at Humebaetz in Yokes.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis begin
some time together.

# Five Hundred And Thirty

This chapter is from Alex's point of view. A reminder description of Wer
during very early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Susandice, Ariaki, Yuka, and Kennis are already present. A reminder
description of Alex. A reminder description of Fumi. A reminder
description of Otoharuko. A reminder description of Chiemikue. A reminder
description of Amao. A reminder description of Chiko. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Riko. A reminder description of Chōbyō. A reminder
description of Christina. A reminder description of Cher. A reminder
description of Anie. A reminder description of Kiyohirō. A reminder
description of Minatsuka. A reminder description of Hisamichi. A reminder
description of Chris. A reminder description of Yumiri. A reminder
description of Shi. A reminder description of Hanase. A reminder
description of Lino. A reminder description of Iemasamori. A reminder
description of Susandice. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their time together.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Susandice, Ariaki, Yuka, and
Kennis begin to fight.  Alex fights along with Fumi, Otoharuko,
Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher,
Anie, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino,
Iemasamori, Ariaki, Yuka, and Kennis.  Susandice fights alone.

Susandice died.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
fighting.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis start to
train together.

# Five Hundred And Thirty One

This chapter is from Alex's point of view. A reminder description of Wer
during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka,
Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka,
and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Otoharuko. A
reminder description of Chiemikue. A reminder description of Amao. A
reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their training.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis begin a
trial.

# Five Hundred And Thirty Two

This chapter is from Alex's point of view. A reminder description of Wer
during morning. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka,
Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka,
and Kennis are already present. A reminder description of Alex. A
reminder description of Fumi. A reminder description of Otoharuko. A
reminder description of Chiemikue. A reminder description of Amao. A
reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis leave Wer
on the route to Sogna.

# Five Hundred And Thirty Three

This chapter is from Alex's point of view. A medium description of Sogna
during late evening.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis begin a
trial.

# Five Hundred And Thirty Four

This chapter is from Alex's point of view. A reminder description of
Sogna during early morning. Alex, Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their trial.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis start to
train together.

# Five Hundred And Thirty Five

This chapter is from Alex's point of view. A reminder description of
Sogna during afternoon. Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis are already present. A reminder description of
Alex. A reminder description of Fumi. A reminder description of
Otoharuko. A reminder description of Chiemikue. A reminder description of
Amao. A reminder description of Chiko. A reminder description of Toshi. A
reminder description of Tadatoshi. A reminder description of Riko. A
reminder description of Chōbyō. A reminder description of Christina. A
reminder description of Cher. A reminder description of Anie. A reminder
description of Kiyohirō. A reminder description of Minatsuka. A reminder
description of Hisamichi. A reminder description of Chris. A reminder
description of Yumiri. A reminder description of Shi. A reminder
description of Hanase. A reminder description of Lino. A reminder
description of Iemasamori. A reminder description of Ariaki. A reminder
description of Yuka. A reminder description of Kennis.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
their training.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis leave
Sogna on the route to Lolan.

# Five Hundred And Thirty Six

This chapter is from Alex's point of view. Lolan is 1,391.36 km away from
the start of the novel. It is located in Chet and it is a tundra. A
detailed description of Lolan during early afternoon. Deboray Gibson and
Melia Brillen are already present. A detailed description of Deboray.
They are 19 years old. They were born at Bahansen in Yokes. A detailed
description of Melia. They are 16 years old. They were born at Hydzinis
in Yokes.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, Melia, and Kennis
begin to fight.  Alex fights along with Fumi, Otoharuko, Chiemikue, Amao,
Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Ariaki, Yuka, and Kennis.  Melia fights alone.

Melia died.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Ariaki, Yuka, and Kennis finish
fighting.

Deboray joins Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi,
Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka,
Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, and Iemasamori.

Alex, Fumi, Otoharuko, Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko,
Chōbyō, Christina, Cher, Anie, Kiyohirō, Minatsuka, Hisamichi, Chris,
Yumiri, Shi, Hanase, Lino, Iemasamori, Deboray, Ariaki, Yuka, and Kennis
leave Lolan on the route to Gard.

# Five Hundred And Thirty Seven

This chapter is from Alex's point of view. A medium description of Gard
during early evening. Robert, Kanori, Shishōhei, Itsue, Zenkichi, Hisa,
Chinae, Etsumi, Yuri, Mane, Sanae, Suke, Miyaka, Tadanari, Yūsuke, Sako,
Omi, Manako, Nodoka, Genjirō, Tatsugi, Otoha, Atsunao, Hidemi, Otome,
Yoritaka, Nanae, Umeko, Hisakiko, Narunosuki, Rumi, Yayo, and Keichi are
already present. A reminder description of Robert. A reminder description
of Kanori. A reminder description of Shishōhei. A reminder description of
Itsue. A reminder description of Zenkichi. A reminder description of
Hisa. A reminder description of Chinae. A reminder description of Etsumi.
A reminder description of Yuri. A reminder description of Mane. A
reminder description of Sanae. A reminder description of Suke. A reminder
description of Miyaka. A reminder description of Tadanari. A reminder
description of Yūsuke. A reminder description of Sako. A reminder
description of Omi. A reminder description of Manako. A reminder
description of Nodoka. A reminder description of Genjirō. A reminder
description of Tatsugi. A reminder description of Otoha. A reminder
description of Atsunao. A reminder description of Hidemi. A reminder
description of Otome. A reminder description of Yoritaka. A reminder
description of Nanae. A reminder description of Umeko. A reminder
description of Hisakiko. A reminder description of Narunosuki. A reminder
description of Rumi. A reminder description of Yayo. A reminder
description of Keichi.

Yūsuke joins Alex, Olly, Fumi, Gakuma, Otoharuko, and Fred.

Zenkichi joins Alex.

Shishōhei joins Alex.

Kanori joins Alex.

Alex, Kanori, Shishōhei, Zenkichi, Fumi, Otoharuko, Yūsuke, Chiemikue,
Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie,
Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Yayo,
Iemasamori, Deboray, Ariaki, Yuka, and Kennis begin to fight.  Alex
fights along with Kanori, Shishōhei, Zenkichi, Fumi, Otoharuko, Yūsuke,
Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher,
Anie, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino,
Iemasamori, Deboray, Ariaki, Yuka, and Kennis.  Yayo fights alone.

Yayo died.

Alex, Kanori, Shishōhei, Zenkichi, Fumi, Otoharuko, Yūsuke, Chiemikue,
Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher, Anie,
Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino,
Iemasamori, Deboray, Ariaki, Yuka, and Kennis finish fighting.

Alex, Kanori, Shishōhei, Zenkichi, Fumi, Otoharuko, Miyaka, Yūsuke,
Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher,
Anie, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino,
Iemasamori, Deboray, Ariaki, Yuka, and Kennis begin to fight.  Alex
fights along with Kanori, Shishōhei, Zenkichi, Fumi, Otoharuko, Yūsuke,
Chiemikue, Amao, Chiko, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Cher,
Anie, Kiyohirō, Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino,
Iemasamori, Deboray, Ariaki, Yuka, and Kennis.  Miyaka fights alone.

Fumi died.

Chiko died.

Miyaka died.

Alex, Kanori, Shishōhei, Zenkichi, Otoharuko, Yūsuke, Chiemikue, Amao,
Toshi, Tadatoshi, Riko, Chōbyō, Christina, Anie, Kiyohirō, Minatsuka,
Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori, Deboray, Ariaki,
Yuka, and Kennis finish fighting.

Alex, Kanori, Shishōhei, Zenkichi, Hisa, Otoharuko, Yūsuke, Chiemikue,
Amao, Toshi, Tadatoshi, Riko, Chōbyō, Christina, Anie, Kiyohirō,
Minatsuka, Hisamichi, Chris, Yumiri, Shi, Hanase, Lino, Iemasamori,
Deboray, Ariaki, Yuka, and Kennis begin to fight.  Alex fights along with
Kanori, Shishōhei, Zenkichi, Otoharuko, Yūsuke, Chiemikue, Amao, Toshi,
Tadatoshi, Riko, Chōbyō, Christina, Anie, Kiyohirō, Minatsuka, Hisamichi,
Chris, Yumiri, Shi, Hanase, Lino, Iemasamori, Deboray, Ariaki, Yuka, and
Kennis.  Hisa fights alone.

Alex, Otoharuko, Toshi, Tadatoshi, Chōbyō, Anie, Hisamichi, Shi, Deboray,
Ariaki, and Kennis finish fighting.

Shishōhei leaves Gard on the route to Kott.

Amao and Lino leave Gard on the route to Sanfelsoni.

Chiemikue, Chris, and Yuka leave Gard on the route to Brannel.

Riko leaves Gard on the route to Edman.

Minatsuka, Yumiri, and Iemasamori leave Gard on the route to Coni.

Kiyohirō leaves Gard on the route to Pois.

Hanase leaves Gard on the route to Corn.

Hisa leaves Gard on the route to Wildi.

Zenkichi leaves Gard on the route to Ryum.

Alex, Itsue, Otoharuko, Toshi, Tadatoshi, Chōbyō, Anie, Hisamichi, Shi,
Deboray, Ariaki, and Kennis begin to fight.  Alex fights along with
Otoharuko, Toshi, Tadatoshi, Chōbyō, Anie, Hisamichi, Shi, Deboray,
Ariaki, and Kennis.  Itsue fights alone.

Kanori leaves Gard on the route to Golkman.

Yūsuke leaves Gard on the route to Spennarra.

Christina leaves Gard on the route to Otto.

Alex, Chōbyō, Deboray, and Kennis leave Gard on the route to Nogg.

# Five Hundred And Thirty Eight

This chapter is from Amao's point of view. Sanfelsoni is 947.39 km away
from the start of the novel. It is located in Gowders and it is a town. A
detailed description of Sanfelsoni during early evening. Mayu Tate is
already present. A detailed description of Mayu. They are 19 years old.
They were born at Trok in Cring.

Mayu joins Amao.

Amao, Mayu, and Lino leaves.

# Five Hundred And Thirty Nine

This chapter is from Chiemikue's point of view. Brannel is 635.54 km away
from the start of the novel. It is located in Ele and it is a town. A
detailed description of Brannel during early evening.

Chiemikue, Chris, and Yuka leaves.

# Five Hundred And Forty

This chapter is from Riko's point of view. Edman is 685.54 km away from
the start of the novel. It is located in Ele and it is a city. A detailed
description of Edman during early evening.

Riko leaves.

# Five Hundred And Forty One

This chapter is from Kiyohirō's point of view. Pois is 800.35 km away
from the start of the novel. It is located in Cring and it is a farm. A
detailed description of Pois during early evening.

Kiyohirō leaves.

# Five Hundred And Forty Two

This chapter is from Hanase's point of view. Corn is 637.84 km away from
the start of the novel. It is located in Ele and it is a farm. A detailed
description of Corn during early evening.

Hanase leaves.

# Five Hundred And Forty Three

This chapter is from Hisa's point of view. Wildi is 764.82 km away from
the start of the novel. It is located in Cring and it is a mountain. A
detailed description of Wildi during early evening.

Hisa leaves.

# Five Hundred And Forty Four

This chapter is from Zenkichi's point of view. Ryum is 723.92 km away
from the start of the novel. It is located in Ele and it is a city. A
detailed description of Ryum during early evening.

Zenkichi leaves.

# Five Hundred And Forty Five

This chapter is from Kanori's point of view. Golkman is 647.06 km away
from the start of the novel. It is located in Ele and it is a town. A
detailed description of Golkman during early evening.

Kanori leaves.

# Five Hundred And Forty Six

This chapter is from Yūsuke's point of view. Spennarra is 1,617.69 km
away from the start of the novel. It is located in Yokes and it is a
town. A detailed description of Spennarra during early evening. Konoka
Yuki, Julius Zimmons, and Charles Duffman are already present. A detailed
description of Konoka. They are 19 years old. They were born at Fason in
Gowders. A detailed description of Julius. They are 18 years old. They
were born at Siblicki in Chet. A detailed description of Charles. They
are 17 years old. They were born at Soe in Yokes.

Konoka joins Yūsuke.

Charles joins Yūsuke and Konoka.

Julius joins Yūsuke and Konoka.

Yūsuke, Konoka, Julius, and Charles leaves.

# Five Hundred And Forty Seven

This chapter is from Christina's point of view. Otto is 625.08 km away
from the start of the novel. It is located in Ele and it is a swamp. A
detailed description of Otto during early evening.

Christina leaves.

# Five Hundred And Forty Eight

This chapter is from Alex's point of view. A medium description of Gard
during early evening. Alex, Robert, Itsue, Chinae, Etsumi, Otoharuko,
Yuri, Mane, Sanae, Suke, Tadanari, Sako, Omi, Manako, Toshi, Tadatoshi,
Nodoka, Genjirō, Chōbyō, Tatsugi, Cher, Otoha, Atsunao, Hidemi, Otome,
Yoritaka, Anie, Nanae, Umeko, Hisakiko, Hisamichi, Narunosuki, Rumi, Shi,
Deboray, Ariaki, Keichi, and Kennis are already present. A reminder
description of Alex. A reminder description of Robert. A reminder
description of Itsue. A reminder description of Chinae. A reminder
description of Etsumi. A reminder description of Otoharuko. A reminder
description of Yuri. A reminder description of Mane. A reminder
description of Sanae. A reminder description of Suke. A reminder
description of Tadanari. A reminder description of Sako. A reminder
description of Omi. A reminder description of Manako. A reminder
description of Toshi. A reminder description of Tadatoshi. A reminder
description of Nodoka. A reminder description of Genjirō. A reminder
description of Chōbyō. A reminder description of Tatsugi. A reminder
description of Cher. A reminder description of Otoha. A reminder
description of Atsunao. A reminder description of Hidemi. A reminder
description of Otome. A reminder description of Yoritaka. A reminder
description of Anie. A reminder description of Nanae. A reminder
description of Umeko. A reminder description of Hisakiko. A reminder
description of Hisamichi. A reminder description of Narunosuki. A
reminder description of Rumi. A reminder description of Shi. A reminder
description of Deboray. A reminder description of Ariaki. A reminder
description of Keichi. A reminder description of Kennis.

Itsue leaves Gard on the route to Alvanh.

Anie leaves Gard on the route to Hini.

Otoharuko and Hisamichi leave Gard on the route to Jek.

Tadatoshi and Shi leave Gard on the route to Mazaris.

Toshi and Ariaki leave Gard on the route to Mas.

# Five Hundred And Forty Nine

This chapter is from Itsue's point of view. Alvanh is 645.38 km away from
the start of the novel. It is located in Ele and it is a farm. A detailed
description of Alvanh during early evening.

Itsue leaves.

# Five Hundred And Fifty

This chapter is from Anie's point of view. Hini is 822.63 km away from
the start of the novel. It is located in Cring and it is a farm. A
detailed description of Hini during early evening.

Anie leaves.

# Five Hundred And Fifty One

This chapter is from Otoharuko's point of view. Jek is 802.08 km away
from the start of the novel. It is located in Cring and it is a town. A
detailed description of Jek during early evening.

Otoharuko and Hisamichi leaves.

# Five Hundred And Fifty Two

This chapter is from Toshi's point of view. Mas is 554.57 km away from
the start of the novel. It is located in Ritt and it is a lake. A
detailed description of Mas during early evening.

Toshi and Ariaki leaves.

# Five Hundred And Fifty Three

This chapter is from Minatsuka's point of view. Coni is 560.91 km away
from the start of the novel. It is located in Ritt and it is a forest. A
detailed description of Coni during evening.

Minatsuka, Yumiri, and Iemasamori leave Coni on the route to Hiel.

# Five Hundred And Fifty Four

This chapter is from Tadatoshi's point of view. Mazaris is 622.52 km away
from the start of the novel. It is located in Ele and it is a farm. A
detailed description of Mazaris during morning.

Tadatoshi and Shi leave Mazaris on the route to Alando.

# Five Hundred And Fifty Five

This chapter is from Minatsuka's point of view. Hiel is 646.72 km away
from the start of the novel. It is located in Ele and it is a swamp. A
detailed description of Hiel during early afternoon.

Minatsuka, Yumiri, and Iemasamori leaves.

# Five Hundred And Fifty Six

This chapter is from Tadatoshi's point of view. Alando is 654.99 km away
from the start of the novel. It is located in Ele and it is a river. A
detailed description of Alando during early afternoon.

Tadatoshi and Shi leaves.

# Five Hundred And Fifty Seven

This chapter is from Alex's point of view. Nogg is 713.18 km away from
the start of the novel. It is located in Ele and it is a mountain. A
detailed description of Nogg during very early morning.

# Five Hundred And Fifty Eight

This chapter is from Shishōhei's point of view. Kott is 853.24 km away
from the start of the novel. It is located in Cring and it is a town. A
detailed description of Kott during early morning.

Shishōhei leaves Kott on the route to Auper.

# Five Hundred And Fifty Nine

This chapter is from Alex's point of view. A medium description of Nogg
during late morning. Alex, Chōbyō, Deboray, and Kennis are already
present. A reminder description of Alex. A reminder description of
Chōbyō. A reminder description of Deboray. A reminder description of
Kennis.

Alex, Chōbyō, Deboray, and Kennis leave Nogg on the route to Gard.

# Five Hundred And Sixty

This chapter is from Alex's point of view. A medium description of Gard
during late morning. Robert, Chinae, Etsumi, Yuri, Mane, Sanae, Suke,
Tadanari, Sako, Omi, Manako, Nodoka, Genjirō, Tatsugi, Cher, Otoha,
Atsunao, Hidemi, Otome, Yoritaka, Nanae, Umeko, Hisakiko, Narunosuki,
Rumi, and Keichi are already present. A reminder description of Robert. A
reminder description of Chinae. A reminder description of Etsumi. A
reminder description of Yuri. A reminder description of Mane. A reminder
description of Sanae. A reminder description of Suke. A reminder
description of Tadanari. A reminder description of Sako. A reminder
description of Omi. A reminder description of Manako. A reminder
description of Nodoka. A reminder description of Genjirō. A reminder
description of Tatsugi. A reminder description of Cher. A reminder
description of Otoha. A reminder description of Atsunao. A reminder
description of Hidemi. A reminder description of Otome. A reminder
description of Yoritaka. A reminder description of Nanae. A reminder
description of Umeko. A reminder description of Hisakiko. A reminder
description of Narunosuki. A reminder description of Rumi. A reminder
description of Keichi.

Alex, Suke, Chōbyō, Deboray, and Kennis begin to fight.  Alex fights
along with Chōbyō, Deboray, and Kennis.  Suke fights alone.

Suke and Omi begin to fight.  Suke fights alone.  Omi fights alone.

Omi and Genjirō begin to fight.  Omi fights alone.  Genjirō fights alone.

Genjirō and Hisakiko begin to fight.  Genjirō fights alone.  Hisakiko
fights alone.

Hisakiko died.

Genjirō finish fighting.

Genjirō died.

Omi finish fighting.

Omi died.

Suke finish fighting.

Suke died.

Alex, Chōbyō, Deboray, and Kennis finish fighting.

Alex, Chōbyō, Rumi, Deboray, and Kennis begin to fight.  Alex fights
along with Chōbyō, Deboray, and Kennis.  Rumi fights alone.

Rumi died.

Alex, Chōbyō, Deboray, and Kennis finish fighting.

Alex, Manako, Chōbyō, Deboray, and Kennis begin to fight.  Alex fights
along with Chōbyō, Deboray, and Kennis.  Manako fights alone.

Manako died.

Alex, Chōbyō, Deboray, and Kennis finish fighting.

Alex, Chōbyō, Deboray, and Kennis finish fighting.

Sanae joins Alex and Otoharuko.

Alex, Sanae, Nodoka, Chōbyō, Deboray, and Kennis begin to fight.  Alex
fights along with Sanae, Chōbyō, Deboray, and Kennis.  Nodoka fights
alone.

Nodoka died.

Alex, Sanae, Chōbyō, Deboray, and Kennis finish fighting.

Alex, Sanae, Chōbyō, Deboray, and Kennis begin a trial.

# Five Hundred And Sixty One

This chapter is from Alex's point of view. A reminder description of Gard
during evening. Alex, Robert, Chinae, Etsumi, Yuri, Mane, Sanae,
Tadanari, Sako, Chōbyō, Tatsugi, Cher, Otoha, Atsunao, Hidemi, Otome,
Yoritaka, Nanae, Umeko, Narunosuki, Deboray, Keichi, and Kennis are
already present. A reminder description of Alex. A reminder description
of Robert. A reminder description of Chinae. A reminder description of
Etsumi. A reminder description of Yuri. A reminder description of Mane. A
reminder description of Sanae. A reminder description of Tadanari. A
reminder description of Sako. A reminder description of Chōbyō. A
reminder description of Tatsugi. A reminder description of Cher. A
reminder description of Otoha. A reminder description of Atsunao. A
reminder description of Hidemi. A reminder description of Otome. A
reminder description of Yoritaka. A reminder description of Nanae. A
reminder description of Umeko. A reminder description of Narunosuki. A
reminder description of Deboray. A reminder description of Keichi. A
reminder description of Kennis.

Alex, Sanae, Chōbyō, Deboray, and Kennis finish their trial.

Alex, Sanae, Chōbyō, Deboray, and Kennis leave Gard on the route to
Sango.

# Five Hundred And Sixty Two

This chapter is from Shishōhei's point of view. Auper is 941.06 km away
from the start of the novel. It is located in Gowders and it is a lake. A
detailed description of Auper during midnight.

Shishōhei leaves.

# Five Hundred And Sixty Three

This chapter is from Alex's point of view. Sango is 822.26 km away from
the start of the novel. It is located in Cring and it is a mountain. A
detailed description of Sango during very early morning. Mayuko Tamura
and Jiichio Utanishi are already present. A detailed description of
Mayuko. They are 18 years old. They were born at Sch in Gowders. A
detailed description of Jiichio. They are 17 years old. They were born at
Adam in Gowders.

Alex, Sanae, Chōbyō, Mayuko, Deboray, and Kennis begin to fight.  Alex
fights along with Sanae, Chōbyō, Deboray, and Kennis.  Mayuko fights
alone.

Chōbyō died.

Mayuko died.

Alex, Sanae, and Kennis finish fighting.

Alex, Sanae, and Kennis start to train together.

# Five Hundred And Sixty Four

This chapter is from Alex's point of view. A reminder description of
Sango during late morning. Alex, Sanae, Jiichio, Deboray, and Kennis are
already present. A reminder description of Alex. A reminder description
of Sanae. A reminder description of Jiichio. A reminder description of
Deboray. A reminder description of Kennis.

Alex, Sanae, and Kennis finish their training.

Alex, Sanae, and Kennis begin a trial.

# Five Hundred And Sixty Five

This chapter is from Alex's point of view. A reminder description of
Sango during late evening. Alex, Sanae, Jiichio, Deboray, and Kennis are
already present. A reminder description of Alex. A reminder description
of Sanae. A reminder description of Jiichio. A reminder description of
Deboray. A reminder description of Kennis.

Alex, Sanae, and Kennis finish their trial.

Alex, Sanae, and Kennis leave Sango on the route to Heynollin.

# Five Hundred And Sixty Six

This chapter is from Alex's point of view. Heynollin is 1,028.78 km away
from the start of the novel. It is located in Gowders and it is a river.
A detailed description of Heynollin during afternoon.

Alex, Sanae, and Kennis begin some time together.

# Five Hundred And Sixty Seven

This chapter is from Alex's point of view. A reminder description of
Heynollin during very early morning. Alex, Sanae, and Kennis are already
present. A reminder description of Alex. A reminder description of Sanae.
A reminder description of Kennis.

Alex, Sanae, and Kennis finish their time together.

Alex, Sanae, and Kennis start to train together.

# Five Hundred And Sixty Eight

This chapter is from Alex's point of view. A reminder description of
Heynollin during very early morning. Alex, Sanae, and Kennis are already
present. A reminder description of Alex. A reminder description of Sanae.
A reminder description of Kennis.

Alex, Sanae, and Kennis finish their training.

Alex, Sanae, and Kennis leave Heynollin on the route to Sango.

# Five Hundred And Sixty Nine

This chapter is from Alex's point of view. A medium description of Sango
during very early morning. Jiichio and Deboray are already present. A
reminder description of Jiichio. A reminder description of Deboray.

Jiichio joins Alex, Sanae, and Virgie.

Alex, Sanae, Jiichio, and Kennis begin some time together.

# Five Hundred And Seventy

This chapter is from Alex's point of view. A reminder description of
Sango during morning. Alex, Sanae, Jiichio, Deboray, and Kennis are
already present. A reminder description of Alex. A reminder description
of Sanae. A reminder description of Jiichio. A reminder description of
Deboray. A reminder description of Kennis.

Alex, Sanae, Jiichio, and Kennis finish their time together.

Alex, Sanae, Jiichio, and Kennis leave Sango on the route to Jayao.

# Five Hundred And Seventy One

This chapter is from Alex's point of view. Jayao is 902.84 km away from
the start of the novel. It is located in Gowders and it is a town. A
detailed description of Jayao during very early morning.

# Five Hundred And Seventy Two

This chapter is from Alex's point of view. A reminder description of
Jayao during early evening. Alex, Sanae, Jiichio, and Kennis are already
present. A reminder description of Alex. A reminder description of Sanae.
A reminder description of Jiichio. A reminder description of Kennis.

Alex, Sanae, Jiichio, and Kennis leave Jayao on the route to Sango.

# Five Hundred And Seventy Three

This chapter is from Alex's point of view. A medium description of Sango
during early evening. Deboray is already present. A reminder description
of Deboray.

Alex, Sanae, Jiichio, and Kennis leave Sango on the route to Hesi.

# Five Hundred And Seventy Four

This chapter is from Alex's point of view. Hesi is 877.28 km away from
the start of the novel. It is located in Cring and it is a mountain. A
detailed description of Hesi during early morning. Aki Kawa is already
present. A detailed description of Aki. They are 19 years old. They were
born at Loord in Gowders.

Alex, Sanae, Jiichio, and Kennis begin a trial.

# Five Hundred And Seventy Five

This chapter is from Alex's point of view. A reminder description of Hesi
during late morning. Alex, Sanae, Aki, Jiichio, and Kennis are already
present. A reminder description of Alex. A reminder description of Sanae.
A reminder description of Aki. A reminder description of Jiichio. A
reminder description of Kennis.

Alex, Sanae, Jiichio, and Kennis finish their trial.

Alex, Sanae, Aki, Jiichio, and Kennis begin to fight.  Alex fights along
with Sanae, Jiichio, and Kennis.  Aki fights alone.

Sanae died.

Aki died.

Alex, Jiichio, and Kennis finish fighting.

Alex, Jiichio, and Kennis begin some time together.

# Five Hundred And Seventy Six

This chapter is from Alex's point of view. A reminder description of Hesi
during early evening. Alex, Jiichio, and Kennis are already present. A
reminder description of Alex. A reminder description of Jiichio. A
reminder description of Kennis.

Alex, Jiichio, and Kennis finish their time together.

Alex, Jiichio, and Kennis start to train together.

# Five Hundred And Seventy Seven

This chapter is from Alex's point of view. A reminder description of Hesi
during very early morning. Alex, Jiichio, and Kennis are already present.
A reminder description of Alex. A reminder description of Jiichio. A
reminder description of Kennis.

Alex, Jiichio, and Kennis finish their training.

Alex, Jiichio, and Kennis leave Hesi on the route to Nor.

# Five Hundred And Seventy Eight

This chapter is from Alex's point of view. Nor is 1,081.43 km away from
the start of the novel. It is located in Gowders and it is a lake. A
detailed description of Nor during early evening.

# Five Hundred And Seventy Nine

This chapter is from Alex's point of view. A reminder description of Nor
during late morning. Alex, Jiichio, and Kennis are already present. A
reminder description of Alex. A reminder description of Jiichio. A
reminder description of Kennis.

Alex, Jiichio, and Kennis leave Nor on the route to Hesi.

# Five Hundred And Eighty

This chapter is from Alex's point of view. A medium description of Hesi
during late morning.

Alex, Jiichio, and Kennis leave Hesi on the route to Skeepereth.

# Five Hundred And Eighty One

This chapter is from Alex's point of view. Skeepereth is 1,090.49 km away
from the start of the novel. It is located in Gowders and it is a
mountain. A detailed description of Skeepereth during early morning.

Alex, Jiichio, and Kennis begin some time together.

# Five Hundred And Eighty Two

This chapter is from Alex's point of view. A reminder description of
Skeepereth during late morning. Alex, Jiichio, and Kennis are already
present. A reminder description of Alex. A reminder description of
Jiichio. A reminder description of Kennis.

Alex, Jiichio, and Kennis finish their time together.

Alex, Jiichio, and Kennis begin a trial.

# Five Hundred And Eighty Three

This chapter is from Alex's point of view. A reminder description of
Skeepereth during late evening. Alex, Jiichio, and Kennis are already
present. A reminder description of Alex. A reminder description of
Jiichio. A reminder description of Kennis.

Alex, Jiichio, and Kennis finish their trial.

Alex, Jiichio, and Kennis leave Skeepereth on the route to Hesi.

# Five Hundred And Eighty Four

This chapter is from Alex's point of view. A medium description of Hesi
during late evening.

Alex, Jiichio, and Kennis leave Hesi on the route to Gard.

# Five Hundred And Eighty Five

This chapter is from Alex's point of view. A medium description of Gard
during late evening. Robert, Chinae, Etsumi, Yuri, Mane, Tadanari, Sako,
Tatsugi, Cher, Otoha, Atsunao, Hidemi, Otome, Yoritaka, Nanae, Umeko,
Narunosuki, and Keichi are already present. A reminder description of
Robert. A reminder description of Chinae. A reminder description of
Etsumi. A reminder description of Yuri. A reminder description of Mane. A
reminder description of Tadanari. A reminder description of Sako. A
reminder description of Tatsugi. A reminder description of Cher. A
reminder description of Otoha. A reminder description of Atsunao. A
reminder description of Hidemi. A reminder description of Otome. A
reminder description of Yoritaka. A reminder description of Nanae. A
reminder description of Umeko. A reminder description of Narunosuki. A
reminder description of Keichi.

Jōichi joins Alex, Kanori, and Shishōhei.

Alex, Jōichi, Yōji, Jiichio, and Kennis begin to fight.  Alex fights
along with Jōichi, Jiichio, and Kennis.  Yōji fights alone.

Alex, Jiichio, and Kennis finish fighting.

Alex, Jiichio, and Kennis leave Gard on the route to Wilk.

# Five Hundred And Eighty Six

This chapter is from Alex's point of view. Wilk is 581.74 km away from
the start of the novel. It is located in Ele and it is a swamp. A
detailed description of Wilk during very early morning.

Alex, Jiichio, and Kennis leave Wilk on the route to Maszett.

# Five Hundred And Eighty Seven

This chapter is from Alex's point of view. Maszett is 775.49 km away from
the start of the novel. It is located in Cring and it is a tundra. A
detailed description of Maszett during early evening. Kazu Futanida and
Azumi Koishi are already present. A detailed description of Kazu. They
are 17 years old. They were born at Man in Cring. A detailed description
of Azumi. They are 16 years old. They were born at Tras in Gowders.

Alex, Jiichio, and Kennis begin a trial.

# Five Hundred And Eighty Eight

This chapter is from Alex's point of view. A reminder description of
Maszett during late evening. Alex, Kazu, Jiichio, Azumi, and Kennis are
already present. A reminder description of Alex. A reminder description
of Kazu. A reminder description of Jiichio. A reminder description of
Azumi. A reminder description of Kennis.

Alex, Jiichio, and Kennis finish their trial.

Alex, Jiichio, and Kennis begin some time together.

# Five Hundred And Eighty Nine

This chapter is from Alex's point of view. A reminder description of
Maszett during early morning. Alex, Kazu, Jiichio, Azumi, and Kennis are
already present. A reminder description of Alex. A reminder description
of Kazu. A reminder description of Jiichio. A reminder description of
Azumi. A reminder description of Kennis.

Alex, Jiichio, and Kennis finish their time together.

Alex, Jiichio, and Kennis start to train together.

# Five Hundred And Ninety

This chapter is from Alex's point of view. A reminder description of
Maszett during early evening. Alex, Kazu, Jiichio, Azumi, and Kennis are
already present. A reminder description of Alex. A reminder description
of Kazu. A reminder description of Jiichio. A reminder description of
Azumi. A reminder description of Kennis.

Alex, Jiichio, and Kennis finish their training.

Alex, Kazu, Jiichio, and Kennis begin to fight.  Alex fights along with
Jiichio and Kennis.  Kazu fights alone.

Azumi joins Kazu.

Kazu died.

Alex, Jiichio, and Kennis finish fighting.

Alex, Jiichio, and Kennis leave Maszett on the route to Sco.

# Five Hundred And Ninety One

This chapter is from Alex's point of view. Sco is 793.90 km away from the
start of the novel. It is located in Cring and it is a forest. A detailed
description of Sco during late evening.

Alex, Jiichio, and Kennis start to train together.

# Five Hundred And Ninety Two

This chapter is from Alex's point of view. A reminder description of Sco
during late evening. Alex, Jiichio, and Kennis are already present. A
reminder description of Alex. A reminder description of Jiichio. A
reminder description of Kennis.

Alex, Jiichio, and Kennis finish their training.

Alex, Jiichio, and Kennis begin some time together.

# Five Hundred And Ninety Three

This chapter is from Alex's point of view. A reminder description of Sco
during early morning. Alex, Jiichio, and Kennis are already present. A
reminder description of Alex. A reminder description of Jiichio. A
reminder description of Kennis.

Alex, Jiichio, and Kennis finish their time together.

Alex, Jiichio, and Kennis leave Sco on the route to Gard.

# Five Hundred And Ninety Four

This chapter is from Alex's point of view. A medium description of Gard
during morning. Robert, Chinae, Etsumi, Yuri, Mane, Tadanari, Sako,
Tatsugi, Cher, Otoha, Atsunao, Hidemi, Otome, Yoritaka, Nanae, Umeko,
Narunosuki, and Keichi are already present. A reminder description of
Robert. A reminder description of Chinae. A reminder description of
Etsumi. A reminder description of Yuri. A reminder description of Mane. A
reminder description of Tadanari. A reminder description of Sako. A
reminder description of Tatsugi. A reminder description of Cher. A
reminder description of Otoha. A reminder description of Atsunao. A
reminder description of Hidemi. A reminder description of Otome. A
reminder description of Yoritaka. A reminder description of Nanae. A
reminder description of Umeko. A reminder description of Narunosuki. A
reminder description of Keichi.

Narunosuki joins Alex and Virgie.

Alex, Atsunao, Narunosuki, Jiichio, and Kennis begin to fight.  Alex
fights along with Narunosuki, Jiichio, and Kennis.  Atsunao fights alone.

Atsunao died.

Alex, Narunosuki, Jiichio, and Kennis finish fighting.

Sako joins Alex, Otoharuko, Sanae, Chiemikue, Ryūki, and Paulie.

Alex, Sako, Otoha, Narunosuki, Jiichio, and Kennis begin to fight.  Alex
fights along with Sako, Narunosuki, Jiichio, and Kennis.  Otoha fights
alone.

Otoha died.

Alex, Sako, Narunosuki, Jiichio, and Kennis finish fighting.

Alex, Sako, Yoritaka, Narunosuki, Jiichio, and Kennis begin to fight.
Alex fights along with Sako, Narunosuki, Jiichio, and Kennis.  Yoritaka
fights alone.

Yoritaka died.

Alex, Sako, Narunosuki, Jiichio, and Kennis finish fighting.

Otome joins Alex, Sako, and Riko.

Alex, Sako, Otome, Narunosuki, Jiichio, and Kennis leave Gard on the
route to Lepise.

# Five Hundred And Ninety Five

This chapter is from Alex's point of view. Lepise is 795.56 km away from
the start of the novel. It is located in Cring and it is a tundra. A
detailed description of Lepise during morning.

# Five Hundred And Ninety Six

This chapter is from Alex's point of view. A reminder description of
Lepise during morning. Alex, Sako, Otome, Narunosuki, Jiichio, and Kennis
are already present. A reminder description of Alex. A reminder
description of Sako. A reminder description of Otome. A reminder
description of Narunosuki. A reminder description of Jiichio. A reminder
description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, and Kennis leave Lepise on the
route to Gard.

# Five Hundred And Ninety Seven

This chapter is from Alex's point of view. A medium description of Gard
during morning. Robert, Chinae, Etsumi, Yuri, Mane, Tadanari, Tatsugi,
Cher, Hidemi, Nanae, Umeko, and Keichi are already present. A reminder
description of Robert. A reminder description of Chinae. A reminder
description of Etsumi. A reminder description of Yuri. A reminder
description of Mane. A reminder description of Tadanari. A reminder
description of Tatsugi. A reminder description of Cher. A reminder
description of Hidemi. A reminder description of Nanae. A reminder
description of Umeko. A reminder description of Keichi.

Alex, Chinae, Sako, Otome, Narunosuki, Jiichio, and Kennis begin to
fight.  Alex fights along with Sako, Otome, Narunosuki, Jiichio, and
Kennis.  Chinae fights alone.

Tadanari joins Chinae.

Chinae died.

Alex, Sako, Otome, Narunosuki, Jiichio, and Kennis finish fighting.

Alex, Sako, Otome, Narunosuki, Jiichio, and Kennis leave Gard on the
route to Zure.

# Five Hundred And Ninety Eight

This chapter is from Alex's point of view. Zure is 852.41 km away from
the start of the novel. It is located in Cring and it is a lake. A
detailed description of Zure during late evening.

Alex, Sako, Otome, Narunosuki, Jiichio, and Kennis leave Zure on the
route to Ateshins.

# Five Hundred And Ninety Nine

This chapter is from Alex's point of view. Ateshins is 1,028.07 km away
from the start of the novel. It is located in Gowders and it is a town. A
detailed description of Ateshins during morning.

Alex, Sako, Otome, Narunosuki, Jiichio, and Kennis leave Ateshins on the
route to Ehr.

# Six Hundred

This chapter is from Alex's point of view. Ehr is 1,081.57 km away from
the start of the novel. It is located in Gowders and it is a town. A
detailed description of Ehr during evening. Nobu Inuki, Wenda Cames,
Jeraldo Per, Jessie Edwarrice, Willip Hayers, and Debora Penters are
already present. A detailed description of Nobu. They are 18 years old.
They were born at Esch in Gowders. A detailed description of Wenda. They
are 19 years old. They were born at Trann in Chet. A detailed description
of Jeraldo. They are 19 years old. They were born at Pick in Chet. A
detailed description of Jessie. They are 20 years old. They were born at
Canne in Chet. A detailed description of Willip. They are 17 years old.
They were born at Grisman in Chet. A detailed description of Debora. They
are 19 years old. They were born at Crnka in Chet.

Debora joins Alex, Sako, Otome, Narunosuki, and Jiichio.

Alex, Sako, Otome, Narunosuki, Jiichio, Debora, and Kennis start to train
together.

# Six Hundred And One

This chapter is from Alex's point of view. A reminder description of Ehr
during early morning. Alex, Nobu, Sako, Wenda, Jeraldo, Jessie, Otome,
Willip, Narunosuki, Jiichio, Debora, and Kennis are already present. A
reminder description of Alex. A reminder description of Nobu. A reminder
description of Sako. A reminder description of Wenda. A reminder
description of Jeraldo. A reminder description of Jessie. A reminder
description of Otome. A reminder description of Willip. A reminder
description of Narunosuki. A reminder description of Jiichio. A reminder
description of Debora. A reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Debora, and Kennis finish their
training.

Alex, Sako, Otome, Narunosuki, Jiichio, Debora, and Kennis leave Ehr on
the route to Loff.

# Six Hundred And Two

This chapter is from Alex's point of view. Loff is 1,335.70 km away from
the start of the novel. It is located in Chet and it is a mountain. A
detailed description of Loff during morning. Franda Heri is already
present. A detailed description of Franda. They are 16 years old. They
were born at Trik in Yokes.

Franda joins Alex, Sako, Otome, Narunosuki, Jiichio, and Wandi.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis begin
some time together.

# Six Hundred And Three

This chapter is from Alex's point of view. A reminder description of Loff
during early evening. Alex, Sako, Otome, Narunosuki, Jiichio, Franda,
Debora, and Kennis are already present. A reminder description of Alex. A
reminder description of Sako. A reminder description of Otome. A reminder
description of Narunosuki. A reminder description of Jiichio. A reminder
description of Franda. A reminder description of Debora. A reminder
description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their time together.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis start
to train together.

# Six Hundred And Four

This chapter is from Alex's point of view. A reminder description of Loff
during evening. Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora,
and Kennis are already present. A reminder description of Alex. A
reminder description of Sako. A reminder description of Otome. A reminder
description of Narunosuki. A reminder description of Jiichio. A reminder
description of Franda. A reminder description of Debora. A reminder
description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their training.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis begin
a trial.

# Six Hundred And Five

This chapter is from Alex's point of view. A reminder description of Loff
during very early morning. Alex, Sako, Otome, Narunosuki, Jiichio,
Franda, Debora, and Kennis are already present. A reminder description of
Alex. A reminder description of Sako. A reminder description of Otome. A
reminder description of Narunosuki. A reminder description of Jiichio. A
reminder description of Franda. A reminder description of Debora. A
reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their trial.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis leave
Loff on the route to Kus.

# Six Hundred And Six

This chapter is from Alex's point of view. Kus is 1,541.38 km away from
the start of the novel. It is located in Yokes and it is a forest. A
detailed description of Kus during evening.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis begin
some time together.

# Six Hundred And Seven

This chapter is from Alex's point of view. A reminder description of Kus
during very early morning. Alex, Sako, Otome, Narunosuki, Jiichio,
Franda, Debora, and Kennis are already present. A reminder description of
Alex. A reminder description of Sako. A reminder description of Otome. A
reminder description of Narunosuki. A reminder description of Jiichio. A
reminder description of Franda. A reminder description of Debora. A
reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their time together.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis start
to train together.

# Six Hundred And Eight

This chapter is from Alex's point of view. A reminder description of Kus
during early afternoon. Alex, Sako, Otome, Narunosuki, Jiichio, Franda,
Debora, and Kennis are already present. A reminder description of Alex. A
reminder description of Sako. A reminder description of Otome. A reminder
description of Narunosuki. A reminder description of Jiichio. A reminder
description of Franda. A reminder description of Debora. A reminder
description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their training.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis begin
a trial.

# Six Hundred And Nine

This chapter is from Alex's point of view. A reminder description of Kus
during very early morning. Alex, Sako, Otome, Narunosuki, Jiichio,
Franda, Debora, and Kennis are already present. A reminder description of
Alex. A reminder description of Sako. A reminder description of Otome. A
reminder description of Narunosuki. A reminder description of Jiichio. A
reminder description of Franda. A reminder description of Debora. A
reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their trial.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis leave
Kus on the route to Byrdselman.

# Six Hundred And Ten

This chapter is from Alex's point of view. Byrdselman is 1,609.80 km away
from the start of the novel. It is located in Yokes and it is a tundra. A
detailed description of Byrdselman during early afternoon. Frederry Olin
and Ric Sturface are already present. A detailed description of Frederry.
They are 17 years old. They were born at Albrinas in Rizzian. A detailed
description of Ric. They are 18 years old. They were born at Dam in
Rizzian.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis begin
a trial.

# Six Hundred And Eleven

This chapter is from Alex's point of view. A reminder description of
Byrdselman during early evening. Alex, Sako, Frederry, Ric, Otome,
Narunosuki, Jiichio, Franda, Debora, and Kennis are already present. A
reminder description of Alex. A reminder description of Sako. A reminder
description of Frederry. A reminder description of Ric. A reminder
description of Otome. A reminder description of Narunosuki. A reminder
description of Jiichio. A reminder description of Franda. A reminder
description of Debora. A reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their trial.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis start
to train together.

# Six Hundred And Twelve

This chapter is from Alex's point of view. A reminder description of
Byrdselman during early morning. Alex, Sako, Frederry, Ric, Otome,
Narunosuki, Jiichio, Franda, Debora, and Kennis are already present. A
reminder description of Alex. A reminder description of Sako. A reminder
description of Frederry. A reminder description of Ric. A reminder
description of Otome. A reminder description of Narunosuki. A reminder
description of Jiichio. A reminder description of Franda. A reminder
description of Debora. A reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their training.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis leave
Byrdselman on the route to Kowski.

# Six Hundred And Thirteen

This chapter is from Alex's point of view. Kowski is 1,909.47 km away
from the start of the novel. It is located in Rizzian and it is a farm. A
detailed description of Kowski during afternoon.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis begin
a trial.

# Six Hundred And Fourteen

This chapter is from Alex's point of view. A reminder description of
Kowski during very early morning. Alex, Sako, Otome, Narunosuki, Jiichio,
Franda, Debora, and Kennis are already present. A reminder description of
Alex. A reminder description of Sako. A reminder description of Otome. A
reminder description of Narunosuki. A reminder description of Jiichio. A
reminder description of Franda. A reminder description of Debora. A
reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their trial.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis leave
Kowski on the route to Byrdselman.

# Six Hundred And Fifteen

This chapter is from Alex's point of view. A medium description of
Byrdselman during very early morning. Frederry and Ric are already
present. A reminder description of Frederry. A reminder description of
Ric.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis begin
some time together.

# Six Hundred And Sixteen

This chapter is from Alex's point of view. A reminder description of
Byrdselman during early morning. Alex, Sako, Frederry, Ric, Otome,
Narunosuki, Jiichio, Franda, Debora, and Kennis are already present. A
reminder description of Alex. A reminder description of Sako. A reminder
description of Frederry. A reminder description of Ric. A reminder
description of Otome. A reminder description of Narunosuki. A reminder
description of Jiichio. A reminder description of Franda. A reminder
description of Debora. A reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their time together.

Alex, Sako, Frederry, Otome, Narunosuki, Jiichio, Franda, Debora, and
Kennis begin to fight.  Alex fights along with Sako, Otome, Narunosuki,
Jiichio, Franda, Debora, and Kennis.  Frederry fights alone.

Frederry and Ric begin to fight.  Frederry fights alone.  Ric fights
alone.

Ric died.

Frederry finish fighting.

Frederry died.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
fighting.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis leave
Byrdselman on the route to Ehr.

# Six Hundred And Seventeen

This chapter is from Alex's point of view. A medium description of Ehr
during early afternoon. Nobu, Wenda, Jeraldo, Jessie, and Willip are
already present. A reminder description of Nobu. A reminder description
of Wenda. A reminder description of Jeraldo. A reminder description of
Jessie. A reminder description of Willip.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis begin
some time together.

# Six Hundred And Eighteen

This chapter is from Alex's point of view. A reminder description of Ehr
during afternoon. Alex, Nobu, Sako, Wenda, Jeraldo, Jessie, Otome,
Willip, Narunosuki, Jiichio, Franda, Debora, and Kennis are already
present. A reminder description of Alex. A reminder description of Nobu.
A reminder description of Sako. A reminder description of Wenda. A
reminder description of Jeraldo. A reminder description of Jessie. A
reminder description of Otome. A reminder description of Willip. A
reminder description of Narunosuki. A reminder description of Jiichio. A
reminder description of Franda. A reminder description of Debora. A
reminder description of Kennis.

Alex, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis finish
their time together.

Alex, Nobu, Sako, Otome, Narunosuki, Jiichio, Franda, Debora, and Kennis
begin to fight.  Alex fights along with Sako, Otome, Narunosuki, Jiichio,
Franda, Debora, and Kennis.  Nobu fights alone.

Nobu died.

Jessie joins Alex, Otoharuko, Sako, Tadatoshi, Riko, and Christina.

Willip joins Alex, Sako, Jessie, and Otome.

Alex, Sako, Jessie, Otome, Willip, Narunosuki, Jiichio, Franda, Debora,
and Kennis finish fighting.

Wenda joins Alex, Otoharuko, Sanae, Chiemikue, Ryūki, Paulie, and Sako.

Alex, Sako, Wenda, Jessie, Otome, Willip, Narunosuki, Jiichio, Franda,
Debora, and Kennis begin a trial.

# Six Hundred And Nineteen

This chapter is from Alex's point of view. A reminder description of Ehr
during early evening. Alex, Sako, Wenda, Jeraldo, Jessie, Otome, Willip,
Narunosuki, Jiichio, Franda, Debora, and Kennis are already present. A
reminder description of Alex. A reminder description of Sako. A reminder
description of Wenda. A reminder description of Jeraldo. A reminder
description of Jessie. A reminder description of Otome. A reminder
description of Willip. A reminder description of Narunosuki. A reminder
description of Jiichio. A reminder description of Franda. A reminder
description of Debora. A reminder description of Kennis.

Alex, Sako, Wenda, Jessie, Otome, Willip, Narunosuki, Jiichio, Franda,
Debora, and Kennis finish their trial.

Alex, Sako, Wenda, Jeraldo, Jessie, Otome, Willip, Narunosuki, Jiichio,
Franda, Debora, and Kennis begin to fight.  Alex fights along with Sako,
Wenda, Jessie, Otome, Willip, Narunosuki, Jiichio, Franda, Debora, and
Kennis.  Jeraldo fights alone.

Willip died.

Sako died.

Jeraldo died.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis
finish fighting.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis leave
Ehr on the route to Delle.

# Six Hundred And Twenty

This chapter is from Alex's point of view. Delle is 1,159.14 km away from
the start of the novel. It is located in Chet and it is a farm. A
detailed description of Delle during late morning.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis leave
Delle on the route to Fout.

# Six Hundred And Twenty One

This chapter is from Alex's point of view. Fout is 1,259.42 km away from
the start of the novel. It is located in Chet and it is a city. A
detailed description of Fout during early morning.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis begin
some time together.

# Six Hundred And Twenty Two

This chapter is from Alex's point of view. A reminder description of Fout
during noon. Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and
Kennis are already present. A reminder description of Alex. A reminder
description of Wenda. A reminder description of Jessie. A reminder
description of Otome. A reminder description of Narunosuki. A reminder
description of Jiichio. A reminder description of Franda. A reminder
description of Kennis.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis
finish their time together.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis leave
Fout on the route to Gard.

# Six Hundred And Twenty Three

This chapter is from Alex's point of view. A medium description of Gard
during afternoon. Robert, Etsumi, Yuri, Mane, Tadanari, Tatsugi, Cher,
Hidemi, Nanae, Umeko, and Keichi are already present. A reminder
description of Robert. A reminder description of Etsumi. A reminder
description of Yuri. A reminder description of Mane. A reminder
description of Tadanari. A reminder description of Tatsugi. A reminder
description of Cher. A reminder description of Hidemi. A reminder
description of Nanae. A reminder description of Umeko. A reminder
description of Keichi.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis start
to train together.

# Six Hundred And Twenty Four

This chapter is from Alex's point of view. A reminder description of Gard
during very early morning. Alex, Robert, Etsumi, Yuri, Mane, Tadanari,
Wenda, Tatsugi, Cher, Jessie, Hidemi, Otome, Nanae, Umeko, Narunosuki,
Jiichio, Franda, Keichi, and Kennis are already present. A reminder
description of Alex. A reminder description of Robert. A reminder
description of Etsumi. A reminder description of Yuri. A reminder
description of Mane. A reminder description of Tadanari. A reminder
description of Wenda. A reminder description of Tatsugi. A reminder
description of Cher. A reminder description of Jessie. A reminder
description of Hidemi. A reminder description of Otome. A reminder
description of Nanae. A reminder description of Umeko. A reminder
description of Narunosuki. A reminder description of Jiichio. A reminder
description of Franda. A reminder description of Keichi. A reminder
description of Kennis.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis
finish their training.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis begin
some time together.

# Six Hundred And Twenty Five

This chapter is from Alex's point of view. A reminder description of Gard
during early morning. Alex, Robert, Etsumi, Yuri, Mane, Tadanari, Wenda,
Tatsugi, Cher, Jessie, Hidemi, Otome, Nanae, Umeko, Narunosuki, Jiichio,
Franda, Keichi, and Kennis are already present. A reminder description of
Alex. A reminder description of Robert. A reminder description of Etsumi.
A reminder description of Yuri. A reminder description of Mane. A
reminder description of Tadanari. A reminder description of Wenda. A
reminder description of Tatsugi. A reminder description of Cher. A
reminder description of Jessie. A reminder description of Hidemi. A
reminder description of Otome. A reminder description of Nanae. A
reminder description of Umeko. A reminder description of Narunosuki. A
reminder description of Jiichio. A reminder description of Franda. A
reminder description of Keichi. A reminder description of Kennis.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, and Kennis
finish their time together.

Etsumi joins Alex, Jōichi, and Vin.

Keichi joins Alex, Etsumi, Wenda, Jessie, Otome, Willip, Narunosuki,
Jiichio, and Franda.

Alex, Robert, Etsumi, Wenda, Tatsugi, Jessie, Otome, Narunosuki, Jiichio,
Franda, Keichi, and Kennis begin to fight.  Alex fights along with
Etsumi, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, Keichi, and
Kennis.  Robert fights along with Tatsugi.

Robert died.

Etsumi and Nanae begin to fight.  Etsumi fights alone.  Nanae fights
alone.

Alex, Wenda, Jessie, Otome, Narunosuki, Jiichio, Franda, Keichi, and
Kennis finish fighting.

Umeko joins Nanae.

Nanae died.

Etsumi finish fighting.

# Six Hundred And Twenty Six

This chapter is from Alex's point of view. A medium description of Cord
during early morning. Eliz, Jose, Juanicole, and Man are already present.
A reminder description of Eliz. A reminder description of Jose. A
reminder description of Juanicole. A reminder description of Man.

THE END

