#region Namespaces

using Autofac;

#endregion

namespace GirlKillsDragon.Cli
{
	public class CliModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			// We need to register all the commands.
			builder.RegisterAssemblyTypes(GetType().Assembly)
				.Where(t => t.Name.EndsWith("Command"))
				.AsSelf()
				.AsImplementedInterfaces()
				.PropertiesAutowired();
			builder.RegisterAssemblyTypes(GetType().Assembly)
				.Where(t => t.Name.EndsWith("Options"))
				.AsImplementedInterfaces();
		}
	}
}
