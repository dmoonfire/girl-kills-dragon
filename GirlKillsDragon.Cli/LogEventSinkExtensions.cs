#region Namespaces

using Serilog;
using Serilog.Configuration;

#endregion

namespace GirlKillsDragon.Cli
{
	public static class LogEventSinkExtensions
	{
		public static LoggerConfiguration CountingSink(
			this LoggerSinkConfiguration loggerConfiguration,
			out CountingLogEventSink sink)
		{
			sink = new CountingLogEventSink();

			return loggerConfiguration.Sink(sink);
		}
	}
}
