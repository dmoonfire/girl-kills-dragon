#region Namespaces

using System;
using CommandLine;

#endregion

namespace GirlKillsDragon.Cli.Commands
{
	public abstract class CommandOptionsBase : ICommandOptions
	{
		[Option('d', "debug", HelpText = "Enables additional debugging output.")]
		public bool Debug { get; set; }

		[Option("pause", HelpText = "Pause after running the command.")]
		public bool Pause { get; set; }

		[Option("pause-on-error", HelpText = "Pause the output if there was an error.")]
		public bool PauseOnError { get; set; }

		[Option("pause-on-warning", HelpText = "Pause the output if there was a warning or error.")]
		public bool PauseOnWarning { get; set; }

		[Option('v', "verbose", HelpText = "Enables a lot of debugging output.")]
		public bool Verbose { get; set; }

		public abstract Type CommandType { get; }
	}
}
