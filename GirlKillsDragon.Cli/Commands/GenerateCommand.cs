#region Namespaces

using System;
using System.IO;
using System.Text;
using GirlKillsDragon.IO;
using GirlKillsDragon.Plots;
using GirlKillsDragon.Plots.Resolvers;
using GirlKillsDragon.Plots.Validators;
using GirlKillsDragon.Writing;
using Serilog;

#endregion

namespace GirlKillsDragon.Cli.Commands
{
	public class GenerateCommand : CommandBase<GenerateCommandOptions>
	{
		private readonly DetailsLoader _detailsLoader;

		private readonly DgmlProjectWriter _dgmlProjectWriter;

		private readonly DotProjectWriter _dotProjectWriter;

		private readonly JsonProjectWriter _jsonProjectWriter;

		private readonly ILogger _log;

		private readonly PlotArranger _plotArranger;

		private readonly PlotInjection _plotInjection;

		private readonly Project _project;

		private readonly ResolverManager _resolvers;

		private readonly ValidatorManager _validators;

		public GenerateCommand(
			ILogger log,
			Project project,
			PlotInjection plotInjection,
			PlotArranger plotArranger,
			ValidatorManager validators,
			ResolverManager resolvers,
			DgmlProjectWriter dgmlProjectWriter,
			DotProjectWriter dotProjectWriter,
			JsonProjectWriter jsonProjectWriter,
			DetailsLoader detailsLoader)
		{
			_log = log;
			_project = project;
			_plotInjection = plotInjection;
			_plotArranger = plotArranger;
			_resolvers = resolvers;
			_validators = validators;

			_dgmlProjectWriter = dgmlProjectWriter;
			_dotProjectWriter = dotProjectWriter;
			_jsonProjectWriter = jsonProjectWriter;

			_detailsLoader = detailsLoader;
		}

		public override void Run()
		{
			// Figure out when we started.
			var started = DateTime.UtcNow;

			// Call the base implementation to load everything into memory.
			base.Run();

			// Load the details.
			_detailsLoader.Load();

			// Load the project.
			var projectFile = new FileInfo(Options.ProjectFileName);
			var projectReader = new ProjectReader();
			var projectData = projectReader.Read(projectFile);

			// Make a bit of noise about the project.
			_log.Information("Seed {0}", projectData.Seed);

			// Initialize the project with the data. This hooks up the various elements we need to build
			// out the plot and write out the results.
			_project.Initialize(projectData);

			// Plots start by seeding them with a series of plot elements. This creates a very basic "shell"
			// of plot items which we'll introduce complexity with later steps.
			_plotInjection.Seed();

			// Insert a number of items into the plot until we have a desired
			// complexity.
			if (!_plotInjection.Inject())
			{
				_log.Error("There was problems injecting plots into the graph.");
				WriteDumps();
				return;
			}

			// Validate the project to ensure we haven't gotten an impossible situation.
			if (!_validators.Validate())
			{
				_log.Error("Could not validate the project for rendering.");
				WriteDumps();
				return;
			}

			// Resolve all the internals variables.
			_resolvers.ResolvePlot();

			// Insert placholder plot items in between the various plots to
			// arrange something based on Dwight V. Swain's Scene/Sequel. The
			// basic idea is that action scenes should be followed by quieter
			// ones to give the reader room to "breathe" between the action.

			// Arrange the plot items into chapters.
			var chapters = _plotArranger.Arrange();

			// Use the project's author to write out the plots to a file.
			using (var stream = File.Open(Options.OutputFileName, FileMode.Create, FileAccess.Write))
			using (var writer = new StreamWriter(stream, new UTF8Encoding(false)))
			{
				// We want consistent line endings so Git doesn't see changes.
				writer.NewLine = "\n";

				// Write out the project.
				_project.Write(writer, chapters);

				// Always finish with a blank line for POSIX love.
				writer.WriteLine();
			}

			// If we have project writer requests, do so.
			WriteDumps();

			// Write out how long we took.
			var elapsed = DateTime.UtcNow - started;

			_log.Information("Generation took {0}", elapsed);
		}

		private void WriteDgml()
		{
			// If we don't have a DGML request, don't write it out.
			if (string.IsNullOrWhiteSpace(Options.DgmlFileName))
			{
				return;
			}

			// Use the project's author to write out the plots to a file.
			using (var stream = File.Open(Options.DgmlFileName, FileMode.Create, FileAccess.Write))
			using (var writer = new StreamWriter(stream, new UTF8Encoding(false)))
			{
				// We want consistent line endings so Git doesn't see changes.
				writer.NewLine = "\n";

				// Write out the project.
				var dgmlWriter = _dgmlProjectWriter;

				_log.Information("Writing DGML: {0}", Options.DgmlFileName);
				dgmlWriter.Write(writer);

				// Always finish with a blank line for POSIX love.
				writer.WriteLine();
			}
		}

		private void WriteDot()
		{
			// If we don't have a Dot request, don't write it out.
			if (string.IsNullOrWhiteSpace(Options.DotFileName))
			{
				return;
			}

			// Use the project's author to write out the plots to a file.
			using (var stream = File.Open(Options.DotFileName, FileMode.Create, FileAccess.Write))
			using (var writer = new StreamWriter(stream, new UTF8Encoding(false)))
			{
				// We want consistent line endings so Git doesn't see changes.
				writer.NewLine = "\n";

				// Write out the project.
				var dotWriter = _dotProjectWriter;

				_log.Information("Writing Dot: {0}", Options.DotFileName);
				dotWriter.Write(writer);

				// Always finish with a blank line for POSIX love.
				writer.WriteLine();
			}
		}

		private void WriteDumps()
		{
			WriteDgml();
			WriteDot();
			WriteJson();
		}

		private void WriteJson()
		{
			// If we don't have a DGML request, don't write it out.
			if (string.IsNullOrWhiteSpace(Options.JsonFileName))
			{
				return;
			}

			// Use the project's author to write out the plots to a file.
			using (var stream = File.Open(Options.JsonFileName, FileMode.Create, FileAccess.Write))
			using (var writer = new StreamWriter(stream, new UTF8Encoding(false)))
			{
				// We want consistent line endings so Git doesn't see changes.
				writer.NewLine = "\n";

				// Write out the project.
				_log.Information("Writing JSON: {0}", Options.JsonFileName);
				_jsonProjectWriter.Write(writer);

				// Always finish with a blank line for POSIX love.
				writer.WriteLine();
			}
		}
	}
}
