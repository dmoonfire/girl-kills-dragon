#region Namespaces

using System;

#endregion

namespace GirlKillsDragon.Cli.Commands
{
	public interface ICommandOptions
	{
		/// <summary>
		/// Returns the type of the command associated with this option.
		/// </summary>
		Type CommandType { get; }
	}
}
