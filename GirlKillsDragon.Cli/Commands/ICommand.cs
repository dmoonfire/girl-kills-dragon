namespace GirlKillsDragon.Cli.Commands
{
	public interface ICommand
	{
		/// <summary>
		/// Gets or sets the options associated with this command.
		/// </summary>
		ICommandOptions CommandOptions { get; set; }

		/// <summary>
		/// Executes the implemented command.
		/// </summary>
		void Run();
	}
}
