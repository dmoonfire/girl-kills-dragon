#region Namespaces

using Autofac;
using GirlKillsDragon.Actors;
using GirlKillsDragon.Details;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using GirlKillsDragon.Plots.Resolvers;
using GirlKillsDragon.Plots.Validators;
using SceneSkope.Serilog.Sinks.XUnit;
using Serilog;
using Serilog.Events;
using Xunit;
using Xunit.Abstractions;

#endregion

namespace GirlKillsDragon.Tests
{
	/// <summary>
	/// Base class which contains much of the functionality to use Autofac to generate the compontents.
	/// </summary>
	public abstract class PlotTestBase
	{
		private readonly IContainer _container;

		protected PlotTestBase(ITestOutputHelper output)
		{
			// Set up the logging sink for our output.
			var log = output.CreateSerilogLogger(LogEventLevel.Verbose);

			// Set up the DI/IoC container which wires everything together.
			var builder = new ContainerBuilder();

			builder.RegisterInstance(log).As<ILogger>().SingleInstance();
			builder.RegisterInstance(new FileSystem()).As<FileSystem>();
			builder.RegisterAssemblyModules(typeof(CoreModule).Assembly);

			_container = builder.Build();

			// Set up the one-only objects.
			WriterContext = _container.Resolve<WriterContext>();
		}

		protected ActorManager Actors => _container.Resolve<ActorManager>();

		protected Chaos Chaos => _container.Resolve<Chaos>();

		protected CountryActorNamingResolver CountryActorNamingResolver => _container.Resolve<CountryActorNamingResolver>();

		protected NodeGraph Graph => _container.Resolve<NodeGraph>();

		protected LocationManager Locations => _container.Resolve<LocationManager>();

		protected ILogger Log => _container.Resolve<ILogger>();

		protected NodeManager Nodes => _container.Resolve<NodeManager>();

		protected PlotDateTimeResolver PlotDateTimeResolver => _container.Resolve<PlotDateTimeResolver>();

		protected ResolverManager Resolvers => _container.Resolve<ResolverManager>();

		protected ValidatorManager Validators => _container.Resolve<ValidatorManager>();

		protected WriterContext WriterContext { get; }

		/// <summary>
		/// Internal function to resolve a specific type.
		/// </summary>
		protected T Resolve<T>()
		{
			return _container.Resolve<T>();
		}

		protected void Validate()
		{
			Assert.True(Validators.Validate());
		}

		protected void ValidateAndResolve()
		{
			// Resolve the internal variables.
			Resolvers.ResolveInjection();
			Resolvers.ResolvePlot();

			// Validate the project to ensure we haven't gotten an impossible situation.
			Validate();
		}
	}
}
