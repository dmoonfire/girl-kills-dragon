#region Namespaces

using GirlKillsDragon.Writing;
using Xunit;

#endregion

namespace GirlKillsDragon.Tests
{
	public class ParagraphBuilderTests
	{
		[Fact]
		public void SingleWord()
		{
			var builder = new ParagraphBuilder();

			builder.Add("abc");

			var results = builder.ToString(5);

			Assert.Equal(
				new[]
				{
					"",
					"abc"
				},
				results.Split("\n"));
		}

		[Fact]
		public void ThreeLines()
		{
			var builder = new ParagraphBuilder();

			builder.Add("abc def ghi");

			var results = builder.ToString(5);

			Assert.Equal(
				new[]
				{
					"",
					"abc",
					"def",
					"ghi"
				},
				results.Split("\n"));
		}

		[Fact]
		public void TwoLines()
		{
			var builder = new ParagraphBuilder();

			builder.Add("abc def");

			var results = builder.ToString(5);

			Assert.Equal(
				new[]
				{
					"",
					"abc",
					"def"
				},
				results.Split("\n"));
		}
	}
}
