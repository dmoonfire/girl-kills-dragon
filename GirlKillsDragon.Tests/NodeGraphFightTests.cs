#region Namespaces

using GirlKillsDragon.Actors;
using GirlKillsDragon.Locations;
using GirlKillsDragon.Nodes;
using Xunit;
using Xunit.Abstractions;

#endregion

namespace GirlKillsDragon.Tests
{
	public class NodeGraphFightTests : PlotTestBase
	{
		private readonly Actor _actor1;

		private readonly Actor _actor2;

		private readonly BeginFightNode _beginFight;

		private readonly BornNode _born1;

		private readonly BornNode _born2;

		private readonly DeathNode _death1;

		private readonly EndFightNode _endFight;

		private readonly ExitNode _exit2;

		private readonly Location _location;

		private readonly Node _middle1;

		private readonly Node _middle2;

		private readonly Node _middle3;

		public NodeGraphFightTests(ITestOutputHelper output) : base(output)
		{
			// Create the actors and locations we need.
			_actor1 = Actors.Create();
			_actor2 = Actors.Create();
			_location = Locations.Create();

			// Create the nodes we care about.
			_born1 = new BornNode(_location, _actor1);
			_born2 = new BornNode(_location, _actor2);
			_beginFight = new BeginFightNode(_location);
			_middle1 = new Node(_location);
			_middle2 = new Node(_location);
			_middle3 = new Node(_location);
			_death1 = new DeathNode(_location, _actor1);
			_endFight = new EndFightNode(_location);
			_exit2 = new ExitNode(_location, _actor2);

			// Connect everything together.
			Nodes.InitialNode = _beginFight;
			Graph.Connect(_born1, _beginFight, _actor1);
			Graph.Connect(_born2, _beginFight, _actor2);
			Graph.Connect(_beginFight, _middle1, _actor1, _actor2);
			Graph.Connect(_middle1, _death1, _actor1);
			Graph.Connect(_middle1, _middle2, _middle3, _actor2);
			Graph.Connect(_middle3, _endFight, _exit2, _actor2);

			// Validate to make sure everything is good.
			Validate();
		}

		[Fact]
		public void BeginFight()
		{
			Assert.True(Graph.IsInFight(_beginFight));
		}

		[Fact]
		public void Born1()
		{
			Assert.False(Graph.IsInFight(_born1));
		}

		[Fact]
		public void Born2()
		{
			Assert.False(Graph.IsInFight(_born2));
		}

		[Fact]
		public void Death1()
		{
			Assert.True(Graph.IsInFight(_death1));
		}

		[Fact]
		public void EndFight()
		{
			Assert.False(Graph.IsInFight(_endFight));
		}

		[Fact]
		public void Exit2()
		{
			Assert.False(Graph.IsInFight(_exit2));
		}

		[Fact]
		public void Middle1()
		{
			Assert.True(Graph.IsInFight(_middle1));
		}

		[Fact]
		public void Middle2()
		{
			Assert.True(Graph.IsInFight(_middle2));
		}

		[Fact]
		public void Middle3()
		{
			Assert.True(Graph.IsInFight(_middle2));
		}
	}
}
