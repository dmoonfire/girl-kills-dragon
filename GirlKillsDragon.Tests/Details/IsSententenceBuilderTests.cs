#region Namespaces

using GirlKillsDragon.Details;
using Xunit;
using Xunit.Abstractions;

#endregion

namespace GirlKillsDragon.Tests.Details
{
	public class IsSententenceBuilderTests : PlotTestBase
	{
		public IsSententenceBuilderTests(ITestOutputHelper output) : base(output)
		{
		}

		[Fact]
		public void Details()
		{
			// We have an arbitrary object that represents a "thing" such as a setting or character.
			// All things have names which is the simpliest thing to figure out.
			var thing = new Noun("Bob");

			// We need a detail about Bob that we'll be presenting. Details come in sets which are
			// added and removed based on the situation of the story.
			var details = new DetailSet
			{
				new Detail(DetailType.Is, "a man")
			};
			var nounDetails = new NounDetail("test", details);

			thing.Details.Add(nounDetails);

			// We gather up things together since we may have sentences that references multiple
			// in a single set.
			WriterContext.Nouns.Add(thing);
			WriterContext.Subject = thing;

			// Now that we have a pool of details (well, one), we need to be able to communicate them
			// to the reader. We do this via a sentence builder.
			SentenceBuilderBase sentenceBuilder = new IsSentenceBuilder();

			// Write out the sentence and verify it.
			var sentence = sentenceBuilder.Write(WriterContext);

			Assert.Equal("Bob was a man.", sentence);
		}
	}
}
