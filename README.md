Girl Kills Dragon
=================

Girl Kills Dragon is a procedural fantasy novel generator written for [NaNoGenMo 2018](https://github.com/NaNoGenMo/2017/issues/12). The primary method for the program is to create a directed acyclic graph of plot elements and use those to generate a complex plot with multiple points of view and then render the results into a Markdown file.

Outstanding Tasks
-----------------

* We really shouldn't have quests and injections in most fights.
* Nodes
    * Split the Party to split apart a group of characters.
    * Second chance for a death to have an another adventure instead of dying.
* Inciting event should be based on a seed factory instead of hard-coded.
* Training, Relationship, and Arrivals really should be during "normal" daylight hours
* Add `IsReduction` to the seed class
    * Have an additional phase that attempts to reduce all plots down to a single point.

### Phase 2 (November 16+)

* Use arrival time span to figure out how people respond when they arrive
* Name actors based on their country
* Figure out how to describe things.
* Figure out how to name characters
* Locations need to have a type, such as natural (forest, swamp) or urban

Building and Running
--------------------

```
dotnet restore
dotnet run -p GirlKillsDragon.Cli generate -- -p NaNoGenMo.yaml -o NaNoGenMo.md
```

The output is put into `NaNoGenMo.md`.

Tests can be run using:

```
dotnet test
```

Working Notes
-------------

"A orange haze hung over the city of Hamburg."
"{environment} {environment:action} {environment:position} the {locationType} of {locationName}."

`environment` would be based on time of day (orange in the morning, purple at night).
